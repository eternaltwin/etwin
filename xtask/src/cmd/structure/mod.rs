use crate::cmd::CliContext;
use clap::Parser;

pub mod print;

#[derive(Debug, Parser)]
pub struct Args {
  #[clap(subcommand)]
  command: Command,
}

#[derive(Debug, Parser)]
pub enum Command {
  /// Print the project structure
  #[clap(name = "print")]
  Print(print::Args),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum Error {
  #[error("print error")]
  Print(#[from] print::Error),
}

pub async fn run(cx: CliContext<'_, Args>) -> Result<(), Error> {
  match &cx.args.command {
    Command::Print(ref args) => {
      print::run(CliContext {
        args,
        env: cx.env,
        working_dir: cx.working_dir,
        out: cx.out,
      })
      .await?
    }
  };
  Ok(())
}
