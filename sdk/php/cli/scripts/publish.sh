#!/usr/bin/env ysh
var PROJECT_ROOT = $(cd "$_this_dir/.." { pwd })

cd $PROJECT_ROOT {
  var TEMP_DIR = "$(mktemp -d)"
  var NEW_VERSION = "$(jq --raw-output ".version" composer.json)"
  git clone git@gitlab.com:eternaltwin/packagist/cli.git "${TEMP_DIR}"
  find "${TEMP_DIR}" -mindepth 1 -not -path "${TEMP_DIR}/.git" -not -path "${TEMP_DIR}/.git/*" -delete
  cp -R composer.json "${TEMP_DIR}/composer.json"
  cp -R README.md "${TEMP_DIR}/README.md"
  cp -R eternaltwin "${TEMP_DIR}/eternaltwin"
  cp -R "src/" "${TEMP_DIR}/src/"
  cd $TEMP_DIR {
    pwd
    git add .
    git commit -m "Release v${NEW_VERSION}"
    git tag -a "v${NEW_VERSION}" -m  "Release v${NEW_VERSION}"
    git push --tags origin main
  }
  rm -rf $TEMP_DIR
}
