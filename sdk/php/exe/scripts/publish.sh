#!/usr/bin/env ysh
var PROJECT_ROOT = $(cd "${_this_dir}/.." { pwd })
var REPO_ROOT = $(cd "${PROJECT_ROOT}/../../.." { pwd })
var BIN_ROOT = $(cd "${REPO_ROOT}/bin" { pwd })

cd $PROJECT_ROOT {
  var TEMP_DIR = "$(mktemp -d)"
  var NEW_VERSION = "$(jq --raw-output ".version" composer.json)"
  git clone git@gitlab.com:eternaltwin/packagist/exe.git "${TEMP_DIR}"
  find "${TEMP_DIR}" -mindepth 1 -not -path "${TEMP_DIR}/.git" -not -path "${TEMP_DIR}/.git/*" -delete
  cp -R composer.json "${TEMP_DIR}/composer.json"
  cp -R README.md "${TEMP_DIR}/README.md"
  sed --expression="s/# \\[workspace\\]/[workspace]/" "${BIN_ROOT}/Cargo.toml" > "${TEMP_DIR}/Cargo.toml"
  cp -R "src/" "${TEMP_DIR}/src/"
  sed --expression="s/\"0.0.0\";/\"${NEW_VERSION}\";/" "./src/Exe/Version.php" > "${TEMP_DIR}/src/Exe/Version.php"
  cp -R "${BIN_ROOT}/src/main.rs" "${TEMP_DIR}/src/main.rs"
  cd $TEMP_DIR {
    pwd
    cargo generate-lockfile
    git add .
    git commit -m "Release v${NEW_VERSION}"
    git tag -a "v${NEW_VERSION}" -m  "Release v${NEW_VERSION}"
    git push --tags origin main
  }
  rm -rf $TEMP_DIR
}
