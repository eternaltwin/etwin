<?php declare(strict_types=1);

namespace Eternaltwin\PathTools;

use Eternaltwin\PathTools\PathTools;

final class WindowsPrefix {
  public readonly string $prefix;
  public readonly ?string $drive;
  public readonly ?string $uncName;
  public readonly ?string $uncBase;

  public static function get(string $path): ?self {
    if (empty($path)) {
      return null;
    }
    $l = strlen($path);
    $c = $path[0];
    if (Windows::isAsciiLetter($c) && $l > 2 && $path[1] === ":" && Windows::isSeparator($path[2])) {
      return new WindowsPrefix(substr($path, 0, 3), $path[0], null, null);
    }
    if (Windows::isSeparator($path[0])) {
      if ($l === 1 || !Windows::isSeparator($path[1])) {
        return new WindowsPrefix(substr($path, 0, 1), null, null, null);
      }
      $uncNameStart = 2;
      $uncNameEnd = $uncNameStart;
      while ($uncNameEnd < $l && !Windows::isSeparator($path[$uncNameEnd])) {
        // Eat segment
        $uncNameEnd += 1;
      }
      $uncName = substr($path, $uncNameStart, $uncNameEnd - $uncNameStart);
      if ($uncNameEnd == $l) {
        // If the prefix is `?` is may cause unexpected behavior:
        // `join("//?", "unc/bar")` risks returning `//?/unc/bar`, but `//unc/bar` is probably more expected
        return new WindowsPrefix(substr($path, 0, $uncNameEnd), null, $uncName, null);
      }
      $uncBaseStart = $uncNameEnd + 1;
      while ($uncBaseStart < $l && Windows::isSeparator($path[$uncBaseStart])) {
        // Eat separators
        $uncBaseStart += 1;
      }
      if ($uncBaseStart == $l || $uncName !== "?") {
        // If the prefix is `?` is may cause unexpected behavior:
        // `join("//?", "unc/bar")` risks returning `//?/unc/bar`, but `//unc/bar` is probably more expected
        return new WindowsPrefix(substr($path, 0, $uncBaseStart), null, $uncName, null);
      }
      $uncBaseEnd = $uncBaseStart;
      // We have an extended UNC path (prefix `//?/`), consume device or server
      while ($uncBaseEnd < $l && !Windows::isSeparator($path[$uncBaseEnd])) {
        // Eat segment
        $uncBaseEnd += 1;
      }
      $uncBase = substr($path, $uncBaseStart, $uncBaseEnd - $uncBaseStart);
      if ($uncBaseEnd == $l) {
        return new WindowsPrefix(substr($path, 0, $uncBaseEnd), null, $uncName, $uncBase);
      }
      $prefixEnd = $uncBaseEnd;
      while ($prefixEnd < $l && Windows::isSeparator($path[$prefixEnd])) {
        // Eat separator
        $prefixEnd += 1;
      }
      return new WindowsPrefix(substr($path, 0, $prefixEnd), null, $uncName, $uncBase);
    }
    return null;
  }

  private function __construct(
    string  $prefix,
    ?string $drive,
    ?string $uncName,
    ?string $uncBase,
  ) {
    $this->prefix = $prefix;
    $this->drive = $drive;
    $this->uncName = $uncName === "" ? null : $uncName;
    $this->uncBase = $uncBase === "" ? null : $uncBase;
  }

  public function normalized(): string {
    if ($this->drive !== null) {
      return $this->drive . ":/";
    } elseif ($this->uncBase !== null) {
      return "//?/" . $this->uncBase . "/";
    } elseif ($this->uncName !== null) {
      return "//" . $this->uncName . "/";
    } else {
      return "/";
    }
  }
}
