<?php declare(strict_types=1);


use Eternaltwin\PathTools\Posix;
use PHPUnit\Framework\TestCase;

final class PosixTest extends TestCase {
  public function testJoin(): void {
    $this->assertEquals("x/b/c.js", Posix::join(".", "x/b", "..", "/b/c.js"));
    $this->assertEquals(".", Posix::join(""));
    $this->assertEquals("/x/b/c.js", Posix::join("/.", "x/b", "..", "/b/c.js"));
    $this->assertEquals("/bar", Posix::join("/foo", "../../../bar"));
    $this->assertEquals("../../bar", Posix::join("foo", "../../../bar"));
    $this->assertEquals("../../bar", Posix::join("foo/", "../../../bar"));
    $this->assertEquals("../bar", Posix::join("foo/x", "../../../bar"));
    $this->assertEquals("foo/x/bar", Posix::join("foo/x", "./bar"));
    $this->assertEquals("foo/x/bar", Posix::join("foo/x/", "./bar"));
    $this->assertEquals("foo/x/bar", Posix::join("foo/x/", ".", "bar"));
    $this->assertEquals("./", Posix::join("./", "./"));
    $this->assertEquals("./", Posix::join(".", "./"));
    $this->assertEquals(".", Posix::join(".", ".", "."));
    $this->assertEquals(".", Posix::join(".", "./", "."));
    $this->assertEquals(".", Posix::join(".", "/./", "."));
    $this->assertEquals(".", Posix::join(".", "/////./", "."));
    $this->assertEquals(".", Posix::join("."));
    $this->assertEquals(".", Posix::join("", "."));
    $this->assertEquals("foo", Posix::join("", "foo"));
    $this->assertEquals("foo/bar", Posix::join("foo", "/bar"));
    $this->assertEquals("/foo", Posix::join("", "/foo"));
    $this->assertEquals("/foo", Posix::join("", "", "/foo"));
    $this->assertEquals("foo", Posix::join("", "", "foo"));
    $this->assertEquals("foo", Posix::join("foo", ""));
    $this->assertEquals("foo/", Posix::join("foo/", ""));
    $this->assertEquals("foo/bar", Posix::join("foo", "", "/bar"));
    $this->assertEquals("../foo", Posix::join("./", "..", "/foo"));
    $this->assertEquals("../../foo", Posix::join("./", "..", "..", "/foo"));
    $this->assertEquals("../../foo", Posix::join(".", "..", "..", "/foo"));
    $this->assertEquals("../../foo", Posix::join("", "..", "..", "/foo"));
    $this->assertEquals("/", Posix::join("/"));
    $this->assertEquals("/", Posix::join("/", "."));
    $this->assertEquals("/", Posix::join("/", ".."));
    $this->assertEquals("/", Posix::join("/", "..", ".."));
    $this->assertEquals(".", Posix::join(""));
    $this->assertEquals(".", Posix::join("", ""));
    $this->assertEquals(" /foo", Posix::join(" /foo"));
    $this->assertEquals(" /foo", Posix::join(" ", "foo"));
    $this->assertEquals(" ", Posix::join(" ", "."));
    $this->assertEquals(" /", Posix::join(" ", "/"));
    $this->assertEquals(" ", Posix::join(" ", ""));
    $this->assertEquals("/foo", Posix::join("/", "foo"));
    $this->assertEquals("/foo", Posix::join("/", "/foo"));
    $this->assertEquals("/foo", Posix::join("/", "//foo"));
    $this->assertEquals("/foo", Posix::join("/", "", "/foo"));
    $this->assertEquals("/foo", Posix::join("", "/", "foo"));
    $this->assertEquals("/foo", Posix::join("", "/", "/foo"));
  }

  public function testRelative(): void {
    $this->assertEquals("..", Posix::relative("/var/lib", "/var"));
    $this->assertEquals("../../bin", Posix::relative("/var/lib", "/bin"));
    $this->assertEquals(".", Posix::relative("/var/lib", "/var/lib"));
    $this->assertEquals("../apache", Posix::relative("/var/lib", "/var/apache"));
    $this->assertEquals("./lib", Posix::relative("/var/", "/var/lib"));
    $this->assertEquals("./var/lib", Posix::relative("/", "/var/lib"));
    $this->assertEquals("./bar/package.json", Posix::relative("/foo/test", "/foo/test/bar/package.json"));
    $this->assertEquals("../..", Posix::relative("/Users/a/web/b/test/mails", "/Users/a/web/b"));
    $this->assertEquals("../baz", Posix::relative("/foo/bar/baz-quux", "/foo/bar/baz"));
    $this->assertEquals("../baz-quux", Posix::relative("/foo/bar/baz", "/foo/bar/baz-quux"));
    $this->assertEquals("../baz", Posix::relative("/baz-quux", "/baz"));
    $this->assertEquals("../baz-quux", Posix::relative("/baz", "/baz-quux"));
    $this->assertEquals("../../..", Posix::relative("/page1/page2/foo", "/"));
  }

  public function testIsAbsolute(): void {
    $this->assertTrue(Posix::isAbsolute("/"));
    $this->assertTrue(Posix::isAbsolute("/foo"));
    $this->assertTrue(Posix::isAbsolute("/foo/bar"));
    $this->assertTrue(Posix::isAbsolute("/."));
    $this->assertFalse(Posix::isAbsolute(""));
    $this->assertFalse(Posix::isAbsolute("."));
    $this->assertFalse(Posix::isAbsolute("\\"));
    $this->assertFalse(Posix::isAbsolute("foo"));
    $this->assertFalse(Posix::isAbsolute("foo/bar"));
    $this->assertFalse(Posix::isAbsolute("C:/"));
    $this->assertFalse(Posix::isAbsolute("C:\\"));
    $this->assertFalse(Posix::isAbsolute("C:\\Windows"));
  }
}
