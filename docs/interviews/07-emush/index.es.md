# Equipo de desarrollo de eMush

## **Eternaltwin:** Hola, gracias por participar en este ejercicio de entrevista. ¿Pueden presentarse brevemente?

**Breut:** Hola, soy Breut (Sylvain Breton, 31 en la vida real). Descubrí los juegos Twinoid con Hordes (versión francesa de Die2Night) alrededor de 2013 y rápidamente comencé a jugar Mush. Cuando se creó el proyecto ET, rápidamente me involucré en la recopilación de datos de Mush y, después de 6 meses, finalmente di el salto y comencé a codificar para eMush. Me encanta codificar, pero no soy un desarrollador capacitado. Estaba usando python para investigar y no pensé que pudiera ser útil para el proyecto. Al final, ahora soy uno de los tres principales desarrolladores de eMush.

**Sami:** Hola, soy Sami, tengo 22 años y estaba ayudando en este proyecto porque Mush era parte de mi infancia, estaba buscando un proyecto existente y ya había uno así que aterricé en él, empecé informática bastante temprano en mi vida, lo que hace que sea una de mis pasiones y quería encontrarle un uso, incluso aunque me haya tomado un tiempo atreverme a hacerlo aquí. En la vida real, soy desarrollador de Java en una consultora, lo que me aleja bastante de PHP, pero sigo en mi campo porque hago el backend, y por el momento estoy en la banca porque me resulta difícil no volver a caer en mis reflejos Java cuando empiezo a usar PHP (y sin Webstorm es aún peor jaja). Mi trabajo ha primado sobre mis proyectos personales para prepararme para éste, por lo que he tenido que dejar el proyecto en suspenso por el momento.

**Zasfree:** Hola, soy zasfree. Descubrí los juegos Twinoid en 2011 y luego Mush en 2013. Realmente comencé a encariñarme con este juego en 2015.

Desde que comencé a jugar a Mush, quise comprender la mecánica del juego lo mejor posible para poder establecer los récords de supervivencia más impresionantes. Cuando empezó el proyecto Emush, aunque no soy desarrollador, me convertí en una de las tres primeras personas a cargo del proyecto. Estuve principalmente a cargo de recopilar datos sobre el juego original, seleccionar jugadores para las naves alfa de Emush y lanzar naves de prueba en Mush para recopilar la mayor cantidad de información posible. También estuve a cargo de los anuncios de proyectos.

Hoy, el alfa está abierto para todos, Mush está cerrado y Evian es mucho mejor que yo haciendo anuncios XD. Así que me encargo de poner a disposición información sobre el juego original a partir de capturas de pantalla o vídeos que tomé cuando el juego aún estaba abierto, para ayudar a los programadores cuando lo necesiten o tengan preguntas.

**Gowoons:** Hey, soy gowoons, un estudiante de ingeniería. Descubrí el mush en 2020 (sí, tarde jejeje, creo que el proyecto emush ya existía) porque estaba confinado con uno de mis mejores amigos al que le encanta el juego desde hace mucho tiempo (comodín). Inmediatamente me enganché y me encantó. Estoy estudiando informática, particularmente devops y cibernética. Empecé a participar en el proyecto hace 1-2 años y, aunque el desarrollo no es mi especialidad, lo disfruto mucho. Participo cuando tengo tiempo, hubo un largo período en el que no hice nada, y ahora estoy tratando de retomar un poco, lo cual a veces es difícil porque lamentablemente mis cursos/formación tienen que tener prioridad sobre emush :calim:

**Evian:** Hola, soy Evian. En la vida real, tengo 25 y trabajo como ingeniero en ciencia de datos.
Descubrí los juegos de Motion Twin con Minitroopers en la universidad, luego jugué casi todos los juegos de Twinoid, hasta que salió Mush en 2013 y le di mi alma en sacrificio (anécdota inútil y, por lo tanto, esencial, en ese momento yo era el 2do mejor jugador de Chun). 😛

Me había unido a un eMush alfa a principios de 2022 y quedé impresionado por el progreso realizado.
Habiendo aprendido a programar durante mi aprendizaje al mismo tiempo, me dio ganas de dar el salto. Hoy soy uno de los principales desarrolladores de eMush.

**A7:** Soy A7 (o Atar7, no importa), parece que descubrí los juegos MT con Hordes, luego Twinoid (ha pasado un tiempo...). De profesión vengo de la industria gráfica, con un gran interés por el desarrollo y la tecnología. Primero me uní al proyecto Eternaltwin para trabajar en el sitio principal, y luego al proyecto eMush, lo que parecía un desafío interesante.

## **Eternaltwin:** ¿Cómo gestionan eMush y su vida profesional/personal? ¿eMush marca el ritmo de sus días o es más bien lo que llena el tiempo de inactividad?

**Breut:** En tres años, ha habido muchos cambios en mi vida profesional, por lo que mi respuesta hoy no es la misma que el año pasado. En particular, hace dos años, pude trabajar en eMush de manera regular y eficiente porque estaba con poco trabajo (no tenía suficientes horas escolares disponibles para hacerlas), hoy es más durante las vacaciones escolares. Sin embargo, hago todo lo posible para resolver periódicamente los errores que se nos informan para que la comunidad, que ya no tiene acceso a Mush, pueda sentir que el proyecto avanza a pesar de nuestra disponibilidad reducida.

**Evian:** Tengo tendencias monomaníacas, por lo que eMush ocupa una (muy) gran parte de mis tardes y fines de semana en este momento, o incluso más si cuentas el tiempo que paso arreglando errores cuando estoy en clase (sí, lo sé, no está bien 😔).

Entonces es justo decir que es el ritmo de mi día.

A veces puedo ser menos activo cuando mi tiempo libre disminuye, principalmente porque tengo que repasar más para compensar el tiempo que dediqué a codificar para eMush en clase. Hmm... Quizás haya que hacer un cambio de estrategia.... 🤔

**R7:** Ha habido períodos de actividad variada en mi vida en los últimos años. Por principio, siempre considero el eMush como una segunda prioridad, mi vida profesional tiene que estar en primer lugar. Por suerte a veces tengo mucho tiempo libre, que lleno con proyectos personales como eMush.

## **Eternaltwin:** ¿Cómo dividen las tareas entre ustedes? ¿Algunos de ustedes tienen facilidades para una funcionalidad específica y simplemente lo hacen, o cada uno toma lo que quiere?

**Breut:** Entonces, para empezar, voy a dar 2 términos que probablemente usaremos para responder esta pregunta. Podemos dividir el proyecto en dos partes principales. El frente es lo que ves en su página web, cómo están dispuestos los diferentes elementos, el Daedalus representado en vista isométrica y el botón tan tentador para golpear a un Chao acostado. La parte trasera son todas las operaciones en el servidor eMush, donde la salud de Chao se cambia porque no pudiste resistirte a presionar ese botón.
Comencé con la parte trasera de eMush y luego terminé haciendo una sección enorme del frente, el Daedalus en vista isométrica. De esto último se podría decir que es mi especialidad. Para la parte trasera, a menudo trabajamos juntos, y cada vez que un desarrollador realiza una modificación, otro desarrollador debe volver a leer su código, lo que significa comprender lo que se ha hecho. Pero el proyecto es cada vez más grande con enfermedades, cazadores, exploraciones... Tengo que admitir que cada vez tengo menos y menos control del proyecto en su conjunto.

**Evian:** Breut lo resumió muy bien. También puedes pensar en la parte trasera como el motor del juego y el frente como su pantalla.

Hay algunas áreas de especialización: por el momento, estoy desarrollando la mayoría de las nuevas características en la parte trasera, como la exploración, mientras Breut consolida y limpia los cimientos del motor.
También tengo un pensamiento muy fuerte para A7, sin el cual el juego sería solo una página en blanco con texto (vamos, podría ser azul). 😱

Por otro lado, debemos ser versátiles si queremos sacar adelante el proyecto: si quiero que las exploraciones realmente funcionen, también necesito codificar el botón para lanzarlas y la página para visualizar los eventos, aunque esto no sea mi especialidad (pero afortunadamente A7 viene detrás de mí a corregir).

Todos hemos tenido que ensuciarnos las manos con todos los aspectos de eMush para realizar actualizaciones periódicas, y no es tan inusual que varios desarrolladores colaboren en una sola función durante varios días a la vez.

Finalmente, están los contribuyentes más ocasionales, que no son menos importantes.
Si miras de cerca, han desarrollado características que no priorizamos, pero que siguen siendo importantes. Pienso, por ejemplo, en nuestro sistema de alerta interno, que nos ayuda enormemente a depurar e intervenir en caso de problemas, o en el minimapa del barco.

**Zasfree:** Actualmente, mi participación con eMush es flexible. Superviso el proyecto varias veces al día, pero debido al bajo número de preguntas, no me ocupa la mayor parte del día. Mantengo un equilibrio dedicando tiempo específico al proyecto a la vez que preservo mi vida personal y profesional. Aunque las interacciones son raras, permanezco disponible y activo para satisfacer las necesidades de la comunidad.

Breut y Evian explicaron muy bien nuestro modus operandi. Por mi parte, después de las vacaciones, planeo involucrarme más como tester instalando el juego localmente. Esto me facilitará probar el juego sin molestar a los jugadores existentes. Mi objetivo es identificar problemas potenciales y contribuir proponiendo soluciones. Aspiro a ofrecer una mejor experiencia de juego a la comunidad de jugadores.

**R7:** Breut y Evian han resumido muy bien el proceso. Cada uno encuentra sus especialidades y el proyecto avanza gracias a un equipo multidisciplinar. Personalmente, no podría tocar la parte trasera, ni siquiera con la mejor voluntad del mundo.
Evian dice que sin mí, el juego sería solo una página de texto en blanco; mi respuesta es que sin él y Breut, el juego sería solo una página de inicio sin seguimiento 🙂

_Evian_ Y Simpkin sobre todo, ¡no te olvides de **Simpkin**! Sin Simpkin no hay proyecto, eMush sobre papel lápiz.

## **Eternaltwin:** Hoy en día, eMush todavía se encuentra en alfa abierta. ¿Qué características se implementarán a continuación? ¿Ya establecieron un hito para finalizar esta alfa y pasar a la beta?

**Breut:** Como recordatorio, la última actualización importante agregó exploraciones y aún queda contenido por agregar, como implementar los efectos de todos los sectores y equipos. Es probable que el siguiente paso esté relacionado con proyectos-habilidades-investigaciones. Normalmente hemos trabajado río arriba para que su implementación sea lo más sencilla posible, pero son tantas que seguramente nos encontraremos con casos especiales que nos harán pasar un mal rato. Una vez completada esta etapa, estaremos muy cerca de tener todas las características del juego básico. Creo que podremos pasar a la beta después de eso. Luego habrá mucho trabajo para finalizar por hacer: errores, pequeñas cosas aún no implementadas, mejoras en la interfaz, herramientas de administración...

**Gowoons:** Schrödinger también está dando los toques finales 😜 éste es muy esperado.

**R7:** También tendremos que empezar a trabajar en las cuentas de usuario y en todas las demás funciones que no forman parte del juego en sí. Todavía queda un poco de trabajo por hacer antes de que podamos esperar una beta realmente limpia.

**Evian:** Todavía estamos desarrollando las exploraciones y, de hecho, la secuela se centrará en todas las "mejoras" del Daedalus (proyectos, investigaciones y habilidades), dependiendo de lo que más soliciten los jugadores.

Muchos de los detalles mencionados por gowoons y A7 aún deben resolverse antes de una versión beta (y hay otros). Por un lado, porque tiendo a priorizar la velocidad de publicación de las actualizaciones sobre la precisión (incluso si eso significa corregir errores rápidamente) para mantener una sensación constante de progreso. Es más, el 20% restante de los detalles suelen ocupar el 80% del tiempo...

Por otro lado, como eMush es un juego muy complejo: a menudo escribo más código de prueba que código que implementa una característica, lo que no me impide olvidar casos particulares o ciertos comportamientos inesperados. Creo que podemos avanzar en esto, por ejemplo involucrando más a zasfree en la redacción de las características que se implementarán.

## **Eternaltwin:** ¿Qué es lo que más anhelan del trabajo restante?

**Breut:** Esto es un poco presuntuoso, pero tengo muchas ganas de añadir mi toque personal a eMush. Todavía está en discusión, pero me encantaría hacer una pequeña adición como un nuevo artefacto o habilidad. Una idea mucho menos controvertida que me gustaría agregar es un sistema de apariencia del equipo (¿quién no ha soñado con etiquetar una calavera y unas tibias cruzadas en su bote patrullero?) Estoy ansioso porque no tengo tiempo en este momento, pero llevo 1 mes queriendo implementar esto.

**Evian:** No puedo esperar a que vuelva el juego de investigación, porque eso es lo que más me gustó de Mush y realmente disfruto leyendo las historias detrás de él. Es más, me gustaría involucrarme cada vez más en la comunicación sobre el juego a medida que avanza el proyecto. Nunca tendremos el mismo número de jugadores que en 2013, pero creo que es posible atraer a muchos más jugadores, incluso entre los que ya han jugado Mush. Si todo va bien, tengo ambiciones mucho más ambiciosas (y controvertidas) que Breut para lo que sigue. 😛

## **Eternaltwin:** ¿Tiene alguna idea del número diario o total de jugadores en esta alfa?

**Evian** Hubo varios reinicios durante el desarrollo, lo que hace imposible proporcionar estadísticas exactas. Dicho esto, mientras hablo, hay cuatro barcos en progreso y 255 jugadores han lanzado al menos un juego.

## **Eternaltwin:** Motion Twin cerró todos sus juegos hace poco más de un mes. ¿Estaban mentalmente preparados para este cierre? ¿Cómo se sentieron al respecto?

**Evian:** Para ser honesto, realmente no había jugado en Twinoid durante al menos dos años, pero todavía leo los foros por costumbre, mientras me comunico en ellos para eMush. Así que fui menos afectado que los demás, pero de hecho, los pocos días después del cierre, me sentí extraño: intentaba ir a Twinoid con regularidad y encontraba la página del cierre.

**Breut:** Mi objetivo era poder ofrecer a los jugadores un alfa abierto antes del final total de Mush. Incluso si el juego está lejos de estar completo, creo que la apuesta ha valido la pena, desde el final de Twinoid hemos visto un aumento en el número de jugadores y en los informes de errores. Concretamente, para mí, esto ha significado un trabajo adicional de corrección de errores, ya que coincidió con un período muy ocupado en el trabajo, por lo que fue un poco complicado combinar todo eso.

## ¿Cómo ven el año que viene para emush? ¿Algún plan en particular, alguna característica sorpresa?

**Evian:** Veo el 2024 como una continuación del 2023.

De hecho, hace exactamente un año, eMush todavía estaba en alfa cerrado: solo se podía lanzar una nave a la vez, no había páginas finales, ni cazadores, ni planetas, ni exploración... La página de inicio dejaba mucho por desear, etc... 😅

Estoy contento con lo que hemos logrado y altamente motivado para seguir actualizando periódicamente, que sigue siendo lo más importante, especialmente para un proyecto de voluntariado como este.

También aprendí mucho en 2023, lo que me hizo sentir mucho más cómodo con el código eMush. Tengo muchas ideas que probar para ofrecer actualizaciones más confiables y en la comunicación en torno al juego.

En términos de funcionalidad, mi prioridad es asegurar que eMush tenga las mismas características que Mush, y con menos errores, lo más rápido posible: así que no nos extenderemos demasiado.

Así que vamos a terminar las exploraciones y luego abordaremos las búsquedas, proyectos o habilidades.

¡No dudes en contarnos en el foro o en Discord qué prefieres!
¡Gracias por la entrevista!

**Breut:** Para mí, 2024 va a estar más ocupado que 2023 en la vida real, lo que significa que estaré allí principalmente para ayudar a Evian a resolver errores y realizar algunos cambios técnicos para que agregar habilidades/proyectos/investigaciones sea lo más cómodo posible.
Como dijo Evian, lo más urgente es implementar todas las características de Mush antes de pasar a las características sorpresa, aunque tengo algunas ideas que probaré si tengo tiempo.