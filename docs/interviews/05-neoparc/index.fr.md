# NeoParc équipe de développement

## **Eternaltwin:** Bonjour à vous, merci de vous prêter à cette exercice d'interview. Pouvez vous vous présenter rapidement ?

**Dau2004:** Hello ! Moi c'est Dau2004 / PharoDev, j'ai été joueur plus ou moins régulier de pas mal de jeu de la MotionTwin à l'époque (Dinoparc, Dinorpg, Hammerfest, un peu de Hordes à l'occasion, ...). Au quotidien, je suis développeur dans ma vie pro. Niveau perso j'essaie également d'y consacrer du temps, même si c'est bien trop occasionnel pour le moment à mon goût 😅

**Jonathan:** Je m'appelle Jonathan, j'ai 28 ans et je suis originaire de la ville de Québec, au Canada. Je travaille comme développeur dans le secteur de la consultation en entreprise (développement et transformation numérique) depuis 2017. Depuis mon jeune âge, j'ai toujours été fasciné par l'informatique, car cela représentait pour moi un support sur lequel tout était possible, une source quasi infinie de contenu et de connaissances. Quand j'étais plus jeune, on avait un PC à la maison qu'on devait se partager ma soeur et moi. C'est elle qui m'avait fait découvrir Dinoparc par le biais de MonLapin en 2005-2006, c'était tellement cool de découvrir ce monde que la Motion Twin était en train de créer à l'époque!

## **Eternalwin:** Jonathan, NeoParc est sorti le 21 Juin 2021. Comment te sentais-tu lors de la première mise en ligne avec l'arrivée des premiers joueurs ?

**Jonathan:** Je me sentais un peu nerveux, car c'était la première fois qu'on mettait un de mes projets personnels en ligne (merci à Demurgos pour son aide sur cette étape du processus 🙂 ) J'étais aussi assez fier du travail accompli, et surpris de voir tous ces joueurs reprendre le jeu petit à petit 🙂

## **Eternalwin:** Pourquoi avoir choisis Dinoparc comme projet ? Était-ce votre jeu de prédilection ? Le seul jeu MT auquel vous jouiez ?

**Dau2004:** Alors actuellement, c'est tout pour Jo et rien pour moi niveau développements 😁 Au global, sans aucun doute, tout ce qui touche au gameplay et aux nouveautés passe par Jo 🙂 De mon côté j'apporte des petits fix à droite à gauche (performances, sécu, détection d'anomalies, fix urgents si je suis dispo) ; mais rien de bien conséquent actuellement ^^

**Jonathan:** Pour le développement à proprement dit, on ne se sépare pas vraiment de tâches, puisque le jeu tourne bien en production. Les mises à jour consistent surtout à mettre en ligne les différents événements de l'année (Pâques, Anniversaire, Halloween et Noel.) et les guerres des clans. Je m'occupe de pas mal tous ces aspects, et Dau et là pour m'épauler quand on a des besoins spécifiques sur des améliorations techniques ou des calibrations in-game. De son côté, il s'occupe aussi de la surveillance du jeu et des sanctions quand certains joueurs sortent des limites permises par notre communauté. Sinon, il n'est pas exclu que je puisse travailler sur des nouvelles features issues purement de l'ère Neoparc, mais souvent, ce sont des épopés que j'entreprends seul. J'aime autant développer que de faire du game-design, mais je ne suis pas très bon pour calibrer et modérer.

## **Eternalwin:** La deuxième guerre des clans s'est terminée cet été. Ce nouveau format de guerre est déjà différent de l'original, pouvez-vous en dire plus sur ce qui vous a amener à une guerre tri-faction plutot que chacun pour son clan ?

**Dau2004:** Celle-ci je la laisse en exclusivité à Jo : en terme de gameplay tout l'honneur lui revient ^^

**Jonathan:** À la base, le but était de pouvoir offrir une compétition viable malgré le petit nombre de joueur actif dans notre communauté. D'autres règles comme le maximum de 5 joueurs par clans ont été instaurées dans le but d'avoir un challenge intéressant et d'avoir un nombre de clans grandissant. Avec les factions, les différents clans alliés peuvent aussi collaborer pour prendre des territoires ensemble et ainsi avoir l'avantage sur ceux qui ne le font pas. Je trouvais que c'était indispensable de repenser le modèle de la Guerre des Clans, car le contexte et le gameplay de Neoparc n'est pas le même que celui de Dinoparc.

## **Eternalwin:** C'est vrai que voir du PvP trifaction est un pari osé a cause de l'équilibre a trouver mais rafraîchissant de part le gameplay apporté. Prévoyez vous d'autres types d'événements au niveau des clans ?

**Dau2004:** Idem question gameplay c'est pour Jo 😁

**Jonathan:** Pour le moment, la guerre des clans restera dans ce format puisqu'il y a d'autres nouveautés à apporter en dehors des clans en général, mais il n'est pas impossible que ce format soit modifié ou changé dans le futur!

## **Eternalwin:** L'event de Noel est déjà à la moitié de sa durée. En plus de celui-ci, un event de Paques et d'Halloween existent. Avez-vous prévu de faire d'autres événements temporaires lié à des périodes particulière ?

**Dau2004:** Habituellement il y a l'évent anniversaire également fin juin, ce qui donne des évents "non guerre" au long de l'année (avec qq mois de pause en début d'année et sur l'été) : pâques, anniversaire, halloween, noël. Pour ce qui est des prévisions de nouveautés, pas à ma connaissance mais Jo a peut-être des trucs dans son chapeau 🙂

## **Eternalwin:** Vous avez comme beaucoup d'entre nous grandis et joué aux jeux de Motion Twin. Vous avez tellement aimé leurs jeux que vous avez décidé d'en faire revivre un. Comment avez vous vécu la fermeture des jeux web de Motion Twin ?

**Dau2004:** Pas si mal, toujours un peu déçu de ne pas avoir pu récupérer des data historiques côté Dinoparc (base de données dinoz/joueurs sans mail par exemple), ça aurait permis à certains de revenir un jour sans tout recommencer, mais bon c'est compréhensible de leur part 😅 Pas mal de jeu sont perdus à jamais, mais c'est la dure loi de l'informatique, certains trucs sont refondus, d'autres disparaissent :/

**Jonathan:** De mon côté, je n'ai pas trouvé cela si terrible. C'est certain que ce sont de gros morceaux d'histoires qui s'effacent, mais en même temps, les jeux qui ont eu la chance de revivre grâce à nos projets sont des versions améliorées de leurs homologues originaux.

## **Eternalwin:** 2024 est la, quelles sont les prochaines grandes étapes et objectifs de NeoParc ?

**Jonathan:** Sans entrer trop dans les détails, il y aura des nouveautés hors événement et mises à jours techniques. On ne peut pas en dire plus pour le moment! 😉

**Dau2004:** Effectivement c'est Jo qui a les plans en tête 😁 Merci à toi pour le temps passé et la patience sur nos retours ^^

## **Eternalwin:** Sauriez nous dire le nombre de joueur ayant participé a l'évent de Noël ainsi que le nombre total de collections échangées ?

**Dau2004:** 49 joueurs ont participé en tout, 42 ont fait au moins 10 collections, et au total 4030 collections échangées