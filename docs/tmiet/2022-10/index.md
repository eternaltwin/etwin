# 2022-10: This Month In Eternaltwin #13

[Eternaltwin](https://eternaltwin.org) is a project with the goal to preserve
Motion Twin's games and player communities.

# Eternaltwin

## Crowdin support

[Eternaltwin is now on Crowdin](https://eternaltwin.crowdin.com/). Crowdin is a
website to work on translations. Crowdin sponsored us, and we have a free
enterprise account.

If you are a player wishing to help, you may now submit translations to help you
favorite projects. If you are game leader, please contact _Demurgos_ to
configure your project.

## Rust Migration

In October, the new Eternaltwin server implemented in Rust was enabled and
the old Node.js backend was entirely removed.

The Eternaltwin code was imported from Eternalfest, dating back to roughly 2015.
It had various issues causing the server to regular crash or hang. To fix this
we started a large internal effort to move to the Rust programming language,
which is faster and more reliable. This proved successful: there are no crashes
and performance is very good.

During the migration, we had a large amount of checks and compatibility layers
between the old and new code. This allowed to avoid any breakage, but it also
made it fairly hard to add new features. Now that this work is definitely complete,
we'll be able to focus on some long announced features such as better tools
around accounts (registration by email, email verification, password reset).

Here is an image of the evolution of the languages used for Eternaltwin:

![Eternaltwin](./loc.svg)

# Eternal DinoRPG

Welcome [dinoz masters](https://dinorpg.eternaltwin.org/)!

The alpha version 0.4.0 is now available. You may still travel, fight, level-up
and see your _moueffe_.

There are not many visible changes, but a lot of work was done to make it
easier to work on the project. We'll be able to publish updates more often.

There's still a new feature in this update: Non-Playable Characters (NPC)!
Only two are present at the moment, but new ones will join over time. By the way,
one these two should give you something nice.

See you later!

# eMush

Welcome to this section dedicated to [eMush](https://emush.eternaltwin.org/),
the project to save Mush.

## What's new?

Last time we announced the **Disease Updated**, a major update to eMush. This
update completed the disease system of them game with over 60 diseases and wounds
(including some that did not work properly in Mush). Some brave players joined
the test spaceship and are dealing with these new plagues.

![eMush](./emush.png)

Thanks to their help, we are working on fixing the genetic kinks caused by this
update (regrowing tongue, Mushes allergic to Mush... 😱).

## Coming next

In the meantime we're working on the next updated. It will let us start the **open alpha**.
We're focusing on 3 main points:

- **safe database migration**, so we don't have to reset the game and spaceships everytime
- **spaceship archives**, to keep track of your achievements
- **english translations**

Regarding this last point, the game is now translated into english at 65% thanks
to the help of the different contributors. Thank you very much.

## I missed the alpha! How do I play?

Depending on when you read this news, the alpha may still be available. 👀

You'll find the details to join the tests [in the Eternaltwin discord](https://discord.gg/SMJavjs3gk).

eMush is an Open Source project, our detailed release notes for the **Disease Update** are [available on Gitlab](https://gitlab.com/eternaltwin/mush/mush/-/releases/0.4.4).

Thank you for your attention, and see you soon aboard the Daedalus!

# MyHordes

Play to [MyHordes](https://myhordes.eu/).

_radio crackle_

Hello everyone, Ben here with our regular desert news update!

![MyHordes - Wireless technology ad](./myhordes.png)

I hear reports that lots of citizens _scrambled_ pumpkins in the wasteland. It
may become a regular occurrence, since it happened last year as well... Although
this time, there's far fewer sightings. What could this all mean? _scrambled_

In other news, it seems that some towns stopped playing by the rules, or rather
invent their own ones. _scrambled_ reported incidents of mysterious poisonings,
citizens running around the desert carrying several bags at once and even...
_paper rustling_ ... digging at the same spot multiple times? These are truly
confusing times _scrambled_

Oh, that reminds me. Surely, you have noticed _scrabled_ crystal clear sound of
my voice. That's because we invested in better tech for our broadcasting station!
With this, our never-ending effort to bring the latest news continues to _scrabled_
_loud beep_ _loud static_ what do you mean, the antenna is on fire??? OH MY G _scrambled_

# Eternal Kingdom

It's been a long time since the last post about [Eternal Kingdom](https://kingdom.eternaltwin.org/).

We keep sending updates for the **alpha 0.4**. We fixed many issues, see [the full details on the forum](https://eternaltwin.org/forum/threads/733e22e2-cbf8-474e-bd28-3183d67a2bc6).

We are working on two things at the moment: the map generation and some maintenance tasks.

We worked on the world map generation tool. In particular on how we display the houses of each city.

![Kingdom world map with different kinds of houses](./kingdom.png)

We are also planning some maintenance before moving to the alpha 0.5 tasks.
- Migrate the project to Symfony 5 (better performance)
- Configure a Crowdin project for translations
- Add automate tests to catch bugs more easily

We would like to deploy the alpha 0.5 in November.

# PiouPiouZ

In the world of the Piouz, everything's good.

The work on a game version without Flash is advancing. The Piouz can move, but
don't have any abilities yet.

[Here is a preview of this new version](https://public.thetoto.fr/piou/pixi/).

No Piou was harmed during development.

# Closing words

You can send [your messages for November](https://gitlab.com/eternaltwin/etwin/-/issues/55).

This article was edited by: Bibni, Biosha, Brainbox, Demurgos, Evian, Fer,
Patate404, and Schroedinger.
