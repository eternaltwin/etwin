# 2022-12 : Ce mois-ci sur Eternaltwin n°15

[Eternaltwin](https://eternaltwin.org) est un projet visant à préserver les jeux
et communautés de joueurs de Motion Twin.

# Eternaltwin

_Biosha_ a créé un outil pour surveiller l'état du site et envoyer des alertes
en cas de soucis. Cet outil sera activé bientôt.

_Moulins_ a mis à jour l'archive Hammerfest pour renvoyer les inventaires et
états des quêtes.

# Eternalfest

[Eternalfest](https://eternalfest.net/) permets de jouer aux niveaux classiques
ou inédits du jeu de plateformes Hammerfest.

_Rarera_ and _Maxdefolsch_ ont publié un nouveau jeu le 25 décembre:
[L'ancienne source](https://eternalfest.net/games/eb10aaf1-23dc-4b3f-a543-66a0596459d4).
Il contient des centaines de super niveaux, on vous recommande de l'essayer.

Il y a également eu beaucoup de travail pour changer la gestion interne des jeux.
Cela devrait permettre de gérer des fonctionnalités attendues depuis longtemps
telles que les pages d'inventaire, le support des traductions ou une barre de
chargement précise. La partie serveur est principalement finie, et nous
travaillons sur l'interface. Ce travail sert aussi de prototype pour améliorer
la gestion des jeux au niveau d'Eternaltwin (et remplacer la configuration
manuelle).

# eMush

Bienvenue à tous dans cette section de consacrés à
[🍄 eMush](https://emush.eternaltwin.org/), le projet de sauvegarde de Mush.

## Quoi de neuf ?

En ce beau mois de décembre, les admins de mush et les aficionados des parties
customisées vont trouver leur bonheur. En effet, nous avons travaillé dur pour
que différents Daedalus avec des configurations répondant à vos plus folles
envies puissent être lancés en parallèle. Et le plus beau, c’est que tout est
paramétrable directement depuis le site par les admins.

![eMush](./emush.png)

Parmi les autres avancées, il faut noter l’amélioration de l’archivage des
Daedalus. Une fois que la partie se termine toutes les entités servant au jeu
sont supprimées pour ne garder que les informations nécessaires à votre
postérité numérique.

Nous vous annoncions en novembre que les traductions anglaises étaient finies,
et bien il est désormais possible pour un administrateur de choisir la langue
des Daedalus qu’il crée.

## Prochainement

Comme annoncé le mois dernier, nous allons continuer à travailler sur les
migrations de notre base de données, afin de déployer de nouvelles
fonctionnalités sans détruire les vaisseaux en cours.

L’archivage des vaisseaux étant fini, il reste encore à les afficher
correctement depuis votre profil.

## À quand l’alpha ouverte ?

Nous continuerons à vous communiquer nos avancées à travers cette gazette,
mais le meilleur moyen d'être informé reste de rejoindre [le discord d'Eternaltwin](https://discord.gg/SMJavjs3gk).

eMush étant un projet open-source, nos avancées sont également disponibles sur GitLab,
en particulier [l'avancement de la mise à jour **Spores for All!**](https://gitlab.com/eternaltwin/mush/mush/-/milestones/12#tab-issues).

Merci de nous avoir lu, et à bientôt sur le Daedalus !

# MyHordes

Jouer à [MyHordes](https://myhordes.eu/).

_crépitement radio_

Bonjour à tous, j'espère que tout va bien pour vous pendant cette saison froide _brouillé_

![MyHordes - Pub pour technologie sans fil](./myhordes.fr.png)

J'espère que vous n'avez pas pris trop de chocolats de Noël parce qu'apparemment
ils so _brouillé_

Je pense aussi que vous aimeriez comparer votre _brouillé_ Nous avons donc
développé un système de classement unique pour toutes les Distinctions.

Les préparatifs de la 15ème saison sont en cours, nous avons ouvert un sondage
pour vous demander à participer à la bêta fermée, disponible sur la fréquence
102.4 MHz (Forums Mondiaux), c'est disponible jusqu'au 8 janvier 2023 !
_crépitement radio_

De plus, nous avons optimisé plusieurs aspects de notre site Web pour qu'il soit
plus pratique pour vos systèmes GPS mobiles Anthrax. _crépitement radio_

mmmmh, je pense qu'il y a _brouillé_ mais c'est quoi ce _brouillé_ BEN ! Dit
à ce corbeau de descendre de _brouillé_

# Le mot de la fin

Bonne année tout le monde !

Vous pouvez envoyer [vos messages pour
l'édition de janvier](https://gitlab.com/eternaltwin/etwin/-/issues/57).

Cet article a été édité par : Breut, Demurgos, Dylan57, Fer, Patate404 et Schroedinger.
