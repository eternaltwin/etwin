# 2021-12: Este mes en Eternaltwin #3

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de
preservar los juegos y comunidades de jugadores de Motion Twin.

# Aplicación Eternaltwin

Ahora puedes [descargar la versión 0.5.6](https://eternaltwin.org/docs/desktop)
de la aplicación Eternaltwin. Te deja jugar a juegos Flash.

- Links actualizados: MyHordes, Eternal DinoRPG.
- Usuarios de Windows ahora pueden descargar la app como un archivo .zip para
  evitar la instalación.
- Arreglados los problemas de conexión con la versión inglesa de Hammerfest.

¡Asegúrate de descargar la última versión!

# Eternaltwin

La migración del servidor al lenguaje Rust se acerca a su fin.

El foro ahora tiene [una sección alemana](https://eternaltwin.org/forum/sections/86b23bed-27e8-4a19-b4d6-22a9fcd9142d).

[Eternal DinoRPG](https://dinorpg.eternaltwin.org/) ya está disponible en línea,
y Epopotamo debería seguirle dentro de poco. Dado el aumento del número de
juegos, estamos planeando movernos a un mejor servidor.

# Emush

El [Emush](https://emush.eternaltwin.org/) proyecto Emush ha tenido algo de
progreso. Principalmente, los problemas hallados durante la última Alfa fueron
(en su mayoría) corregidos.

Esto nos trae al despliegue de la última versión (0.4.2) al principio de este
mes, con arreglos y completando los efectos de enfermedad. Algunos testers
están buscando deshacerse de estas nuevas naves Daedalus.

# MyHordes

_estática de radio_

¡Hola amigos y gracias por usar nuestro sistema radiofónico inalámbrico! Soy
Connor y hoy les envío un pequeño mensaje sobre
[MyHordes](https://myhordes.eu/).

![MyHordes - Wireless technology ad](./myhordes.png)

Como saben, los Cuervos se reúnen alrededor de las ciudades a fin de año. Por
falta de calor ellos seguirán devorando tu hígado durante este período invernal.
Pero incluso así ellos no son codiciosos: desde el 20 de diciembre hasta el 7
de enero serás capaz de robar a tus amigos con el traje de Santa Claus. Quién
sabe, ¿tal vez serás capaz de soportar su mal olor? Además tómate el tiempo de
buscar meticulosamente en los centros de correo, ¿quizá encuentres paquetes
navideños perdidos? Oh y por cierto, ten cuidado porque la arena se está
endureciendo, ¡podrías lastimarte! Y cuida de tu hígado… estática el azúcar no
debería ser abusado.

Eso es todo por ahora, tengan felices fiestas con sus compañeros ciudadanos, y
sus zombies ami… _cortado_

# Eternal DinoRPG

**¡La versión alfa 0.1.0 de Eternal DinoRPG ya está disponible!**

Puedes hallarla aquí: [https://dinorpg.eternaltwin.org/](https://dinorpg.eternaltwin.org/).

El sitio web funciona sin Flash, sin embargo algunas animaciones podrían no
mostrarse correctamente. Se recomienda usar [la aplicación Eternaltwin](https://eternaltwin.org/docs/desktop)
por ahora: Eternal DinoRPG está al tope de la lista.

Puedes ingresar, comprar unos pocos dinoz y moverlos aleatoriamente por unas
pocas ubicaciones. Una funcionalidad menos visible pero funcional es que puedes
mostrar todos los efectos de estado dentro del juego, tu inventario, así como
todas las habilidades.

Tenemos comparador de Dinoz por lo que ustedes, la comunidad de DinoRPG, nos
pueden ayudar con la clasificación de los más de 7000 elementos gráficos que
componen a un dinoz. Siéntanse libres de comprar algunos dinoz, ¡prueben de
todo!

La base de datos será reiniciada unas pocas veces a lo largo del período de
desarrollo.

Ya estamos trabajando en la siguiente versión para dejarte importar dinoz a tu
cuenta, y agregar algunas mejoras como un selector de lenguaje. Luego, ¡la
versión siguiente a esa se deshará completamente de Flash!

¡Nos vemos pronto para más noticias!

# Neoparc

Un evento de navidad está ocurriendo desde el 1 de diciembre en
[Neoparc](https://neoparc.eternaltwin.org/).
Durante este evento, cada maestro de Dinoz puede recolectar Tickets de Regalo
cada día al excavar en las distintas ubicaciones de Dinoland. Además, la famosa
raza de dinoz “Santaz” ha regresado.

Muchos jugadores afortunados ya han recolectado algunos huevos de esta codiciada
raza de dinoz en la Ruleta Navideña de Dinotown. Pequeño spoiler para nuestros
queridos lectores: todos aquellos que ingresen el 25 de diciembre conseguirán
un Huevo de Santaz como obsequio por este maravilloso primer año.

¡Esta actualización de la temporada festiva está llena de emocionantes secretos!
Por favor disfruten descubrirlos a su ritmo. Vayan y compartan lo que encuentren
y lo que sienten con el resto de la comunidad en
[el canal de Discord](https://discord.gg/ERc3svy).

¡Es con placer que les deseo a todos una estupenda temporada festiva!

Su Neodev, Jonathan.

# Directquiz

[Directquiz](https://www.directquiz.org/) ganó algunas nuevas funcionalidades
estos últimos dos meses.

La primera nueva funcionalidad es un indicador de escritura, como en muchas apps
modernas. ¡Ahora está disponible en **Directquiz**!

Lo siguiente, ahora pueden [presentar nuevas preguntas](https://www.directquiz.org/validationQuestion.php)
sin gastar sus Direct Dollars. ¡Pueden elegir de una lista de más de 300
categorías! ¡Adelante, envíen preguntas!

Finalmente, por esta temporada invernal, el sitio web ahora tiene un tema nevado
animado para celebrar el fin de año como corresponde.

También hicimos algunas mejoras internas alrededor de los archivos de
configuración para preparar una futura migración a los servidores de Eternaltwin.

# Palabras finales

Pueden mandar [sus mensajes para diciembre](https://gitlab.com/eternaltwin/etwin/-/issues/35).

¡Feliz navidad y feliz fin de año!
