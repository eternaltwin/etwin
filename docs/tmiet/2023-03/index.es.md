# 2023-03: Este Mes En Eternaltwin #18

[Eternaltwin](https://eternaltwin.org) es un proyecto con el objetivo de preservar
los juegos de Motion Twin y sus comunidades de jugadores.

# Eternaltwin

¡EternalTwin necesita desarrolladores! Todos los juegos avanzan a su propio ritmo, pero el sitio web principal utilizado para unificarlo todo lo está pasando mal. Si estás interesado, comunícate en Discord con Patate404#8620, Demurgos#8218 o Biosha#2584.

Se acerca la actualización 0.12 y se enfoca en el lanzamiento de sus paquetes para desarrolladores.

# DinoCard

La primera versión del sitio ya está en línea. Ya es posible crear tu mazo y probarlo contra otros jugadores.

En estos primeros meses de desarrollo hemos implementado el ciclo de combate e implementado un total de 96 cartas (71 dinoz y 25 incantaciones). Ya es posible probar batallas con casi todas las habilidades especiales de las cartas (25 / 28).

El siguiente paso del proyecto ahora es implementar el funcionamiento interno del sistema de efectos que se usará para implementar casi todas las demás cartas del juego. También discutimos cómo dar recompensas al jugador. Puedes leerlo en dinocard.net o en el hilo de Discord asociado.

Aquí hay algunas estadísticas sobre las cartas en el juego.

![DinoCard - Cards stats](./dinocard.png)

# Popotamo

Luego de una (muy) larga interrupción, la codificación se reanudó en los últimos días de marzo bajo el impulso de @Aredhel. Su primera acción fue abordar el error más difícil que bloqueaba todo el progreso: algunas palabras legítimas eran rechazadas en el caso de la mampostería, lo que impidió validar el movimiento. El parche está esperando a ser cargado en el sitio web de ePopotamo.

# MyHordes

Juega [MyHordes](https://myhordes.eu/).

¡Hola a todos!

Pequeño anuncio para informarles que un concurso de arte ya está abierto. Finaliza el 30 de abril, [toda la información disponible sobre este tema se encuentra aquí.](https://myhordes.eu/jx/forum/9/68041) ¡A sus lápices!

# eMush

Bienvenidos a esta sección dedicada a [🍄 eMush](https://emush.eternaltwin.org/),
el proyecto dedicado a salvar Mush.

## ¿Qué hay de nuevo?

El mes pasado anunciamos el lanzamiento en alfa abierta de la actualización "Spores for All!".

¡Más de 150 de ustedes se unieron a una nave!

Hemos pasado el mes de marzo estabilizando el juego gracias a sus numerosos comentarios: ¡todo el equipo de eMush los agradece muchísimo! ❤

![eMush Spore Update for All](./emush1.png)

Por otro lado, también tuvimos la oportunidad de hablar este mes con blackmagic, uno de los desarrolladores de Mush, seguido de una serie de preguntas y respuestas con la comunidad.

Fue una oportunidad para discutir las decisiones que se tomaron en Mush, lo cual fue muy enriquecedor para nosotros.

Salimos de esta entrevista con un montón de buenos consejos y mucha motivación para ofrecerles un gran remake de Mush.

## Lo que se viene

Durante este mes ocupado y siguiendo sus comentarios, también comenzamos a trabajar en la próxima versión de eMush que introducirá a los cazadores.

Ya podemos eliminar las naves iniciales con torretas en la versión de desarrollo:

![eMush Coming soon](./emush2.png)

Tenemos la intención de ofrecerles esta actualización muy pronto y agregar funciones (patrulleros, escombros) con regularidad.

## ¡Genial! ¿Cómo podemos jugar?

🎮 Juega [eMush](https://emush.eternaltwin.org)

💬 [Discord](https://discord.gg/SMJavjs3gk)

¡Gracias por leer, y espero verlos pronto en el Daedalus!

# Eternal DinoRPG

Hola a todos y gracias por su atención a esta sección dedicada a [Eternal DinoRPG](https://dinorpg.eternaltwin.org/).

¡Saludos, Maestros Dinoz!

La versión 0.6.0 está lista para ser puesta en línea. Desafortunadamente, aún no hay importaciones disponibles, pero estamos haciendo todo lo posible para traerlas lo antes posible.

No hay grandes noticias visibles, pero una gran refactorización del código está en progreso ahora que hemos podido dar un paso atrás del proyecto.

El equipo todavía está buscando un nuevo nombre para el juego. ¡No se detengan!

Nos vemos pronto. El equipo de EternalDino.

# Palabras finales

Pueden enviarnos [sus mensajes de abril](https://gitlab.com/eternaltwin/etwin/-/issues/60).

Este artículo ha sido editado por: Biosha, BrainBox, Demurgos, Dylan57, Evian, Fer, Patate404, Schroedinger y Zenoo.
