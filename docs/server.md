# Server

## Environment

| Name     | Version                          |
|----------|----------------------------------|
| JRE      | 17                               |
| Nginx    | 1.22                             |
| Node.js  | >= 18.17.0, LTS recommended      |
| PHP      | 8.3                              |
| Postgres | 17.2                             |
| Ruby     | 3.0                              |

## Network

- Internal name: `pousty.eternaltwin.org`
- IPv4: `162.55.107.242`
- IPv6: `2a01:4f8:2191:302a::2`

# Configuration

The server is initialized from a custom Arch VM image.
The application configuration is then loaded from the repo [eternaltwin/config](https://gitlab.com/eternaltwin/config).
