use crate::core::{Listing, Request};
use crate::job::{StoreTask, TaskStatus, Tick};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Hash)]
pub struct GetTasks {
  pub status: Option<TaskStatus>,
  /// Do not returned ready tasks that were polled during the provided tick
  ///
  /// This can be used to process multiple batches of tasks during the same tick without re-polling already handled
  /// tasks.
  pub skip_polled: Option<Tick>,
  pub offset: u32,
  pub limit: u32,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetTasksError {
  #[error(transparent)]
  Other(WeakError),
}

impl GetTasksError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl<OpaqueTask, OpaqueValue> Request<fn() -> (OpaqueTask, OpaqueValue)> for GetTasks {
  type Response = Result<Listing<StoreTask<OpaqueTask, OpaqueValue>>, GetTasksError>;
}
