use crate::core::{Instant, Request};
use crate::types::WeakError;
use std::error::Error;

/// Refresh the task status for all task waiting on a timer.
#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct OnTimer {
  /// Current time
  ///
  /// Tasks waiting for any time older than `time` should become ready to run.
  pub now: Instant,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum OnTimerError {
  #[error(transparent)]
  Other(WeakError),
}

impl OnTimerError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for OnTimer {
  type Response = Result<(), OnTimerError>;
}
