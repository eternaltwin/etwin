use crate::core::Request;
use crate::job::{StoreTask, TaskId};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct GetTask {
  pub id: TaskId,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetTaskError {
  #[error("task {0} not found")]
  NotFound(TaskId),
  #[error(transparent)]
  Other(WeakError),
}

impl GetTaskError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl<OpaqueTask, OpaqueValue> Request<fn() -> (OpaqueTask, OpaqueValue)> for GetTask {
  type Response = Result<StoreTask<OpaqueTask, OpaqueValue>, GetTaskError>;
}
