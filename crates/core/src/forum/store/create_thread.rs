use crate::core::{HtmlFragment, Request};
use crate::forum::{ForumSectionRef, ForumThreadTitle, MarktwinText, RawCreateForumThreadResult, RawForumActor};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CreateThread {
  pub actor: RawForumActor,
  pub section: ForumSectionRef,
  pub title: ForumThreadTitle,
  pub body_mkt: MarktwinText,
  pub body_html: HtmlFragment,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum CreateThreadError {
  #[error(transparent)]
  Other(WeakError),
}

impl CreateThreadError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for CreateThread {
  type Response = Result<RawCreateForumThreadResult, CreateThreadError>;
}
