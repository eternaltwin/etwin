use crate::core::Request;
use crate::forum::ForumSectionRef;
use crate::types::WeakError;
use crate::user::UserIdRef;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AddModerator {
  pub section: ForumSectionRef,
  pub target: UserIdRef,
  pub granter: UserIdRef,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum AddModeratorError {
  #[error(transparent)]
  Other(WeakError),
}

impl AddModeratorError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for AddModerator {
  type Response = Result<(), AddModeratorError>;
}
