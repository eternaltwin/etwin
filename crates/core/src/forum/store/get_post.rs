use crate::core::Request;
use crate::forum::{ForumPostIdRef, RawForumPost};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetPost {
  pub post: ForumPostIdRef,
  /// Revision offset
  pub offset: u32,
  /// Revision limit
  pub limit: u32,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetPostError {
  #[error("section not found")]
  NotFound,
  #[error(transparent)]
  Other(WeakError),
}

impl GetPostError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetPost {
  type Response = Result<RawForumPost, GetPostError>;
}
