pub mod add_moderator;
pub mod create_post;
pub mod create_post_revision;
pub mod create_thread;
pub mod delete_moderator;
pub mod get_post;
pub mod get_posts;
pub mod get_role_grants;
pub mod get_section_meta;
pub mod get_sections;
pub mod get_thread_meta;
pub mod get_threads;
pub mod upsert_system_section;

pub use self::add_moderator::{AddModerator, AddModeratorError};
pub use self::create_post::{CreatePost, CreatePostError};
pub use self::create_post_revision::{CreatePostRevision, CreatePostRevisionError};
pub use self::create_thread::{CreateThread, CreateThreadError};
pub use self::delete_moderator::{DeleteModerator, DeleteModeratorError};
pub use self::get_post::{GetPost, GetPostError};
pub use self::get_posts::{GetPosts, GetPostsError};
pub use self::get_role_grants::{GetRoleGrants, GetRoleGrantsError};
pub use self::get_section_meta::{GetSectionMeta, GetSectionMetaError};
pub use self::get_sections::{GetSections, GetSectionsError};
pub use self::get_thread_meta::{GetThreadMeta, GetThreadMetaError};
pub use self::get_threads::{GetThreads, GetThreadsError};
pub use self::upsert_system_section::{UpsertSystemSection, UpsertSystemSectionError};
use crate::core::{Handler, Listing};
use crate::forum::{
  ForumSection, RawCreateForumPostResult, RawCreateForumPostRevisionResult, RawCreateForumThreadResult, RawForumPost,
  RawForumRoleGrant, RawForumSectionMeta, RawForumThreadMeta, RawShortForumPost,
};
use async_trait::async_trait;
use std::ops::Deref;

#[async_trait]
pub trait ForumStore:
  Send
  + Sync
  + Handler<AddModerator>
  + Handler<DeleteModerator>
  + Handler<GetSections>
  + Handler<GetSectionMeta>
  + Handler<GetThreads>
  + Handler<GetThreadMeta>
  + Handler<CreateThread>
  + Handler<GetPosts>
  + Handler<CreatePost>
  + Handler<GetPost>
  + Handler<CreatePostRevision>
  + Handler<GetRoleGrants>
  + Handler<UpsertSystemSection>
{
  async fn add_moderator(&self, req: AddModerator) -> Result<(), AddModeratorError> {
    Handler::<AddModerator>::handle(self, req).await
  }

  async fn delete_moderator(&self, req: DeleteModerator) -> Result<(), DeleteModeratorError> {
    Handler::<DeleteModerator>::handle(self, req).await
  }

  async fn get_sections(&self, req: GetSections) -> Result<Listing<RawForumSectionMeta>, GetSectionsError> {
    Handler::<GetSections>::handle(self, req).await
  }

  async fn get_section_meta(&self, req: GetSectionMeta) -> Result<RawForumSectionMeta, GetSectionMetaError> {
    Handler::<GetSectionMeta>::handle(self, req).await
  }

  async fn get_threads(&self, req: GetThreads) -> Result<Listing<RawForumThreadMeta>, GetThreadsError> {
    Handler::<GetThreads>::handle(self, req).await
  }

  async fn get_thread_meta(&self, req: GetThreadMeta) -> Result<RawForumThreadMeta, GetThreadMetaError> {
    Handler::<GetThreadMeta>::handle(self, req).await
  }

  async fn create_thread(&self, req: CreateThread) -> Result<RawCreateForumThreadResult, CreateThreadError> {
    Handler::<CreateThread>::handle(self, req).await
  }

  async fn get_posts(&self, req: GetPosts) -> Result<Listing<RawShortForumPost>, GetPostsError> {
    Handler::<GetPosts>::handle(self, req).await
  }

  async fn create_post(&self, req: CreatePost) -> Result<RawCreateForumPostResult, CreatePostError> {
    Handler::<CreatePost>::handle(self, req).await
  }

  async fn get_post(&self, req: GetPost) -> Result<RawForumPost, GetPostError> {
    Handler::<GetPost>::handle(self, req).await
  }

  async fn get_role_grants(&self, req: GetRoleGrants) -> Result<Vec<RawForumRoleGrant>, GetRoleGrantsError> {
    Handler::<GetRoleGrants>::handle(self, req).await
  }

  async fn create_post_revision(
    &self,
    req: CreatePostRevision,
  ) -> Result<RawCreateForumPostRevisionResult, CreatePostRevisionError> {
    Handler::<CreatePostRevision>::handle(self, req).await
  }

  async fn upsert_system_section(&self, req: UpsertSystemSection) -> Result<ForumSection, UpsertSystemSectionError> {
    Handler::<UpsertSystemSection>::handle(self, req).await
  }
}

impl<T> ForumStore for T where
  T: Send
    + Sync
    + Handler<AddModerator>
    + Handler<DeleteModerator>
    + Handler<GetSections>
    + Handler<GetSectionMeta>
    + Handler<GetThreads>
    + Handler<GetThreadMeta>
    + Handler<CreateThread>
    + Handler<GetPosts>
    + Handler<CreatePost>
    + Handler<GetPost>
    + Handler<CreatePostRevision>
    + Handler<GetRoleGrants>
    + Handler<UpsertSystemSection>
{
}

/// Like [`Deref`], but the target has the bound [`ForumStore`]
pub trait ForumStoreRef: Send + Sync {
  type ForumStore: ForumStore + ?Sized;

  fn forum_store(&self) -> &Self::ForumStore;
}

impl<TyRef> ForumStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: ForumStore,
{
  type ForumStore = TyRef::Target;

  fn forum_store(&self) -> &Self::ForumStore {
    self.deref()
  }
}
