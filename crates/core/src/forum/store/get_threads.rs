use crate::core::{Listing, Request};
use crate::forum::{ForumSectionRef, RawForumThreadMeta};
use crate::types::WeakError;
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetThreads {
  pub section: ForumSectionRef,
  /// Thread offset
  pub offset: u32,
  /// Thread limit
  pub limit: u32,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetThreadsError {
  #[error("section not found")]
  NotFound,
  #[error(transparent)]
  Other(WeakError),
}

impl GetThreadsError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}

impl Request for GetThreads {
  type Response = Result<Listing<RawForumThreadMeta>, GetThreadsError>;
}
