use crate::core::{Instant, Request};
use crate::opentelemetry::TraceParent;
use crate::types::WeakError;
use crate::user::{CompleteSimpleUser, UserIdRef};
use std::error::Error;

#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct EnableUser {
  pub now: Instant,
  pub r#ref: UserIdRef,
  pub actor: UserIdRef,
  pub span: Option<TraceParent>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum EnableUserError {
  #[error("target user not found: {}", .0.id)]
  UserNotFound(UserIdRef),
  #[error(transparent)]
  Other(WeakError),
}

impl EnableUserError {
  pub fn other<E: Error>(e: E) -> Self {
    Self::Other(WeakError::wrap(e))
  }
}
#[cfg_attr(
  feature = "serde",
  derive(eternaltwin_serde_tools::Serialize, eternaltwin_serde_tools::Deserialize)
)]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Hash)]
pub struct EnableUserSummary {
  pub deleted_at: Option<Instant>,
  pub current: CompleteSimpleUser,
  pub username: Result<(), UserIdRef>,
}

impl Request for EnableUser {
  type Response = Result<EnableUserSummary, EnableUserError>;
}
