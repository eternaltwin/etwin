use crate::core::{Instant, Request};
use crate::oauth::TwinoidRefreshToken;
use crate::twinoid::TwinoidUserIdRef;
use crate::types::WeakError;
use thiserror::Error;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetTwinoidRefreshToken {
  pub time: Instant,
  pub twinoid_user: Option<TwinoidUserIdRef>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum GetTwinoidRefreshTokenError {
  #[error("no matching refresh token found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl Request for GetTwinoidRefreshToken {
  type Response = Result<TwinoidRefreshToken, GetTwinoidRefreshTokenError>;
}
