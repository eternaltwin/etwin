use eternaltwin_core::oauth::{
  OauthClient, OauthProviderStore, OauthProviderStoreRef, RawUpsertSystemOauthClientError, UpsertSystemClientOptions,
};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{RawGetShortUserError, UserStore, UserStoreRef};
use std::sync::Arc;
use thiserror::Error;

pub struct OauthService<TyOauthProviderStore, TyUserStore>
where
  TyOauthProviderStore: OauthProviderStoreRef,
  TyUserStore: UserStoreRef,
{
  oauth_provider_store: TyOauthProviderStore,
  user_store: TyUserStore,
}

pub type DynOauthService = OauthService<Arc<dyn OauthProviderStore>, Arc<dyn UserStore>>;

#[derive(Error, Debug)]
pub enum UpsertSystemClientError {
  #[error("failed to upsert system client")]
  Internal(RawUpsertSystemOauthClientError),
  #[error("failed to retrieve owner")]
  InternalGetOwner(RawGetShortUserError),
  #[error(transparent)]
  Other(WeakError),
}

impl<TyOauthProviderStore, TyUserStore> OauthService<TyOauthProviderStore, TyUserStore>
where
  TyOauthProviderStore: OauthProviderStoreRef,
  TyUserStore: UserStoreRef,
{
  pub fn new(oauth_provider_store: TyOauthProviderStore, user_store: TyUserStore) -> Self {
    Self {
      oauth_provider_store,
      user_store,
    }
  }

  pub async fn upsert_system_client(
    &self,
    options: &UpsertSystemClientOptions,
  ) -> Result<OauthClient, UpsertSystemClientError> {
    let client = self
      .oauth_provider_store
      .oauth_provider_store()
      .upsert_system_client(options)
      .await
      .map_err(UpsertSystemClientError::Internal)?;
    let owner = match client.owner {
      None => None,
      Some(owner_id) => Some(
        self
          .user_store
          .user_store()
          .get_short_user(&owner_id.into())
          .await
          .map_err(UpsertSystemClientError::InternalGetOwner)?,
      ),
    };
    Ok(OauthClient {
      id: client.id,
      key: client.key,
      display_name: client.display_name,
      app_uri: client.app_uri,
      callback_uri: client.callback_uri,
      owner,
    })
  }
}
