pub use eternaltwin_squirrel::{
  EmptyError, ForceCreateError, GetStateError, SchemaState, SchemaStateRef, SchemaVersion, SchemaVersionRef,
};
use eternaltwin_squirrel::{MigrationDirection, SchemaResolver};
use include_dir::{include_dir, Dir};
use lazy_static::lazy_static;
use sqlx::PgPool;
use std::error::Error;

pub mod schema;

const POSTGRES: Dir = include_dir!("$CARGO_MANIFEST_DIR/scripts");
#[cfg(feature = "sqlite")]
const SQLITE: Dir = include_dir!("$CARGO_MANIFEST_DIR/sqlite");

lazy_static! {
  static ref SQUIRREL: SchemaResolver = SchemaResolver::new(&POSTGRES);
}

pub async fn get_state(db: &PgPool) -> Result<SchemaStateRef<'static>, GetStateError> {
  SQUIRREL.get_state(db).await
}

pub async fn empty(db: &PgPool) -> Result<(), EmptyError> {
  SQUIRREL.empty(db).await
}

pub async fn sync(db: &PgPool) -> Result<(), Box<dyn Error>> {
  let current: SchemaStateRef<'_> = SQUIRREL.get_state(db).await?;
  let latest: SchemaVersionRef<'_> = SQUIRREL.get_latest();
  if current != SchemaStateRef::from(latest) {
    match SQUIRREL.create_migration(current, latest, MigrationDirection::UpgradeOnly) {
      Some(migration) => SQUIRREL.apply_migration(db, &migration).await,
      None => Err("failed to plan migration".into()),
    }
  } else {
    Ok(())
  }
}

pub async fn force_create_latest(db: &PgPool, void: bool) -> Result<(), ForceCreateError> {
  SQUIRREL.force_create_latest(db, void).await
}

pub async fn force_create(db: &PgPool, state: SchemaStateRef<'static>, void: bool) -> Result<(), ForceCreateError> {
  SQUIRREL.force_create(db, state, void).await
}

#[cfg(feature = "sqlite")]
pub async fn force_create_latest_sqlite(
  db: &sqlx::Pool<sqlx::Sqlite>,
) -> Result<(), eternaltwin_core::types::WeakError> {
  let schema = SQLITE
    .get_file("create/latest.sql")
    .expect("latest sqlite schema exists");
  let schema = schema.contents_utf8().expect("latest sqlite schema is valid UTF-8");
  sqlx::query(schema)
    .execute(db)
    .await
    .map(drop)
    .map_err(eternaltwin_core::types::WeakError::wrap)
}
