use crate::{BuiltinEternaltwinConfigProfile, ConfigSource, ConfigValue};
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct SqliteConfig<TyMeta> {
  pub file: ConfigValue<String, TyMeta>,
  pub max_connections: ConfigValue<u32, TyMeta>,
}

impl<TyMeta> SqliteConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch(mut self, config: SqliteConfigPatch, meta: TyMeta) -> Self {
    // todo: file path should be normalized based on source (relative to cwd or file)
    if let SimplePatch::Set(file) = config.file {
      self.file = ConfigValue::new_meta(file, meta.clone());
    }
    if let SimplePatch::Set(max_connections) = config.max_connections {
      self.max_connections = ConfigValue::new_meta(max_connections, meta.clone());
    }
    self
  }
}

impl SqliteConfig<ConfigSource> {
  pub(crate) fn default_for_profile(profile: BuiltinEternaltwinConfigProfile) -> Self {
    let meta = ConfigSource::Default;
    Self {
      file: ConfigValue::new_meta(format!("./eternaltwin.{}.sqlite", profile.as_str()), meta.clone()),
      max_connections: ConfigValue::new_meta(50, meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct SqliteConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub file: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub max_connections: SimplePatch<u32>,
}
