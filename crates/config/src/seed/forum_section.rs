use crate::ConfigValue;
use eternaltwin_core::core::LocaleId;
use eternaltwin_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct ForumSectionConfig<TyMeta> {
  pub display_name: ConfigValue<String, TyMeta>,
  pub locale: ConfigValue<Option<LocaleId>, TyMeta>,
}

impl<TyMeta> ForumSectionConfig<TyMeta>
where
  TyMeta: Clone,
{
  pub(crate) fn patch_ref(&mut self, config: PartialForumSectionConfig, meta: TyMeta) {
    if let SimplePatch::Set(display_name) = config.display_name {
      self.display_name = ConfigValue::new_meta(display_name, meta.clone());
    }
    if let SimplePatch::Set(locale) = config.locale {
      self.locale = ConfigValue::new_meta(locale, meta.clone());
    }
  }

  pub(crate) fn default_for_key(key: &str, meta: TyMeta) -> Self {
    Self {
      display_name: ConfigValue::new_meta(key.to_string(), meta.clone()),
      locale: ConfigValue::new_meta(None, meta.clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PartialForumSectionConfig {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub display_name: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub locale: SimplePatch<Option<LocaleId>>,
}
