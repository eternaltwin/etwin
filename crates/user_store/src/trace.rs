use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::Handler;
use eternaltwin_core::opentelemetry::TraceParent;
use eternaltwin_core::types::DisplayErrorChain;
use eternaltwin_core::user::store::enable_user::{EnableUser, EnableUserError, EnableUserSummary};
use eternaltwin_core::user::{
  CompleteSimpleUser, CreateUserOptions, GetShortUserOptions, GetUserOptions, GetUserTos, RawCreateUserError,
  RawDeleteUser, RawDeleteUserError, RawGetShortUserError, RawGetUser, RawGetUserError, RawGetUserResult,
  RawGetUserTosError, RawGetUserTosOptions, RawGetUserWithPasswordError, RawHardDeleteUser, RawHardDeleteUserError,
  RawUpdateUserError, RawUpdateUserOptions, RawUpdateUserTosError, RawUpdateUserTosOptions, RawUpsertSeedUserError,
  ShortUser, ShortUserWithPassword, UpsertSeedUserOptions, UserStore,
};
use opentelemetry::trace::{FutureExt, Status, TraceContextExt, Tracer};
use opentelemetry::{Context, KeyValue};
use std::borrow::Cow;

pub struct TraceUserStore<TyClock, TyTracer, TyUserStore> {
  clock: TyClock,
  tracer: TyTracer,
  inner: TyUserStore,
}

impl<TyClock, TyTracer, TyUserStore> TraceUserStore<TyClock, TyTracer, TyUserStore> {
  pub fn new(clock: TyClock, tracer: TyTracer, inner: TyUserStore) -> Self {
    Self { clock, tracer, inner }
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyUserStore> Handler<EnableUser> for TraceUserStore<TyClock, TyTracer, TyUserStore>
where
  TyClock: ClockRef,
  TyUserStore: UserStore + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, mut cmd: EnableUser) -> Result<EnableUserSummary, EnableUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::enable_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    cmd.span = Some(TraceParent::from_span_context(ocx.span().span_context()));
    let res = self.inner.enable_user(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.current.id.to_string()));
        span.set_attribute(KeyValue::new("username", format!("{:?}", v.current.id.to_string())));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyUserStore> UserStore for TraceUserStore<TyClock, TyTracer, TyUserStore>
where
  TyClock: ClockRef,
  TyUserStore: UserStore + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn create_user(&self, cmd: &CreateUserOptions) -> Result<CompleteSimpleUser, RawCreateUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::create_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.create_user(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new(
          "display_name.current.value",
          v.display_name.current.value.to_string(),
        ));
        span.set_attribute(KeyValue::new("created_at", v.created_at.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn upsert_seed_user(&self, cmd: &UpsertSeedUserOptions) -> Result<CompleteSimpleUser, RawUpsertSeedUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::upsert_seed_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.upsert_seed_user(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new(
          "display_name.current.value",
          v.display_name.current.value.to_string(),
        ));
        span.set_attribute(KeyValue::new("created_at", v.created_at.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_user(&self, query: &GetUserOptions) -> Result<RawGetUserResult, RawGetUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::get_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_user(query).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id().to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_short_user(&self, query: &GetShortUserOptions) -> Result<ShortUser, RawGetShortUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::get_short_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_short_user(query).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_user_with_password(
    &self,
    query: RawGetUser,
  ) -> Result<ShortUserWithPassword, RawGetUserWithPasswordError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::get_user_with_password")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_user_with_password(query).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn get_user_tos(&self, options: &RawGetUserTosOptions) -> Result<GetUserTos, RawGetUserTosError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::get_user_tos")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.get_user_tos(options).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new("is_tos_accepted", v.is_tos_accepted.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn update_user(&self, cmd: &RawUpdateUserOptions) -> Result<CompleteSimpleUser, RawUpdateUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::update_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.update_user(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new(
          "display_name.current.value",
          v.display_name.current.value.to_string(),
        ));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn create_user_tos(&self, options: &RawUpdateUserTosOptions) -> Result<(), RawUpdateUserTosError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::create_user_tos")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.create_user_tos(options).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn delete_user(&self, cmd: &RawDeleteUser) -> Result<CompleteSimpleUser, RawDeleteUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::delete_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.delete_user(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }

  async fn hard_delete_user(&self, cmd: &RawHardDeleteUser) -> Result<(), RawHardDeleteUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("UserStore::hard_delete_user")
        .with_attributes([KeyValue::new("req.user.id", cmd.user.id.to_string())])
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.hard_delete_user(cmd).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}
