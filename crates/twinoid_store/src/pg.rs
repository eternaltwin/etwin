use async_trait::async_trait;
use eternaltwin_constants::twinoid::{ACHIEVEMENTS, RESTRICTED_SITES, SITES, STATS};
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Date, Handler, HtmlFragment, Instant, LocaleId, PeriodLower, SecretString};
use eternaltwin_core::oauth::{
  RfcOauthAccessTokenKey, RfcOauthRefreshTokenKey, TwinoidAccessToken, TwinoidRefreshToken,
};
use eternaltwin_core::pg_num::PgU32;
use eternaltwin_core::temporal::{ForeignRetrieved, ForeignSnapshot, LatestTemporal};
use eternaltwin_core::twinoid::api::{Contact, Like, Missing, Site, SiteIcon, UrlRef, User, UserOldName};
use eternaltwin_core::twinoid::api::{Restrict, TwinoidGender};
use eternaltwin_core::twinoid::client::{AchievementMetadata, FullAchievement, FullSiteUser, FullStat, StatMetadata};
use eternaltwin_core::twinoid::client::{FullSite, FullSiteInfo};
use eternaltwin_core::twinoid::store::{MissingUserRelations, RawGetRewardsError, RawGetScoresError};
use eternaltwin_core::twinoid::{
  store, ArchivedTwinoidRewards, ArchivedTwinoidScore, ArchivedTwinoidScores, ArchivedTwinoidStats,
  ArchivedTwinoidStatsAndRewards, ShortTwinoidSite, TwinoidLink, TwinoidLinkUser, TwinoidLocale, TwinoidSiteHost,
  TwinoidSiteId, TwinoidSiteIdRef, TwinoidSiteUserId, TwinoidSiteUserIdRef, TwinoidUserIdRef,
};
use eternaltwin_core::twinoid::{ArchivedTwinoidUser, ShortTwinoidUser, TwinoidUserDisplayName, TwinoidUserId};
use eternaltwin_core::types::{DisplayErrorChain, WeakError};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternaltwin_postgres_tools::upsert_archive_query;
use sha3::{Digest, Sha3_256};
use sqlx::postgres::PgQueryResult;
use sqlx::types::Uuid;
use sqlx::{PgPool, Postgres, Transaction};
use std::collections::HashSet;
use std::collections::{BTreeMap, BTreeSet};
use std::str::FromStr;
use url::Url;

pub struct PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  database_secret: SecretString,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub async fn new(
    clock: TyClock,
    database: TyDatabase,
    database_secret: SecretString,
    uuid_generator: TyUuidGenerator,
  ) -> Result<Self, WeakError> {
    let mut tx = database.begin().await.map_err(WeakError::wrap)?;
    let now = clock.clock().now();
    populate_twinoid(&mut tx, now).await?;
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(Self {
      clock,
      database,
      database_secret,
      uuid_generator,
    })
  }
}

pub async fn populate_twinoid(tx: &mut Transaction<'_, Postgres>, now: Instant) -> Result<(), WeakError> {
  populate_twinoid_sites(tx, now).await?;
  populate_twinoid_stats(tx, now).await?;
  populate_twinoid_achievements(tx, now).await?;
  Ok(())
}

pub async fn populate_twinoid_sites(tx: &mut Transaction<'_, Postgres>, now: Instant) -> Result<(), WeakError> {
  let allowed_sites = SITES
    .iter()
    .map(eternaltwin_constants::twinoid::ConstFullSite::into_full_site);
  let restricted_sites = RESTRICTED_SITES.into_iter().map(|id: TwinoidSiteId| -> FullSite {
    Site {
      id,
      name: Restrict::Deny,
      host: Restrict::Deny,
      icon: Restrict::Deny,
      lang: Restrict::Deny,
      like: Restrict::Deny,
      infos: Restrict::Deny,
      me: Missing,
      status: Restrict::Deny,
    }
  });

  for site in allowed_sites.chain(restricted_sites) {
    touch_site(tx, now, site).await?;
  }
  Ok(())
}

pub async fn populate_twinoid_stats(tx: &mut Transaction<'_, Postgres>, now: Instant) -> Result<(), WeakError> {
  for stat in &STATS {
    let (site, stat) = stat.into_stat_metadata();
    touch_stat(tx, now, site, &stat).await.map_err(|e| {
      WeakError::new(format!(
        "failed to touch stat {:?}: {}",
        (site, stat.id),
        DisplayErrorChain(&e)
      ))
    })?;
  }
  Ok(())
}

pub async fn populate_twinoid_achievements(tx: &mut Transaction<'_, Postgres>, now: Instant) -> Result<(), WeakError> {
  for achievement in &ACHIEVEMENTS {
    let (site, achievement) = achievement.into_achievement_metadata();
    touch_achievement(tx, now, site, &achievement).await.map_err(|e| {
      WeakError::new(format!(
        "failed to touch achievement {:?}: {}",
        (site, achievement.id),
        DisplayErrorChain(&e)
      ))
    })?;
  }
  Ok(())
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetShortUser>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, req: store::GetShortUser) -> Result<ShortTwinoidUser, store::GetShortUserError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_user_id: PgU32,
      display_name: Option<TwinoidUserDisplayName>,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT twinoid_user_id, display_name
      FROM twinoid_user
        LEFT OUTER JOIN twinoid_user_display_name_history USING (twinoid_user_id)
      WHERE twinoid_user_id = $1::u32
        AND (twinoid_user_display_name_history.period IS NULL OR twinoid_user_display_name_history.period @> $2::instant);
    ",
    )
    .bind(PgU32::from(req.user.id.get()))
    .bind(req.time)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    let row = row.ok_or(store::GetShortUserError::NotFound)?;

    Ok(ShortTwinoidUser {
      id: TwinoidUserId::new(u32::from(row.twinoid_user_id)).map_err(WeakError::wrap)?,
      display_name: row
        .display_name
        .unwrap_or_else(|| TwinoidUserDisplayName::from_str("tidNameError").unwrap()),
    })
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetTwinoidAccessToken>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    query: store::GetTwinoidAccessToken,
  ) -> Result<TwinoidAccessToken, store::GetTwinoidAccessTokenError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_user_id: PgU32,
      ctime: Instant,
      atime: Instant,
      expired_at: Instant,
      key: String,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT twinoid_user_id, expired_at,
        LOWER(period) AS ctime, retrieved_at[array_upper(retrieved_at, 1)] AS atime,
        pgp_sym_decrypt(twinoid_access_token, $3::text)::text AS key
      FROM twinoid_access_token
      WHERE period @> $1::INSTANT
        AND $1::INSTANT < expired_at
        AND twinoid_user_id = COALESCE($2::twinoid_user_id, twinoid_user_id)
    ",
    )
    .bind(query.time)
    .bind(query.twinoid_user.map(|u| PgU32::from(u.id.get())))
    .bind(self.database_secret.as_str())
    .fetch_optional(&*self.database)
    .await
    .map_err(|e| store::GetTwinoidAccessTokenError::Other(WeakError::wrap(e)))?;

    let row = row.ok_or(store::GetTwinoidAccessTokenError::NotFound)?;

    let token = TwinoidAccessToken {
      key: RfcOauthAccessTokenKey::from_str(&row.key).map_err(WeakError::wrap)?,
      created_at: row.ctime,
      accessed_at: row.atime,
      expires_at: row.expired_at,
      twinoid_user_id: TwinoidUserId::new(u32::from(row.twinoid_user_id)).map_err(WeakError::wrap)?,
    };

    Ok(token)
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetTwinoidRefreshToken>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    query: store::GetTwinoidRefreshToken,
  ) -> Result<TwinoidRefreshToken, store::GetTwinoidRefreshTokenError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_user_id: PgU32,
      ctime: Instant,
      atime: Instant,
      key: String,
    }

    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT twinoid_user_id,
        LOWER(period) AS ctime, retrieved_at[array_upper(retrieved_at, 1)] AS atime,
        pgp_sym_decrypt(twinoid_refresh_token, $3::text)::text AS key
      FROM twinoid_refresh_token
      WHERE period @> $1::INSTANT
        AND twinoid_user_id = COALESCE($2::twinoid_user_id, twinoid_user_id)
    ",
    )
    .bind(query.time)
    .bind(query.twinoid_user.map(|u| PgU32::from(u.id.get())))
    .bind(self.database_secret.as_str())
    .fetch_optional(&*self.database)
    .await
    .map_err(|e| store::GetTwinoidRefreshTokenError::Other(WeakError::wrap(e)))?;

    let row = row.ok_or(store::GetTwinoidRefreshTokenError::NotFound)?;

    let token = TwinoidRefreshToken {
      key: RfcOauthRefreshTokenKey::from_str(&row.key).map_err(WeakError::wrap)?,
      created_at: row.ctime,
      accessed_at: row.atime,
      twinoid_user_id: TwinoidUserId::new(u32::from(row.twinoid_user_id)).map_err(WeakError::wrap)?,
    };

    Ok(token)
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetUser>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetUser) -> Result<ArchivedTwinoidUser, store::GetUserError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    let result = get_user(&mut tx, &query).await;
    tx.commit().await.map_err(WeakError::wrap)?;
    result
  }
}

async fn get_user(
  tx: &mut Transaction<'_, Postgres>,
  query: &store::GetUser,
) -> Result<ArchivedTwinoidUser, store::GetUserError> {
  #[derive(Debug, sqlx::FromRow)]
  struct UserRow {
    twinoid_user_id: PgU32,
    retrieved_at: Instant,
    display_name: Option<TwinoidUserDisplayName>,
  }
  #[derive(Debug, sqlx::FromRow)]
  struct LinkRow {
    twinoid_site_id: TwinoidSiteId,
    host: Option<TwinoidSiteHost>,
    twinoid_site_user_id: Option<TwinoidSiteUserId>,
    link_period: PeriodLower,
    link_retrieved_latest: Instant,
  }

  let user_row: UserRow = {
    // language=PostgreSQL
    let row: Option<UserRow> = sqlx::query_as::<_, UserRow>(
      r"
      SELECT twinoid_user_id, display_name, twinoid_user.retrieved_at
      FROM twinoid_user
        LEFT OUTER JOIN twinoid_user_display_name_history USING (twinoid_user_id)
      WHERE twinoid_user_id = $1::u32
        AND (
          twinoid_user_display_name_history.period IS NULL
          OR twinoid_user_display_name_history.period @> $2::instant
        );
    ",
    )
    .bind(PgU32::from(query.user.id.get()))
    .bind(query.time)
    .fetch_optional(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    row.ok_or(store::GetUserError::NotFound)?
  };

  // language=PostgreSQL
  let rows: Vec<LinkRow> = sqlx::query_as::<_, LinkRow>(
    r"
      SELECT twinoid_site_id, host, twinoid_site_user_id,
        twinoid_site_user_link.period AS link_period, twinoid_site_user_link.retrieved_at[CARDINALITY(twinoid_site_user_link.retrieved_at)] AS link_retrieved_latest
      FROM twinoid_site_user_link
        LEFT OUTER JOIN twinoid_site_history USING (twinoid_site_id)
      WHERE twinoid_user_id = $1::u32
        AND twinoid_site_user_link.period @> $2::instant
        AND twinoid_site_history.period @> $2::instant;
    ",
  )
  .bind(PgU32::from(query.user.id.get()))
  .bind(query.time)
  .fetch_all(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;

  let mut links: Vec<TwinoidLink> = Vec::new();
  for row in rows {
    let user = row.twinoid_site_user_id.map(|id| TwinoidLinkUser { id });
    let user = to_latest_temporal(row.link_period, row.link_retrieved_latest, user);
    links.push(TwinoidLink {
      site: ShortTwinoidSite {
        id: row.twinoid_site_id,
        host: row.host,
      },
      user,
    })
  }

  Ok(ArchivedTwinoidUser {
    id: TwinoidUserId::new(u32::from(user_row.twinoid_user_id)).map_err(WeakError::wrap)?,
    archived_at: user_row.retrieved_at,
    display_name: user_row
      .display_name
      .unwrap_or_else(|| TwinoidUserDisplayName::from_str("tidNameError").unwrap()),
    links,
  })
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetScores>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetScores) -> Result<ArchivedTwinoidScores, RawGetScoresError> {
    let mut tx = self.database.begin().await.map_err(RawGetScoresError::other)?;
    let result = get_scores(&mut tx, &query).await;
    tx.commit().await.map_err(RawGetScoresError::other)?;
    result
  }
}

async fn get_scores(
  tx: &mut Transaction<'_, Postgres>,
  query: &store::GetScores,
) -> Result<ArchivedTwinoidScores, RawGetScoresError> {
  #[derive(Debug, sqlx::FromRow)]
  struct ArchivedTwinoidScoreRow {
    pub twinoid_site_id: TwinoidSiteId,
    pub npoints: f64,
  }

  let rows: Vec<ArchivedTwinoidScoreRow> = sqlx::query_as::<_, ArchivedTwinoidScoreRow>(
    r"
      SELECT tsuh.twinoid_site_id, tsuh.npoints
      FROM twinoid_site_user_link tsul
        LEFT OUTER JOIN twinoid_site_user_history tsuh ON tsuh.twinoid_site_user_id = tsul.twinoid_site_user_id
      WHERE tsul.twinoid_user_id = $1::u32
        AND tsul.twinoid_site_id = tsuh.twinoid_site_id
        AND tsuh.npoints IS NOT NULL",
  )
  .bind(PgU32::from(query.user.id.get()))
  .fetch_all(&mut **tx)
  .await
  .map_err(RawGetScoresError::other)?;

  let scores: Vec<ArchivedTwinoidScore> = rows
    .iter()
    .map(|row| ArchivedTwinoidScore {
      site: TwinoidSiteIdRef {
        id: row.twinoid_site_id,
      },
      points: row.npoints,
    })
    .collect();

  Ok(ArchivedTwinoidScores { scores })
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::GetRewards>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, query: store::GetRewards) -> Result<ArchivedTwinoidStatsAndRewards, RawGetRewardsError> {
    let mut tx = self.database.begin().await.map_err(RawGetRewardsError::other)?;
    let rewards = get_rewards(&mut tx, &query).await;
    let stats = get_stats(&mut tx, &query).await;
    tx.commit().await.map_err(RawGetRewardsError::other)?;
    Ok(ArchivedTwinoidStatsAndRewards {
      rewards: rewards?,
      stats: stats?,
    })
  }
}

async fn get_rewards(
  tx: &mut Transaction<'_, Postgres>,
  query: &store::GetRewards,
) -> Result<Vec<ArchivedTwinoidRewards>, store::RawGetRewardsError> {
  #[derive(Debug, sqlx::FromRow)]
  struct ArchivedTwinoidRewardsRow {
    pub twinoid_achievement_key: String,
    pub npoints: f64,
  }

  let rows: Vec<ArchivedTwinoidRewardsRow> = sqlx::query_as::<_, ArchivedTwinoidRewardsRow>(
    r"
    SELECT tuasi.twinoid_achievement_key, ta.npoints
    FROM twinoid_site_user_link tsul
      INNER JOIN twinoid_site_user_history tsuh USING (twinoid_site_id, twinoid_site_user_id)
      INNER JOIN twinoid_user_achievement_set_item tuasi ON tuasi.twinoid_user_achievement_set_id = tsuh.achievements
      INNER JOIN twinoid_achievement ta USING (twinoid_achievement_key)
    WHERE tsul.twinoid_user_id = $1::u32
      AND tsul.twinoid_site_id = $2::u32",
  )
  .bind(PgU32::from(query.user.id.get()))
  .bind(PgU32::from(query.site.id.get()))
  .fetch_all(&mut **tx)
  .await
  .map_err(RawGetRewardsError::other)?;

  let response: Vec<ArchivedTwinoidRewards> = rows
    .iter()
    .map(|row| ArchivedTwinoidRewards {
      achievement_key: row.twinoid_achievement_key.clone(),
      points: row.npoints,
    })
    .collect();

  Ok(response)
}

async fn get_stats(
  tx: &mut Transaction<'_, Postgres>,
  query: &store::GetRewards,
) -> Result<Vec<ArchivedTwinoidStats>, RawGetRewardsError> {
  #[derive(Debug, sqlx::FromRow)]
  struct ArchivedTwinoidStatsRow {
    pub twinoid_stat_key: String,
    pub score: i32,
    pub rare: i32,
  }

  let rows: Vec<ArchivedTwinoidStatsRow> = sqlx::query_as::<_, ArchivedTwinoidStatsRow>(
    r"
    SELECT tusmi.twinoid_stat_key, tusmi.score, ts.rare
    FROM twinoid_site_user_link tsul
      INNER JOIN twinoid_site_user_history tsuh USING (twinoid_site_id, twinoid_site_user_id)
      INNER JOIN twinoid_user_stat_map_item tusmi ON tusmi.twinoid_user_stat_map_id = tsuh.stats
      INNER JOIN twinoid_stat ts ON ts.twinoid_site_id = tsuh.twinoid_site_id AND ts.twinoid_stat_key = tusmi.twinoid_stat_key
    WHERE tsul.twinoid_user_id = $1::u32
      AND tsul.twinoid_site_id = $2::u32",
  )
  .bind(PgU32::from(query.user.id.get()))
  .bind(PgU32::from(query.site.id.get()))
  .fetch_all(&mut **tx)
  .await
  .map_err(RawGetRewardsError::other)?;

  let response: Vec<ArchivedTwinoidStats> = rows
    .iter()
    .map(|row| ArchivedTwinoidStats {
      stat_key: row.twinoid_stat_key.clone(),
      score: row.score,
      rarity: row.rare,
    })
    .collect();

  Ok(response)
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::TouchTwinoidOauth>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, cmd: store::TouchTwinoidOauth) -> Result<(), store::TouchTwinoidOauthError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    let now = self.clock.clock().now();
    touch_user_id(&mut tx, now, cmd.twinoid_user.id)
      .await
      .map_err(WeakError::wrap)?;
    let access_token = TwinoidAccessToken {
      key: cmd.access_token,
      created_at: now,
      accessed_at: now,
      expires_at: cmd.expiration_time,
      twinoid_user_id: cmd.twinoid_user.id,
    };
    touch_twinoid_access_token(&mut tx, now, &access_token, &self.database_secret).await?;
    if let Some(refresh_token) = cmd.refresh_token {
      let refresh_token = TwinoidRefreshToken {
        key: refresh_token,
        created_at: now,
        accessed_at: now,
        twinoid_user_id: cmd.twinoid_user.id,
      };
      touch_twinoid_refresh_token(&mut tx, now, &refresh_token, &self.database_secret).await?;
    }
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::TouchShortUser>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, req: store::TouchShortUser) -> Result<(), store::TouchShortUserError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    touch_user_id(&mut tx, now, req.user.id).await?;
    touch_user_display_name(&mut tx, now, req.user.id, &req.user.display_name).await?;
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::TouchSite>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, req: store::TouchSite) -> Result<(), store::TouchSiteError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    touch_site(&mut tx, now, req.site).await?;
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::TouchFullUserList>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, req: store::TouchFullUserList) -> Result<(), store::TouchFullUserListError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    touch_user_list(self.uuid_generator.uuid_generator(), &mut tx, now, &req.users).await?;
    for user in &req.users {
      touch_user_display_name(&mut tx, now, user.id, &user.name).await?;
      touch_user_custom_fields(
        &mut tx,
        now,
        user.id,
        user.desc.as_ref(),
        user.status.as_ref(),
        &user.old_names,
        user.city.as_deref(),
        user.country.as_deref(),
      )
      .await?;
    }
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> Handler<store::TouchSafeUserList>
  for PgTwinoidStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(&self, req: store::TouchSafeUserList) -> Result<(), store::TouchSafeUserListError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    touch_user_list(self.uuid_generator.uuid_generator(), &mut tx, now, &req.users).await?;
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }
}

#[allow(clippy::type_complexity)]
async fn touch_user_list<
  TyUuidGenerator: UuidGenerator + ?Sized,
  TyName,
  TyOldNames,
  TyCity,
  TyCountry,
  TyDesc,
  TyStatus,
>(
  uuid_generator: &TyUuidGenerator,
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  users: &[User<
    TwinoidUserId,
    TyName,
    Option<UrlRef>,
    TwinoidLocale,
    Option<HtmlFragment>,
    TyOldNames,
    Vec<FullSiteUser>,
    Like<Url, u32>,
    Option<TwinoidGender>,
    Option<Date>,
    TyCity,
    TyCountry,
    TyDesc,
    TyStatus,
    Restrict<Vec<Contact<bool, Box<User>>>>,
    Missing,
    Missing,
  >],
) -> Result<(), WeakError> {
  let mut missing_relations = MissingUserRelations::from_users(users);
  let existing = get_existing_sites(tx, &missing_relations.sites)
    .await
    .map_err(WeakError::wrap)?;

  for site in existing {
    missing_relations.sites.remove(&site);
  }
  for stat in &STATS {
    missing_relations.stats.remove(&(stat.site_id, stat.key.to_string()));
  }
  for achievement in &ACHIEVEMENTS {
    missing_relations
      .achievements
      .remove(&(achievement.site_id, achievement.key.to_string()));
  }
  if !missing_relations.stats.is_empty() || !missing_relations.achievements.is_empty() {
    for u in users.iter() {
      for site_user in u.sites.iter() {
        let site = TwinoidSiteIdRef { id: site_user.site.id };
        if let Some(stats) = site_user.stats.as_ref() {
          for stat in stats {
            let stat_key = (site_user.site.id, stat.id.clone());
            if missing_relations.stats.contains(&stat_key) {
              touch_stat(tx, now, site, &stat.clone().into_stat_metadata()).await?;
              missing_relations.stats.remove(&stat_key);
            }
          }
        }
        if let Some(achievements) = site_user.achievements.as_ref() {
          for achievement in achievements {
            let achievement_key = (site_user.site.id, achievement.id.clone());
            if missing_relations.achievements.contains(&achievement_key) {
              touch_achievement(tx, now, site, &achievement.clone().into_achievement_metadata()).await?;
              missing_relations.achievements.remove(&achievement_key);
            }
          }
        }
      }
    }
  }

  if !missing_relations.is_empty() {
    return Err(WeakError::wrap(store::TouchSafeUserListError::MissingRelations(
      missing_relations,
    )));
  }

  let mut contacts: BTreeSet<(TwinoidUserId, TwinoidUserId)> = BTreeSet::new();
  let mut touched_user_ids = HashSet::new();
  for user in users.iter() {
    touch_user(uuid_generator, tx, now, user).await?;
    touched_user_ids.insert(user.id);
    match user.contacts.as_ref() {
      Restrict::Deny => {
        continue;
      }
      Restrict::Allow(clist) => {
        for contact in clist.iter() {
          if !touched_user_ids.contains(&contact.user.id) {
            touch_user_id(tx, now, contact.user.id).await?;
            touched_user_ids.insert(user.id);
          }
          contacts.insert((user.id, contact.user.id));
          if contact.friend {
            contacts.insert((contact.user.id, user.id));
          }
        }
      }
    }
  }
  for (sender, receiver) in contacts {
    touch_contact(tx, now, sender, receiver, true).await?;
  }
  Ok(())
}

#[allow(clippy::type_complexity)]
async fn touch_user<
  TyUuidGenerator: UuidGenerator + ?Sized,
  TyName,
  TyOldNames,
  TyCity,
  TyCountry,
  TyDesc,
  TyStatus,
>(
  uuid_generator: &TyUuidGenerator,
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: &User<
    TwinoidUserId,
    TyName,
    Option<UrlRef>,
    TwinoidLocale,
    Option<HtmlFragment>,
    TyOldNames,
    Vec<FullSiteUser>,
    Like<Url, u32>,
    Option<TwinoidGender>,
    Option<Date>,
    TyCity,
    TyCountry,
    TyDesc,
    TyStatus,
    Restrict<Vec<Contact<bool, Box<User>>>>,
    Missing,
    Missing,
  >,
) -> Result<(), WeakError> {
  let tid_user = TwinoidUserIdRef::from(user.id);
  touch_user_id(tx, now, user.id).await?;
  touch_user_history(
    tx,
    now,
    user.id,
    user.locale.into_locale_id(),
    user.picture.as_ref().map(|p| p.url.as_url()),
    user.title.as_ref(),
    &user.like.url,
    user.like.likes,
    user.gender,
    user.birthday,
  )
  .await?;
  let mut missing_sites: BTreeSet<TwinoidSiteId> = {
    let mut sites = BTreeSet::new();
    sites.extend(SITES.iter().map(|s| s.id));
    sites.extend(RESTRICTED_SITES.into_iter());
    sites
  };
  for site_user in user.sites.iter() {
    let site_user_id = match site_user.real_id {
      Some(id) => id,
      None => {
        // Can happen if the account was linked at some point, and then unlinked
        // or deleted.
        continue;
      }
    };
    let site = TwinoidSiteIdRef::from(site_user.site.id);
    let site_user_ref = TwinoidSiteUserIdRef { site, id: site_user_id };
    touch_site_user_id(tx, now, site_user_ref).await?;
    touch_site_user_link(tx, now, tid_user, site, Some(site_user_ref.id)).await?;
    touch_site_user_history(
      uuid_generator,
      tx,
      now,
      site_user_ref,
      site_user.link.as_deref(),
      site_user.stats.as_deref(),
      site_user.achievements.as_deref(),
      site_user.points,
      site_user.npoints,
    )
    .await?;
    missing_sites.remove(&site_user.site.id);
  }
  for missing_site in missing_sites {
    touch_site_user_link(tx, now, tid_user, missing_site.into(), None).await?;
  }
  Ok(())
}

async fn touch_user_id(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  tid_uid: TwinoidUserId,
) -> Result<(), WeakError> {
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO twinoid_user(twinoid_user_id, retrieved_at)
    VALUES ($1::twinoid_user_id, $2::instant)
    ON CONFLICT DO NOTHING;
  "#,
  )
  .bind(PgU32::from(tid_uid.get()))
  .bind(now)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);
  Ok(())
}

async fn touch_twinoid_access_token(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  token: &TwinoidAccessToken,
  db_secret: &SecretString,
) -> Result<(), WeakError> {
  {
    let hash = Sha3_256::digest(token.key.as_str().as_bytes());
    let hash = hash.as_slice();

    let res: PgQueryResult = sqlx::query(upsert_archive_query!(
      twinoid_access_token(
        time($1 period, retrieved_at),
        primary($2 _twinoid_access_token_hash::BYTEA),
        data($3 twinoid_user_id::twinoid_user_id, $4 expired_at::instant),
        secret($5, $6 twinoid_access_token),
        unique(user_token(twinoid_user_id)),
      )
    ))
    .bind(now)
    .bind(hash)
    .bind(PgU32::from(token.twinoid_user_id.get()))
    .bind(token.expires_at)
    .bind(db_secret.as_str())
    .bind(token.key.as_str())
    .execute(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;
    // Affected row counts:
    // 1 : 1 updated (matching data)
    // 1 : 1 inserted (first insert)
    // 2 : 1 inserted (first insert), 1 invalidated (user_session)
    // 2 : 1 inserted (data change), 1 invalidated (primary)
    // 3 : 1 inserted (data change), 1 invalidated (primary), 1 invalidated (user_token)
    assert!((1..=3u64).contains(&res.rows_affected()));
  }

  Ok(())
}

async fn touch_twinoid_refresh_token(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  token: &TwinoidRefreshToken,
  db_secret: &SecretString,
) -> Result<(), WeakError> {
  {
    let hash = Sha3_256::digest(token.key.as_str().as_bytes());
    let hash = hash.as_slice();

    let res: PgQueryResult = sqlx::query(upsert_archive_query!(
      twinoid_refresh_token(
        time($1 period, retrieved_at),
        primary($2 _twinoid_refresh_token_hash::BYTEA),
        data($3 twinoid_user_id::twinoid_user_id),
        secret($4, $5 twinoid_refresh_token),
        unique(user_token(twinoid_user_id)),
      )
    ))
    .bind(now)
    .bind(hash)
    .bind(PgU32::from(token.twinoid_user_id.get()))
    .bind(db_secret.as_str())
    .bind(token.key.as_str())
    .execute(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;
    // Affected row counts:
    // 1 : 1 updated (matching data)
    // 1 : 1 inserted (first insert)
    // 2 : 1 inserted (first insert), 1 invalidated (user_session)
    // 2 : 1 inserted (data change), 1 invalidated (primary)
    // 3 : 1 inserted (data change), 1 invalidated (primary), 1 invalidated (user_token)
    assert!((1..=3u64).contains(&res.rows_affected()));
  }

  Ok(())
}

async fn touch_site_user_id(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  site_user: TwinoidSiteUserIdRef,
) -> Result<(), WeakError> {
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO twinoid_site_user(twinoid_site_id, twinoid_site_user_id, retrieved_at)
    VALUES ($1::twinoid_site_id, $2::twinoid_site_user_id, $3::instant)
    ON CONFLICT DO NOTHING;
  "#,
  )
  .bind(site_user.site.id)
  .bind(site_user.id)
  .bind(now)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);
  Ok(())
}

async fn touch_contact(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: TwinoidUserId,
  target: TwinoidUserId,
  is_contact: bool,
) -> Result<(), WeakError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_contact(
      time($1 period, retrieved_at),
      primary($2 twinoid_user_id::twinoid_user_id, $3 target::twinoid_user_id),
      data(
        $4 is_contact::bool
      ),
    )
  ))
  .bind(now)
  .bind(PgU32::from(user.get()))
  .bind(PgU32::from(target.get()))
  .bind(is_contact)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 2);
  Ok(())
}

#[allow(clippy::too_many_arguments)]
async fn touch_user_history(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: TwinoidUserId,
  locale: LocaleId,
  picture: Option<&Url>,
  title: Option<&HtmlFragment>,
  like_url: &Url,
  like_count: u32,
  gender: Option<TwinoidGender>,
  birthday: Option<Date>,
) -> Result<(), WeakError> {
  if let Some(picture) = picture {
    touch_remote_image(tx, now, picture).await?;
  }

  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_user_history(
      time($1 period, retrieved_at),
      primary($2 twinoid_user_id::twinoid_user_id),
      data(
        $3 picture::url,
        $4 locale::locale_id,
        $5 title::html_fragment,
        $6 like_url::url,
        $7 like_count::u32,
        $8 gender::twinoid_gender,
        $9 birthday::date,
      ),
    )
  ))
  .bind(now)
  .bind(PgU32::from(user.get()))
  .bind(picture.map(|u| u.as_str()))
  .bind(locale)
  .bind(title)
  .bind(like_url.as_str())
  .bind(PgU32::from(like_count))
  .bind(gender)
  .bind(birthday.map(|b| b.into_chrono()))
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 2);
  Ok(())
}

#[allow(clippy::too_many_arguments)]
async fn touch_user_custom_fields(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: TwinoidUserId,
  description: Option<&HtmlFragment>,
  status: Option<&HtmlFragment>,
  old_names: &[UserOldName],
  city: Option<&str>,
  country: Option<&str>,
) -> Result<(), WeakError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_user_custom_fields(
      time($1 period, retrieved_at),
      primary($2 twinoid_user_id::twinoid_user_id),
      data(
        $3 description::html_fragment,
        $4 status::html_fragment,
        $5 old_names::old_twinoid_user_display_name_array,
        $6 city::text,
        $7 country::text,
      ),
    )
  ))
  .bind(now)
  .bind(PgU32::from(user.get()))
  .bind(description)
  .bind(status)
  .bind(old_names)
  .bind(city)
  .bind(country)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 2);
  Ok(())
}

async fn touch_site_user_link(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  tid_user: TwinoidUserIdRef,
  site: TwinoidSiteIdRef,
  site_user_id: Option<TwinoidSiteUserId>,
) -> Result<(), WeakError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_site_user_link(
      time($1 period, retrieved_at),
      primary($2 twinoid_user_id::twinoid_user_id, $3 twinoid_site_id::twinoid_site_id),
      data(
        $4 twinoid_site_user_id::twinoid_site_user_id,
      ),
      unique(site_user(twinoid_site_id, twinoid_site_user_id)),
    )
  ))
  .bind(now)
  .bind(PgU32::from(tid_user.id.get()))
  .bind(site.id)
  .bind(site_user_id)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 4);
  Ok(())
}

#[allow(clippy::too_many_arguments)]
async fn touch_site_user_history<TyUuid: UuidGenerator + ?Sized>(
  uuid_generator: &TyUuid,
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  site_user: TwinoidSiteUserIdRef,
  link: Option<&str>,
  stats: Option<&[FullStat]>,
  achievements: Option<&[FullAchievement]>,
  points: Option<i32>,
  npoints: Option<f64>,
) -> Result<(), WeakError> {
  let stats = if let Some(stats) = stats {
    let stat_map: BTreeMap<String, i32> = stats.iter().map(|s| (s.id.clone(), s.score)).collect();
    let map_id = touch_stat_map(tx, site_user.site, &stat_map, uuid_generator.next()).await?;
    Some(map_id)
  } else {
    None
  };
  let achievements: Option<Uuid> = if let Some(achievements) = achievements {
    let mut achievement_set: BTreeSet<String> = BTreeSet::new();
    for achievement in achievements {
      achievement_set.insert(achievement.id.clone());
      touch_user_achievement_unlock_time(tx, site_user, &achievement.id, achievement.date.into_instant()).await?;
    }
    let set_id = touch_achievement_set(tx, site_user.site, &achievement_set, uuid_generator.next()).await?;
    Some(set_id)
  } else {
    None
  };

  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_site_user_history(
      time($1 period, retrieved_at),
      primary($2 twinoid_site_id::twinoid_site_id, $3 twinoid_site_user_id::twinoid_site_user_id,),
      data(
        $4 link::text,
        $5 stats::uuid,
        $6 achievements::uuid,
        $7 points::i32,
        $8 npoints::float8,
      ),
    )
  ))
  .bind(now)
  .bind(site_user.site.id)
  .bind(site_user.id)
  .bind(link)
  .bind(stats)
  .bind(achievements)
  .bind(points)
  .bind(npoints)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 2);
  Ok(())
}

async fn touch_stat_map(
  tx: &mut Transaction<'_, Postgres>,
  site: TwinoidSiteIdRef,
  stats: &BTreeMap<String, i32>,
  new_id: Uuid,
) -> Result<Uuid, WeakError> {
  let hash = {
    let json = serde_json::to_string(&(site.id, stats)).unwrap();
    let hash = Sha3_256::digest(json.as_bytes());
    hash
  };

  let map_id = {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_user_stat_map_id: Uuid,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH
        input_row(twinoid_user_stat_map_id, twinoid_site_id, _sha3_256) AS (
          VALUES($1::twinoid_user_stat_map_id, $2::twinoid_site_id, $3::bytea)
        ),
        inserted_rows AS (
          INSERT
          INTO twinoid_user_stat_map(twinoid_user_stat_map_id, twinoid_site_id, _sha3_256)
          SELECT * FROM input_row
          ON CONFLICT DO NOTHING
          RETURNING twinoid_user_stat_map_id
        )
      SELECT twinoid_user_stat_map_id FROM inserted_rows
      UNION ALL
      SELECT old.twinoid_user_stat_map_id FROM twinoid_user_stat_map AS old INNER JOIN input_row USING(_sha3_256);
      ",
    )
    .bind(new_id)
    .bind(site.id)
    .bind(hash.as_slice())
    .fetch_one(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    row.twinoid_user_stat_map_id
  };

  if map_id == new_id {
    // Newly created map: fill its content
    for (stat_key, score) in stats {
      // language=PostgreSQL
      let res: PgQueryResult = sqlx::query(
        r"
        INSERT
        INTO twinoid_user_stat_map_item(twinoid_user_stat_map_id, twinoid_site_id, twinoid_stat_key, score)
        VALUES ($1::twinoid_user_stat_map_id, $2::twinoid_site_id, $3::twinoid_stat_key, $4::i32);
      ",
      )
      .bind(map_id)
      .bind(site.id)
      .bind(stat_key)
      .bind(score)
      .execute(&mut **tx)
      .await
      .map_err(WeakError::wrap)?;
      assert_eq!(res.rows_affected(), 1);
    }
  } else {
    // Re-using old id, check for hash collision
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_stat_key: String,
      score: i32,
    }

    // language=PostgreSQL
    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT twinoid_stat_key, score
      FROM twinoid_user_stat_map_item
      WHERE twinoid_user_stat_map_id = $1::twinoid_user_stat_map_id;
    ",
    )
    .bind(map_id)
    .fetch_all(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    let actual: BTreeMap<String, i32> = rows.into_iter().map(|r| (r.twinoid_stat_key, r.score)).collect();

    assert_eq!(actual, *stats);
  }

  Ok(map_id)
}

async fn touch_user_achievement_unlock_time(
  tx: &mut Transaction<'_, Postgres>,
  site_user: TwinoidSiteUserIdRef,
  achievement_key: &str,
  time: Instant,
) -> Result<(), WeakError> {
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO twinoid_user_achievement_unlock_time(twinoid_site_id, twinoid_site_user_id, twinoid_achievement_key, unlocked_at)
    VALUES ($1::twinoid_site_id, $2::twinoid_site_user_id, $3::twinoid_achievement_key, $4::instant)
    ON CONFLICT DO NOTHING;
  "#,
  )
    .bind(site_user.site.id)
    .bind(site_user.id)
    .bind(achievement_key)
    .bind(time)
    .execute(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);
  Ok(())
}

async fn touch_achievement_set(
  tx: &mut Transaction<'_, Postgres>,
  site: TwinoidSiteIdRef,
  achievements: &BTreeSet<String>,
  new_id: Uuid,
) -> Result<Uuid, WeakError> {
  let hash = {
    let json = serde_json::to_string(&(site.id, achievements)).unwrap();
    let hash = Sha3_256::digest(json.as_bytes());
    hash
  };

  let set_id = {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_user_achievement_set_id: Uuid,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      WITH
        input_row(twinoid_user_achievement_set_id, twinoid_site_id, _sha3_256) AS (
          VALUES($1::twinoid_user_achievement_set_id, $2::twinoid_site_id, $3::bytea)
        ),
        inserted_rows AS (
          INSERT
          INTO twinoid_user_achievement_set(twinoid_user_achievement_set_id, twinoid_site_id, _sha3_256)
          SELECT * FROM input_row
          ON CONFLICT DO NOTHING
          RETURNING twinoid_user_achievement_set_id
        )
      SELECT twinoid_user_achievement_set_id FROM inserted_rows
      UNION ALL
      SELECT old.twinoid_user_achievement_set_id FROM twinoid_user_achievement_set AS old INNER JOIN input_row USING(_sha3_256);
      ",
    )
    .bind(new_id)
    .bind(site.id)
    .bind(hash.as_slice())
    .fetch_one(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    row.twinoid_user_achievement_set_id
  };

  if set_id == new_id {
    // Newly created set: fill its content
    for achievement_key in achievements {
      // language=PostgreSQL
      let res: PgQueryResult = sqlx::query(
        r"
        INSERT
        INTO twinoid_user_achievement_set_item(twinoid_user_achievement_set_id, twinoid_site_id, twinoid_achievement_key)
        VALUES ($1::twinoid_user_achievement_set_id, $2::twinoid_site_id, $3::twinoid_achievement_key);
      ",
      )
      .bind(set_id)
      .bind(site.id)
      .bind(achievement_key)
      .execute(&mut **tx)
      .await
      .map_err(WeakError::wrap)?;
      assert_eq!(res.rows_affected(), 1);
    }
  } else {
    // Re-using old id, check for hash collision
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      twinoid_achievement_key: String,
    }

    // language=PostgreSQL
    let rows: Vec<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT twinoid_achievement_key
      FROM twinoid_user_achievement_set_item
      WHERE twinoid_user_achievement_set_id = $1::twinoid_user_achievement_set_id;
    ",
    )
    .bind(set_id)
    .fetch_all(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    let actual: BTreeSet<String> = rows.into_iter().map(|r| r.twinoid_achievement_key).collect();

    assert_eq!(actual, *achievements);
  }

  Ok(set_id)
}

#[allow(unused)]
async fn touch_user_display_name(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  user: TwinoidUserId,
  name: &TwinoidUserDisplayName,
) -> Result<(), WeakError> {
  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_user_display_name_history(
      time($1 period, retrieved_at),
      primary($2 twinoid_user_id::twinoid_user_id),
      data(
        $3 display_name::twinoid_user_display_name
      ),
    )
  ))
  .bind(now)
  .bind(PgU32::from(user.get()))
  .bind(name)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 2);
  Ok(())
}

async fn get_existing_sites(
  tx: &mut Transaction<'_, Postgres>,
  sites: &BTreeSet<TwinoidSiteId>,
) -> Result<Vec<TwinoidSiteId>, WeakError> {
  let sites: Vec<i64> = sites.iter().map(|id| i64::from(id.get())).collect();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    twinoid_site_id: TwinoidSiteId,
  }

  // language=PostgreSQL
  let rows: Vec<Row> = sqlx::query_as::<_, Row>(
    r"
      SELECT twinoid_site_id
      FROM twinoid_site
      WHERE array_position($1::int8[]::twinoid_site_id[], twinoid_site_id) IS NOT NULL;
    ",
  )
  .bind(sites)
  .fetch_all(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  Ok(rows.into_iter().map(|r| r.twinoid_site_id).collect())
}

async fn touch_site(tx: &mut Transaction<'_, Postgres>, now: Instant, site: FullSite) -> Result<(), WeakError> {
  use Restrict::*;

  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO twinoid_site(twinoid_site_id, retrieved_at)
    VALUES ($1::twinoid_site_id, $2::instant)
    ON CONFLICT DO NOTHING;
  "#,
  )
  .bind(site.id)
  .bind(now)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);

  if let Allow(icon) = site.icon.as_ref() {
    touch_remote_image(tx, now, icon.url.as_url()).await?;
  }

  match (site.name, site.host, site.icon, site.lang, site.like, site.status) {
    (Allow(name), Allow(host), Allow(icon), Allow(lang), Allow(like), Allow(status)) => {
      let res: PgQueryResult = sqlx::query(upsert_archive_query!(
        twinoid_site_history(
          time($1 period, retrieved_at),
          primary($2 twinoid_site_id::twinoid_site_id),
          data(
            $3 name::twinoid_site_name, $4 host::twinoid_site_host, $5 icon::url,
            $6 locale::locale_id, $7 like_url::url, $8 like_title::html_fragment,
            $9 status::twinoid_site_status
          ),
        )
      ))
      .bind(now)
      .bind(site.id)
      .bind(name)
      .bind(host)
      .bind(icon.url.as_url().to_string())
      .bind(lang.map(TwinoidLocale::into_locale_id))
      .bind(like.url.to_string())
      .bind(like.title)
      .bind(status)
      .execute(&mut **tx)
      .await
      .map_err(WeakError::wrap)?;
      // Affected row counts:
      // 1: 1 updated (matching data)
      // 1: 1 inserted (first insert)
      assert!(res.rows_affected() <= 2);
    }
    (Deny, Deny, Deny, Deny, Deny, Deny) => {
      // Nothing to do
    }
    _ => {
      return Err(WeakError::new(
        "unexpected combination of present and missing `Site` fields",
      ))
    }
  }

  if let Allow(infos) = site.infos {
    for info in infos {
      touch_site_info(tx, now, site.id, info).await?;
    }
  }

  Ok(())
}

async fn touch_site_info(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  site_id: TwinoidSiteId,
  site_info: FullSiteInfo,
) -> Result<(), WeakError> {
  touch_remote_image(tx, now, site_info.cover.url.as_url()).await?;

  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_site_info(
      time($1 period, retrieved_at),
      primary($2 twinoid_site_info_id::twinoid_site_info_id),
      data(
        $3 twinoid_site_id::twinoid_site_id, $4 locale::locale_id, $5 cover::url,
        $6 tag_line::html_fragment, $7 description::html_fragment, $8 tid::html_fragment
      ),
    )
  ))
  .bind(now)
  .bind(site_info.id)
  .bind(site_id)
  .bind(site_info.lang.into_locale_id())
  .bind(site_info.cover.url.as_url().to_string())
  .bind(site_info.tag_line)
  .bind(site_info.description)
  .bind(site_info.tid)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  // Affected row counts:
  // 1 : 1 updated (matching data)
  // 1 : 1 inserted (first insert)
  assert!(res.rows_affected() <= 2);

  for (rank, icon) in site_info.icons.iter().enumerate() {
    let rank = u32::try_from(rank).map_err(WeakError::wrap)?;
    touch_site_icon(tx, now, site_id, icon, rank).await?;
  }

  Ok(())
}

async fn touch_site_icon(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  site_id: TwinoidSiteId,
  icon: &SiteIcon,
  rank: u32,
) -> Result<(), WeakError> {
  touch_remote_image(tx, now, icon.url.as_url()).await?;

  let res: PgQueryResult = sqlx::query(upsert_archive_query!(
    twinoid_site_icon(
      time($1 period, retrieved_at),
      primary($2 twinoid_site_id::twinoid_site_id, $3 tag::twinoid_site_icon_tag),
      data(
        $4 alt_tag::twinoid_site_icon_tag, $5 url::url, $6 type::twinoid_site_icon_type,
        $7 _rank::u32
      ),
    )
  ))
  .bind(now)
  .bind(site_id)
  .bind(&icon.tag)
  .bind(icon.alt_tag.as_ref())
  .bind(icon.url.as_url().as_str())
  .bind(&icon.typ)
  .bind(PgU32::from(rank))
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  // Affected row counts:
  // 1: 1 updated (matching data)
  // 1: 1 inserted (first insert)
  assert!(res.rows_affected() <= 2);

  Ok(())
}

async fn touch_stat(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  site: TwinoidSiteIdRef,
  stat: &StatMetadata,
) -> Result<(), WeakError> {
  if let Some(icon) = stat.icon.as_ref() {
    touch_remote_image(tx, now, icon.url.as_url()).await?;
  }

  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO twinoid_stat(twinoid_site_id, twinoid_stat_key, name, description, icon, rare, social)
    VALUES ($1::twinoid_site_id, $2::twinoid_stat_key, $3::text, $4::html_fragment, $5::url, $6::i32, $7::boolean)
    ON CONFLICT DO NOTHING;
  "#,
  )
  .bind(site.id)
  .bind(stat.id.as_str())
  .bind(stat.name.as_str())
  .bind(stat.description.as_deref())
  .bind(stat.icon.as_ref().map(|i| i.url.as_url().to_string()))
  .bind(stat.rare)
  .bind(stat.social)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);

  Ok(())
}

async fn touch_achievement(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  site: TwinoidSiteIdRef,
  achievement: &AchievementMetadata,
) -> Result<(), WeakError> {
  if let Some(icon) = achievement.data.url.as_ref() {
    touch_remote_image(tx, now, icon.as_url()).await?;
  }

  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO twinoid_achievement(twinoid_site_id, twinoid_achievement_key, name, stat, score, points, npoints, description, "rank", data_type, data_title, data_url, data_prefix, data_suffix)
    VALUES (
      $1::twinoid_site_id, $2::twinoid_stat_key,
      $3::text, $4::twinoid_stat_key,
      $5::i32, $6::i32, $7::float8,
      $8::html_fragment, $9::i32,
      $10::text, $11::html_fragment, $12::url, $13::boolean, $14::boolean)
    ON CONFLICT DO NOTHING;
  "#,
  )
  .bind(site.id)
  .bind(achievement.id.as_str())
  .bind(achievement.name.as_str())
  .bind(achievement.stat.as_str())
  .bind(achievement.score)
  .bind(achievement.points)
  .bind(achievement.npoints)
  .bind(achievement.description.as_str())
  .bind(achievement.index)
  .bind(achievement.data.typ.as_str())
  .bind(achievement.data.title.as_deref())
  .bind(achievement.data.url.as_ref().map(|u| u.as_url().to_string()))
  .bind(achievement.data.prefix)
  .bind(achievement.data.suffix)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);

  Ok(())
}

async fn touch_remote_image(tx: &mut Transaction<'_, Postgres>, now: Instant, url: &Url) -> Result<(), WeakError> {
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
    r#"
    INSERT INTO remote_image(url, created_at)
    VALUES ($1::url, $2::instant)
    ON CONFLICT DO NOTHING;
  "#,
  )
  .bind(url.as_str())
  .bind(now)
  .execute(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;
  assert!(res.rows_affected() <= 1);
  Ok(())
}

fn to_latest_temporal<T>(period: PeriodLower, latest: Instant, value: T) -> LatestTemporal<T> {
  LatestTemporal {
    latest: ForeignSnapshot {
      period,
      retrieved: ForeignRetrieved { latest },
      value,
    },
  }
}

#[cfg(test)]
mod test {
  use super::PgTwinoidStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::twinoid::TwinoidStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn TwinoidStore>> {
    let config = eternaltwin_config::Config::for_test();

    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let database_secret = SecretString::new("dev_secret".to_string());
    let twinoid_store: Arc<dyn TwinoidStore> = Arc::new(
      PgTwinoidStore::new(
        Arc::clone(&clock),
        Arc::clone(&database),
        database_secret,
        Uuid4Generator,
      )
      .await
      .unwrap(),
    );

    TestApi { clock, twinoid_store }
  }

  #[tokio::test]
  #[serial]
  async fn test_empty() {
    crate::test::test_empty(make_test_api().await).await;
  }

  #[tokio::test]
  #[serial]
  async fn test_touch_user() {
    crate::test::test_touch_user(make_test_api().await).await;
  }

  #[tokio::test]
  #[serial]
  async fn test_get_missing_user() {
    crate::test::test_get_missing_user(make_test_api().await).await;
  }

  #[tokio::test]
  #[serial]
  async fn test_touch_drpg_fr() {
    crate::test::test_touch_drpg_fr(make_test_api().await).await;
  }

  #[tokio::test]
  #[serial]
  async fn test_touch_all_safe_users() {
    crate::test::test_touch_all_safe_users(make_test_api().await).await;
  }

  #[tokio::test]
  #[serial]
  async fn test_touch_all_full_users() {
    crate::test::test_touch_all_full_users(make_test_api().await).await;
  }
}
