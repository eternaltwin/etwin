use std::borrow::Cow;

use async_trait::async_trait;
use eternaltwin_core::{
  clock::{Clock, ClockRef},
  core::Handler,
  oauth::{TwinoidAccessToken, TwinoidRefreshToken},
  twinoid::{
    store::{self},
    ArchivedTwinoidScores, ArchivedTwinoidStatsAndRewards, ArchivedTwinoidUser, ShortTwinoidUser,
  },
  types::DisplayErrorChain,
};
use opentelemetry::{
  trace::{FutureExt, Status, TraceContextExt, Tracer},
  Context, KeyValue,
};

pub struct TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore> {
  clock: TyClock,
  tracer: TyTracer,
  inner: TyTwinoidStore,
}

impl<TyClock, TyTracer, TyTwinoidStore> TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore> {
  pub fn new(clock: TyClock, tracer: TyTracer, inner: TyTwinoidStore) -> Self {
    Self { clock, tracer, inner }
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::GetShortUser>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::GetShortUser> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::GetShortUser) -> Result<ShortTwinoidUser, store::GetShortUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::get_short_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new("display_name", v.display_name.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::GetTwinoidAccessToken>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::GetTwinoidAccessToken> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(
    &self,
    req: store::GetTwinoidAccessToken,
  ) -> Result<TwinoidAccessToken, store::GetTwinoidAccessTokenError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::get_twinoid_access_token")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("twinoid_id", v.twinoid_user_id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::GetTwinoidRefreshToken>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::GetTwinoidRefreshToken> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(
    &self,
    req: store::GetTwinoidRefreshToken,
  ) -> Result<TwinoidRefreshToken, store::GetTwinoidRefreshTokenError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::get_twinoid_refresh_token")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("twinoid_id", v.twinoid_user_id.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::GetUser> for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::GetUser> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::GetUser) -> Result<ArchivedTwinoidUser, store::GetUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::get_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(v) => {
        span.set_attribute(KeyValue::new("id", v.id.to_string()));
        span.set_attribute(KeyValue::new("display_name", v.display_name.to_string()));
        Status::Ok
      }
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::GetScores>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::GetScores> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::GetScores) -> Result<ArchivedTwinoidScores, store::RawGetScoresError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::get_scores")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::GetRewards>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::GetRewards> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::GetRewards) -> Result<ArchivedTwinoidStatsAndRewards, store::RawGetRewardsError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::get_rewards")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::TouchTwinoidOauth>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::TouchTwinoidOauth> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::TouchTwinoidOauth) -> Result<(), store::TouchTwinoidOauthError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::touch_twinoid_oauth")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::TouchShortUser>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::TouchShortUser> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::TouchShortUser) -> Result<(), store::TouchShortUserError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::touch_short_user")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::TouchSite>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::TouchSite> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::TouchSite) -> Result<(), store::TouchSiteError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::touch_site")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::TouchFullUserList>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::TouchFullUserList> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::TouchFullUserList) -> Result<(), store::TouchFullUserListError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::touch_full_user_list")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}

#[async_trait]
impl<TyClock, TyTracer, TyTwinoidStore> Handler<store::TouchSafeUserList>
  for TraceTwinoidStore<TyClock, TyTracer, TyTwinoidStore>
where
  TyClock: ClockRef,
  TyTwinoidStore: Handler<store::TouchSafeUserList> + Sync,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn handle(&self, req: store::TouchSafeUserList) -> Result<(), store::TouchSafeUserListError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("TwinoidStore::touch_safe_user_list")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self.inner.handle(req).with_context(ocx.clone()).await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{}", DisplayErrorChain(e))),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}
