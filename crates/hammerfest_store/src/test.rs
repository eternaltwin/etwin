use eternaltwin_core::api::SyncRef;
use eternaltwin_core::clock::{Clock, VirtualClock};
use eternaltwin_core::core::{Duration, HtmlFragment, Instant, PeriodLower};
use eternaltwin_core::hammerfest::{
  GetActiveSession, GetHammerfestUserOptions, HammerfestDate, HammerfestDateTime, HammerfestForumPost,
  HammerfestForumPostAuthor, HammerfestForumPostId, HammerfestForumPostListing, HammerfestForumRole,
  HammerfestForumThemeId, HammerfestForumThemePage, HammerfestForumThemePageResponse, HammerfestForumThemeTitle,
  HammerfestForumThread, HammerfestForumThreadId, HammerfestForumThreadKind, HammerfestForumThreadListing,
  HammerfestForumThreadPage, HammerfestForumThreadPageResponse, HammerfestForumThreadTitle, HammerfestGodchild,
  HammerfestGodchildrenResponse, HammerfestInventoryResponse, HammerfestItemId, HammerfestLadderLevel,
  HammerfestProfile, HammerfestProfileResponse, HammerfestQuestId, HammerfestQuestStatus, HammerfestServer,
  HammerfestSession, HammerfestSessionKey, HammerfestSessionUser, HammerfestShop, HammerfestShopResponse,
  HammerfestStore, HammerfestStoreRef, HammerfestUserId, HammerfestUserIdRef, HammerfestUserRef, HammerfestUsername,
  RawGetShortHammerfestUserError, ShortHammerfestForumTheme, ShortHammerfestForumThread, ShortHammerfestUser,
  StoredHammerfestProfile, StoredHammerfestUser, TouchEvniUser, TouchSession,
};
use eternaltwin_core::temporal::{ForeignRetrieved, ForeignSnapshot, LatestTemporal};
use std::collections::BTreeMap;
use std::convert::TryInto;
use std::num::NonZeroU16;
use std::str::FromStr;

#[macro_export]
macro_rules! test_hammerfest_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_empty);
    register_test!($(#[$meta])*, $api, test_touch_user);
    register_test!($(#[$meta])*, $api, test_touch_short_user_eole);
    register_test!($(#[$meta])*, $api, test_touch_session);
    register_test!($(#[$meta])*, $api, test_touch_evni);
    register_test!($(#[$meta])*, $api, test_get_missing_user);
    register_test!($(#[$meta])*, $api, test_touch_profile_empty);
    register_test!($(#[$meta])*, $api, test_touch_matching_profile);
    register_test!($(#[$meta])*, $api, test_touch_changed_profile);
    register_test!($(#[$meta])*, $api, test_touch_profile_with_email);
    register_test!($(#[$meta])*, $api, test_touch_profile_with_forum_ban);
    register_test!($(#[$meta])*, $api, test_changed_email);
    register_test!($(#[$meta])*, $api, test_touch_full_profile);
    register_test!($(#[$meta])*, $api, test_touch_user_inventory);
  };
}

// TODO: Remove these pg-specific tests: they should be supported by the mem impl too.
#[macro_export]
macro_rules! test_hammerfest_store_pg {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_touch_profile_igor010);
    register_test!($(#[$meta])*, $api, test_touch_profile_toto43sh);
    register_test!($(#[$meta])*, $api, test_touch_profile_nader38);
    register_test!($(#[$meta])*, $api, test_email_uniqueness);
    register_test!($(#[$meta])*, $api, test_touch_forum_theme_page);
    register_test!($(#[$meta])*, $api, test_touch_forum_thread_page);
    register_test!($(#[$meta])*, $api, test_touch_forum_thread_page_as_moderator);
    register_test!($(#[$meta])*, $api, test_touch_forum_thread_page_fr114628);
    register_test!($(#[$meta])*, $api, test_touch_godchildren);
    register_test!($(#[$meta])*, $api, test_touch_hammerfest_shop);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

macro_rules! assert_ok {
  ($result:expr $(,)?) => {{
    match &$result {
      Err(_) => {
        panic!("assertion failed: `result.is_ok()`: {:?}", &$result)
      }
      Ok(()) => {}
    }
  }};
}

fn session_user(id: &str, username: &str, tokens: u32) -> HammerfestSessionUser {
  HammerfestSessionUser {
    user: ShortHammerfestUser {
      server: HammerfestServer::HammerfestFr,
      id: id.parse().unwrap(),
      username: username.parse().unwrap(),
    },
    tokens,
  }
}

fn simple_profile(id: &str, username: &str) -> HammerfestProfile {
  HammerfestProfile {
    user: ShortHammerfestUser {
      server: HammerfestServer::HammerfestFr,
      id: id.parse().unwrap(),
      username: username.parse().unwrap(),
    },
    forum_ban: None,
    email: None,
    best_score: 0,
    best_level: 0,
    has_carrot: false,
    season_score: 0,
    ladder_level: 0.try_into().unwrap(),
    hall_of_fame: None,
    items: Default::default(),
    quests: Default::default(),
  }
}

fn profile_response(profile: &HammerfestProfile, session: Option<&HammerfestSessionUser>) -> HammerfestProfileResponse {
  HammerfestProfileResponse {
    session: session.cloned(),
    profile: Some(profile.clone()),
  }
}

fn profile_to_stored_user(
  profile: &HammerfestProfile,
  archived_at: Instant,
  modified_at: Instant,
  retrieved_at: Instant,
) -> StoredHammerfestUser {
  StoredHammerfestUser {
    server: profile.user.server,
    id: profile.user.id,
    username: profile.user.username.clone(),
    archived_at,
    profile: Some(LatestTemporal {
      latest: ForeignSnapshot {
        period: PeriodLower::unbounded(modified_at),
        retrieved: ForeignRetrieved { latest: retrieved_at },
        value: StoredHammerfestProfile {
          best_score: profile.best_score,
          best_level: profile.best_level,
          game_completed: profile.has_carrot,
          items: profile.items.clone(),
          quests: profile.quests.clone(),
        },
      },
    }),
    inventory: None,
  }
}

async fn retrieve_stored_user<T: HammerfestStore + ?Sized>(
  store: &T,
  user: &ShortHammerfestUser,
) -> Option<StoredHammerfestUser> {
  let options = GetHammerfestUserOptions {
    server: user.server,
    id: user.id,
    time: None,
  };

  store.get_user(&options).await.unwrap()
}

pub(crate) struct TestApi<TyClock, TyHammerfestStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) hammerfest_store: TyHammerfestStore,
}

pub(crate) async fn test_empty<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let options = GetHammerfestUserOptions {
    server: HammerfestServer::HammerfestFr,
    id: "123".parse().unwrap(),
    time: None,
  };
  let actual = api.hammerfest_store.hammerfest_store().get_short_user(&options).await;
  let expected = Err(RawGetShortHammerfestUserError::NotFound);
  assert_eq!(actual, expected);
}

pub(crate) async fn test_touch_user<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let get_user = GetHammerfestUserOptions {
    server: HammerfestServer::HammerfestFr,
    id: "123".parse().unwrap(),
    time: None,
  };

  let expected_short = ShortHammerfestUser {
    server: HammerfestServer::HammerfestFr,
    id: "123".parse().unwrap(),
    username: "alice".parse().unwrap(),
  };

  let expected = StoredHammerfestUser {
    server: HammerfestServer::HammerfestFr,
    id: "123".parse().unwrap(),
    username: "alice".parse().unwrap(),
    archived_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    profile: None,
    inventory: None,
  };

  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_short_user(&expected_short)
      .await
      .unwrap();
    assert_eq!(actual, expected);
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .get_short_user(&get_user)
      .await
      .unwrap();
    assert_eq!(actual, expected_short);
  }
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .get_user(&get_user)
      .await
      .unwrap();
    assert_eq!(actual.unwrap(), expected);
  }
}

pub(crate) async fn test_touch_short_user_eole<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_short_user(&ShortHammerfestUser {
        server: HammerfestServer::HammerfestFr,
        id: "120662".parse().unwrap(),
        username: HammerfestUsername::from_str("#eole").unwrap(),
      })
      .await
      .unwrap();
  }
  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .get_short_user(&GetHammerfestUserOptions {
        server: HammerfestServer::HammerfestFr,
        id: "120662".parse().unwrap(),
        time: None,
      })
      .await
      .unwrap();
    let expected = ShortHammerfestUser {
      server: HammerfestServer::HammerfestFr,
      id: "120662".parse().unwrap(),
      username: HammerfestUsername::from_str("#eole").unwrap(),
    };
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_session<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let clock = api.clock;
  clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_session(TouchSession {
        now: clock.now(),
        server: HammerfestServer::HammerfestFr,
        key: HammerfestSessionKey::from_str("m4desrt8boltb5ejvmjnacnt72").unwrap(),
        user: Some(ShortHammerfestUser {
          server: HammerfestServer::HammerfestFr,
          id: "123".parse().unwrap(),
          username: "alice".parse().unwrap(),
        }),
      })
      .await
      .unwrap();
  }
  clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .get_active_session(GetActiveSession {
        user: HammerfestUserRef::Id(HammerfestUserIdRef {
          server: HammerfestServer::HammerfestFr,
          id: "123".parse().unwrap(),
        }),
        time: clock.now(),
      })
      .await
      .unwrap();
    let expected = HammerfestSession {
      key: HammerfestSessionKey::from_str("m4desrt8boltb5ejvmjnacnt72").unwrap(),
      user: ShortHammerfestUser {
        server: HammerfestServer::HammerfestFr,
        id: "123".parse().unwrap(),
        username: "alice".parse().unwrap(),
      },
      ctime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
      atime: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
    };
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_get_missing_user<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let get_user = GetHammerfestUserOptions {
    server: HammerfestServer::HammerfestFr,
    id: "123".parse().unwrap(),
    time: None,
  };

  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api.hammerfest_store.hammerfest_store().get_short_user(&get_user).await;
    let expected = Err(RawGetShortHammerfestUserError::NotFound);
    assert_eq!(actual, expected);
  }
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .get_user(&get_user)
      .await
      .unwrap();
    let expected = None;
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_profile_empty<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let profile = simple_profile("123", "alice");
  let profile_response = profile_response(&profile, None);

  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_matching_profile<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let profile = simple_profile("123", "alice");
  let profile_response = profile_response(&profile, None);

  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
  let later = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 1));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, later));
    assert_eq!(actual, expected);
  }
}
pub(crate) async fn test_touch_changed_profile<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let mut profile = simple_profile("123", "alice");
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let profile_response = profile_response(&profile, None);
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
  let later = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 1));
  {
    profile.best_score = 100;
    let profile_response = profile_response(&profile, None);
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, later, later));
    assert_eq!(actual, expected);
  }
  let later2 = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 2));
  {
    profile.best_score = 0;
    let profile_response = profile_response(&profile, None);
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, later2, later2));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_profile_with_email<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let mut profile = simple_profile("123", "alice");
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_profile_igor010<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let profile = HammerfestProfile {
    user: ShortHammerfestUser {
      server: HammerfestServer::HammerfestEs,
      id: HammerfestUserId::from_str("74003").unwrap(),
      username: HammerfestUsername::from_str("igor010").unwrap(),
    },
    forum_ban: None,
    email: Some(None),
    best_score: 142741,
    best_level: 215,
    has_carrot: true,
    season_score: 0,
    ladder_level: HammerfestLadderLevel::new(3).unwrap(),
    hall_of_fame: None,
    items: [0, 102, 103, 1000]
      .into_iter()
      .map(|i| HammerfestItemId::new(i).unwrap())
      .collect(),
    quests: [
      (2, HammerfestQuestStatus::Complete),
      (6, HammerfestQuestStatus::Pending),
      (7, HammerfestQuestStatus::Pending),
      (10, HammerfestQuestStatus::Pending),
      (14, HammerfestQuestStatus::Pending),
      (18, HammerfestQuestStatus::Pending),
      (29, HammerfestQuestStatus::Pending),
      (39, HammerfestQuestStatus::Complete),
      (43, HammerfestQuestStatus::Complete),
      (59, HammerfestQuestStatus::Complete),
      (62, HammerfestQuestStatus::Pending),
      (63, HammerfestQuestStatus::Complete),
      (64, HammerfestQuestStatus::Complete),
      (67, HammerfestQuestStatus::Pending),
    ]
    .into_iter()
    .map(|(i, s)| (HammerfestQuestId::new(i).unwrap(), s))
    .collect(),
  };
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_profile_toto43sh<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let profile = HammerfestProfile {
    user: ShortHammerfestUser {
      server: HammerfestServer::HfestNet,
      id: HammerfestUserId::from_str("209749").unwrap(),
      username: HammerfestUsername::from_str("toto43sh").unwrap(),
    },
    forum_ban: None,
    email: Some(None),
    best_score: 0,
    best_level: -1,
    has_carrot: false,
    season_score: 0,
    ladder_level: HammerfestLadderLevel::new(4).unwrap(),
    hall_of_fame: None,
    items: [102, 115]
      .into_iter()
      .map(|i| HammerfestItemId::new(i).unwrap())
      .collect(),
    quests: BTreeMap::new(),
  };
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_profile_nader38<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let profile = HammerfestProfile {
    user: ShortHammerfestUser {
      server: HammerfestServer::HfestNet,
      id: HammerfestUserId::from_str("645001").unwrap(),
      username: HammerfestUsername::from_str("Nader38").unwrap(),
    },
    forum_ban: None,
    email: Some(None),
    best_score: 3_752_890,
    best_level: 100000,
    has_carrot: true,
    season_score: 0,
    ladder_level: HammerfestLadderLevel::new(1).unwrap(),
    hall_of_fame: None,
    items: [102, 115]
      .into_iter()
      .map(|i| HammerfestItemId::new(i).unwrap())
      .collect(),
    quests: BTreeMap::new(),
  };
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_evni<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  api
    .hammerfest_store
    .hammerfest_store()
    .touch_evni_user(TouchEvniUser {
      now,
      user: HammerfestUserIdRef {
        server: HammerfestServer::HammerfestFr,
        id: HammerfestUserId::new(9).unwrap(),
      },
    })
    .await
    .unwrap();
}

pub(crate) async fn test_touch_profile_with_forum_ban<TyClock, TyHammerfestStore>(
  api: TestApi<TyClock, TyHammerfestStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let mut profile = simple_profile("123", "alice");
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    profile.forum_ban = Some(true);
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_changed_email<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let mut profile = simple_profile("123", "alice");
  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
  let later = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 1));
  {
    profile.email = Some(Some("bob@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    // Note: HammerfestStore doesn't allow reading out emails.
    let expected = Some(profile_to_stored_user(&profile, now, now, later));
    assert_eq!(actual, expected);
  }
  let later2 = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 2));
  {
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    // Note: HammerfestStore doesn't allow reading out emails.
    let expected = Some(profile_to_stored_user(&profile, now, now, later2));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_email_uniqueness<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let mut profile = simple_profile("123", "alice");
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await;
    assert_ok!(actual);
  }
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 1));
  {
    let mut profile = simple_profile("234", "bob");
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await;
    assert_ok!(actual);
  }
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 2));
  {
    let mut profile = simple_profile("123", "alice");
    profile.email = Some(Some("alice@example.com".parse().unwrap()));
    let profile_response = profile_response(&profile, Some(&alice));
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await;
    assert_ok!(actual);
  }
}

pub(crate) async fn test_touch_user_inventory<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  let inventory_response = HammerfestInventoryResponse {
    session: alice.clone(),
    inventory: [
      ("0".parse().unwrap(), 152),
      ("50".parse().unwrap(), 2),
      ("1000".parse().unwrap(), 20375),
    ]
    .into_iter()
    .collect(),
  };

  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_inventory(&inventory_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &alice.user).await;
    let expected = StoredHammerfestUser {
      server: alice.user.server,
      id: alice.user.id,
      username: alice.user.username.clone(),
      archived_at: now,
      profile: None,
      inventory: Some(LatestTemporal {
        latest: ForeignSnapshot {
          period: PeriodLower::unbounded(now),
          retrieved: ForeignRetrieved { latest: now },
          value: [
            ("0".parse().unwrap(), 152),
            ("50".parse().unwrap(), 2),
            ("1000".parse().unwrap(), 20375),
          ]
          .into_iter()
          .collect(),
        },
      }),
    };
    assert_eq!(actual, Some(expected));
  }
}

pub(crate) async fn test_touch_full_profile<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let mut profile = simple_profile("123", "alice");
  profile.items = ["0".parse().unwrap(), "50".parse().unwrap(), "100".parse().unwrap()]
    .into_iter()
    .collect();
  profile.quests = [
    ("0".parse().unwrap(), HammerfestQuestStatus::Complete),
    ("10".parse().unwrap(), HammerfestQuestStatus::Complete),
    ("23".parse().unwrap(), HammerfestQuestStatus::Pending),
  ]
  .into_iter()
  .collect();
  let profile_response = profile_response(&profile, None);

  let now = api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    api
      .hammerfest_store
      .hammerfest_store()
      .touch_profile(&profile_response)
      .await
      .unwrap();
    let actual = retrieve_stored_user(api.hammerfest_store.hammerfest_store(), &profile.user).await;
    let expected = Some(profile_to_stored_user(&profile, now, now, now));
    assert_eq!(actual, expected);
  }
}

pub(crate) async fn test_touch_forum_theme_page<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let mut threads: Vec<HammerfestForumThread> = Vec::with_capacity(15);
    for i in 0..15 {
      threads.push(HammerfestForumThread {
        short: ShortHammerfestForumThread {
          server: HammerfestServer::HammerfestFr,
          id: format!("{}", 1000 + i).parse().unwrap(),
          name: format!("Thread {}", i).parse().unwrap(),
          is_closed: i % 2 == 0,
        },
        author: ShortHammerfestUser {
          server: HammerfestServer::HammerfestFr,
          id: "127".parse().unwrap(),
          username: "elseabora".parse().unwrap(),
        },
        author_role: HammerfestForumRole::None,
        kind: HammerfestForumThreadKind::Regular {
          latest_post_date: HammerfestDate {
            month: 3,
            day: 5,
            weekday: 5,
          },
        },
        reply_count: 4 * i,
      });
    }

    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_theme_page(&HammerfestForumThemePageResponse {
        session: Some(alice.clone()),
        page: HammerfestForumThemePage {
          theme: ShortHammerfestForumTheme {
            server: HammerfestServer::HammerfestFr,
            id: "3".parse().unwrap(),
            name: "Les secrets de Tuberculoz".parse().unwrap(),
            is_public: true,
          },
          sticky: vec![HammerfestForumThread {
            short: ShortHammerfestForumThread {
              server: HammerfestServer::HammerfestFr,
              id: "474604".parse().unwrap(),
              name: "[officiel] Corporate Soccer 2".parse().unwrap(),
              is_closed: false,
            },
            author: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "195".parse().unwrap(),
              username: "deepnight".parse().unwrap(),
            },
            author_role: HammerfestForumRole::Administrator,
            kind: HammerfestForumThreadKind::Sticky,
            reply_count: 0,
          }],
          threads: HammerfestForumThreadListing {
            page1: NonZeroU16::new(1).unwrap(),
            pages: NonZeroU16::new(16).unwrap(),
            items: threads,
          },
        },
      })
      .await;
    assert_ok!(actual);
  }
}

pub(crate) async fn test_touch_forum_thread_page<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_thread_page(&HammerfestForumThreadPageResponse {
        session: None,
        page: HammerfestForumThreadPage {
          theme: ShortHammerfestForumTheme {
            server: HammerfestServer::HammerfestFr,
            id: "3".parse().unwrap(),
            name: "Les secrets de Tuberculoz".parse().unwrap(),
            is_public: true,
          },
          thread: ShortHammerfestForumThread {
            server: HammerfestServer::HammerfestFr,
            id: "474604".parse().unwrap(),
            name: "[officiel] Corporate Soccer 2".parse().unwrap(),
            is_closed: false,
          },
          posts: HammerfestForumPostListing {
            page1: NonZeroU16::new(1).unwrap(),
            pages: NonZeroU16::new(1).unwrap(),
            items: {
              let mut posts: Vec<HammerfestForumPost> = Vec::with_capacity(15);
              for i in 0u8..15 {
                posts.push(HammerfestForumPost {
                  id: None,
                  author: if i % 2 == 0 {
                    HammerfestForumPostAuthor {
                      user: ShortHammerfestUser {
                        server: HammerfestServer::HammerfestFr,
                        id: "195".parse().unwrap(),
                        username: "deepnight".parse().unwrap(),
                      },
                      has_carrot: false,
                      ladder_level: HammerfestLadderLevel::new(2).unwrap(),
                      rank: None,
                      role: HammerfestForumRole::Administrator,
                    }
                  } else {
                    HammerfestForumPostAuthor {
                      user: ShortHammerfestUser {
                        server: HammerfestServer::HammerfestFr,
                        id: format!("{}", 1 + i).parse().unwrap(),
                        username: format!("usr{}", 1 + i).parse().unwrap(),
                      },
                      has_carrot: true,
                      ladder_level: HammerfestLadderLevel::new(1).unwrap(),
                      rank: Some((1 + i).into()),
                      role: HammerfestForumRole::None,
                    }
                  },
                  ctime: HammerfestDateTime {
                    date: HammerfestDate {
                      month: 3,
                      day: 5,
                      weekday: 5,
                    },
                    hour: 0,
                    minute: i,
                  },
                  content: format!("Hello! {}", i),
                });
              }
              posts
            },
          },
        },
      })
      .await;
    assert_ok!(actual);
  }
}

pub(crate) async fn test_touch_forum_thread_page_as_moderator<TyClock, TyHammerfestStore>(
  api: TestApi<TyClock, TyHammerfestStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_thread_page(&HammerfestForumThreadPageResponse {
        session: Some(alice),
        page: HammerfestForumThreadPage {
          theme: ShortHammerfestForumTheme {
            server: HammerfestServer::HammerfestFr,
            id: "3".parse().unwrap(),
            name: "Les secrets de Tuberculoz".parse().unwrap(),
            is_public: true,
          },
          thread: ShortHammerfestForumThread {
            server: HammerfestServer::HammerfestFr,
            id: "474604".parse().unwrap(),
            name: "[officiel] Corporate Soccer 2".parse().unwrap(),
            is_closed: false,
          },
          posts: HammerfestForumPostListing {
            page1: NonZeroU16::new(1).unwrap(),
            pages: NonZeroU16::new(1).unwrap(),
            items: {
              let mut posts: Vec<HammerfestForumPost> = Vec::with_capacity(15);
              for i in 0u8..15 {
                posts.push(HammerfestForumPost {
                  id: Some((1 + i).to_string().parse().unwrap()),
                  author: if i % 2 == 0 {
                    HammerfestForumPostAuthor {
                      user: ShortHammerfestUser {
                        server: HammerfestServer::HammerfestFr,
                        id: "195".parse().unwrap(),
                        username: "deepnight".parse().unwrap(),
                      },
                      has_carrot: false,
                      ladder_level: HammerfestLadderLevel::new(2).unwrap(),
                      rank: None,
                      role: HammerfestForumRole::Administrator,
                    }
                  } else {
                    HammerfestForumPostAuthor {
                      user: ShortHammerfestUser {
                        server: HammerfestServer::HammerfestFr,
                        id: format!("{}", 1 + i).parse().unwrap(),
                        username: format!("usr{}", 1 + i).parse().unwrap(),
                      },
                      has_carrot: true,
                      ladder_level: HammerfestLadderLevel::new(1).unwrap(),
                      rank: Some((1 + i).into()),
                      role: HammerfestForumRole::None,
                    }
                  },
                  ctime: HammerfestDateTime {
                    date: HammerfestDate {
                      month: 3,
                      day: 5,
                      weekday: 5,
                    },
                    hour: 0,
                    minute: i,
                  },
                  content: format!("Hello! {}", i),
                });
              }
              posts
            },
          },
        },
      })
      .await;
    assert_ok!(actual);
  }
}

pub(crate) async fn test_touch_forum_thread_page_fr114628<TyClock, TyHammerfestStore>(
  api: TestApi<TyClock, TyHammerfestStore>,
) where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_thread_page(&HammerfestForumThreadPageResponse {
        session: Some(HammerfestSessionUser {
          user: ShortHammerfestUser {
            server: HammerfestServer::HammerfestFr,
            id: HammerfestUserId::new(176431).unwrap(),
            username: HammerfestUsername::from_str("maniaclan").unwrap(),
          },
          tokens: 5096,
        }),
        page: HammerfestForumThreadPage {
          theme: ShortHammerfestForumTheme {
            server: HammerfestServer::HammerfestFr,
            id: HammerfestForumThemeId::from_str("3").unwrap(),
            name: HammerfestForumThemeTitle::from_str("Les secrets de Tuberculoz").unwrap(),
            is_public: false,
          },
          thread: ShortHammerfestForumThread {
            server: HammerfestServer::HammerfestFr,
            id: HammerfestForumThreadId::from_str("114628").unwrap(),
            name: HammerfestForumThreadTitle::from_str("Corrections mineures").unwrap(),
            is_closed: true,
          },
          posts: HammerfestForumPostListing {
            page1: NonZeroU16::new(1).unwrap(),
            pages: NonZeroU16::new(1).unwrap(),
            items: vec![
              HammerfestForumPost {
                id: Some(HammerfestForumPostId::new(114628).unwrap()),
                author: HammerfestForumPostAuthor {
                  user: ShortHammerfestUser {
                    server: HammerfestServer::HammerfestFr,
                    id: HammerfestUserId::new(195).unwrap(),
                    username: HammerfestUsername::from_str("deepnight").unwrap(),
                  },
                  has_carrot: true,
                  ladder_level: HammerfestLadderLevel::new(3).unwrap(),
                  rank: Some(628),
                  role: HammerfestForumRole::Administrator,
                },
                ctime: HammerfestDateTime {
                  date: HammerfestDate {
                    month: 1,
                    day: 5,
                    weekday: 5,
                  },
                  hour: 14,
                  minute: 48,
                },
                content: HtmlFragment::from("Divers petits soucis..."),
              },
              HammerfestForumPost {
                id: Some(HammerfestForumPostId::new(114682).unwrap()),
                author: HammerfestForumPostAuthor {
                  user: ShortHammerfestUser {
                    server: HammerfestServer::HammerfestFr,
                    id: HammerfestUserId::new(195).unwrap(),
                    username: HammerfestUsername::from_str("deepnight").unwrap(),
                  },
                  has_carrot: true,
                  ladder_level: HammerfestLadderLevel::new(3).unwrap(),
                  rank: Some(628),
                  role: HammerfestForumRole::Administrator,
                },
                ctime: HammerfestDateTime {
                  date: HammerfestDate {
                    month: 1,
                    day: 5,
                    weekday: 5,
                  },
                  hour: 15,
                  minute: 10,
                },
                content: HtmlFragment::from("Les pics du niveau 33..."),
              },
            ],
          },
        },
      })
      .await;
    assert_ok!(actual);
  }
}

pub(crate) async fn test_touch_godchildren<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_godchildren(&HammerfestGodchildrenResponse {
        session: alice.clone(),
        godchildren: vec![],
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_godchildren(&HammerfestGodchildrenResponse {
        session: alice.clone(),
        godchildren: vec![HammerfestGodchild {
          user: ShortHammerfestUser {
            server: HammerfestServer::HammerfestFr,
            id: "456".parse().unwrap(),
            username: "bob".parse().unwrap(),
          },
          tokens: 0,
        }],
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_godchildren(&HammerfestGodchildrenResponse {
        session: alice.clone(),
        godchildren: vec![
          HammerfestGodchild {
            user: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "456".parse().unwrap(),
              username: "bob".parse().unwrap(),
            },
            tokens: 1,
          },
          HammerfestGodchild {
            user: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "789".parse().unwrap(),
              username: "charlie".parse().unwrap(),
            },
            tokens: 0,
          },
        ],
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_godchildren(&HammerfestGodchildrenResponse {
        session: alice.clone(),
        godchildren: vec![HammerfestGodchild {
          user: ShortHammerfestUser {
            server: HammerfestServer::HammerfestFr,
            id: "456".parse().unwrap(),
            username: "bob".parse().unwrap(),
          },
          tokens: 1,
        }],
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_godchildren(&HammerfestGodchildrenResponse {
        session: alice.clone(),
        godchildren: vec![
          HammerfestGodchild {
            user: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "456".parse().unwrap(),
              username: "bob".parse().unwrap(),
            },
            tokens: 1,
          },
          HammerfestGodchild {
            user: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "789".parse().unwrap(),
              username: "charlie".parse().unwrap(),
            },
            tokens: 0,
          },
        ],
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_godchildren(&HammerfestGodchildrenResponse {
        session: alice.clone(),
        godchildren: vec![
          HammerfestGodchild {
            user: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "456".parse().unwrap(),
              username: "bob".parse().unwrap(),
            },
            tokens: 2,
          },
          HammerfestGodchild {
            user: ShortHammerfestUser {
              server: HammerfestServer::HammerfestFr,
              id: "789".parse().unwrap(),
              username: "charlie".parse().unwrap(),
            },
            tokens: 2,
          },
        ],
      })
      .await;
    assert_ok!(actual);
  }
}

pub(crate) async fn test_touch_hammerfest_shop<TyClock, TyHammerfestStore>(api: TestApi<TyClock, TyHammerfestStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyHammerfestStore: HammerfestStoreRef,
{
  let alice = session_user("123", "alice", 50);
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_shop(&HammerfestShopResponse {
        session: alice.clone(),
        shop: HammerfestShop {
          weekly_tokens: 0,
          purchased_tokens: None,
          has_quest_bonus: false,
        },
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_shop(&HammerfestShopResponse {
        session: alice.clone(),
        shop: HammerfestShop {
          weekly_tokens: 0,
          purchased_tokens: Some(5),
          has_quest_bonus: false,
        },
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_shop(&HammerfestShopResponse {
        session: alice.clone(),
        shop: HammerfestShop {
          weekly_tokens: 0,
          purchased_tokens: Some(5),
          has_quest_bonus: false,
        },
      })
      .await;
    assert_ok!(actual);
  }

  api.clock.advance_by(Duration::from_seconds(1));
  {
    let actual = api
      .hammerfest_store
      .hammerfest_store()
      .touch_shop(&HammerfestShopResponse {
        session: alice.clone(),
        shop: HammerfestShop {
          weekly_tokens: 0,
          purchased_tokens: Some(5),
          has_quest_bonus: false,
        },
      })
      .await;
    assert_ok!(actual);
  }
}
