use async_trait::async_trait;
use eternaltwin_core::api::SyncRef;
use eternaltwin_core::core::{Duration, FinitePeriod, Handler, Instant, Listing, PeriodLower, SecretString};
use eternaltwin_core::digest::{DigestSha2_256, DigestSha3_256};
use eternaltwin_core::email::{touch_email_address, EmailAddress};
use eternaltwin_core::mailer::store::acquire_outbound_email_request::{
  AcquireOutboundEmailRequest, AcquireOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::create_outbound_email::{CreateOutboundEmail, CreateOutboundEmailError};
use eternaltwin_core::mailer::store::get_outbound_email_requests::{
  GetOutboundEmailRequests, GetOutboundEmailRequestsError,
};
use eternaltwin_core::mailer::store::get_outbound_emails::{GetOutboundEmails, GetOutboundEmailsError};
use eternaltwin_core::mailer::store::release_outbound_email_request::{
  ReleaseOutboundEmailRequest, ReleaseOutboundEmailRequestError,
};
use eternaltwin_core::mailer::store::{
  EmailContentPayload, EmailContentSummary, EmailDeliveryStatus, EmailPayloadKind, EmailRequestStatus, OutboundEmail,
  OutboundEmailId, OutboundEmailIdRef, OutboundEmailRequest, OutboundEmailRequestId,
};
use eternaltwin_core::pg_num::{PgU16, PgU32, PgU8};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{UserId, UserIdRef};
use eternaltwin_core::uuid::{UuidGenerator, UuidGeneratorRef};
use serde_json::Value as JsonValue;
use sqlx::postgres::PgDatabaseError;
use sqlx::{PgPool, Postgres};
use std::str::FromStr;

pub struct PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  database: TyDatabase,
  database_secret: SecretString,
  uuid_generator: TyUuidGenerator,
}

impl<TyDatabase, TyUuidGenerator> PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(database: TyDatabase, database_secret: SecretString, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      database,
      database_secret,
      uuid_generator,
    }
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<CreateOutboundEmail<JsonValue>> for PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: CreateOutboundEmail<JsonValue>,
  ) -> Result<OutboundEmail<JsonValue>, CreateOutboundEmailError> {
    if cmd.deadline <= cmd.now {
      return Err(CreateOutboundEmailError::InvalidDeadline(cmd.deadline, cmd.now));
    }

    let outbound_email_id = OutboundEmailId::from_uuid(self.uuid_generator.uuid_generator().next());

    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    let sender = touch_email_address(&mut tx, &self.database_secret, &cmd.sender.to_raw(), cmd.now)
      .await
      .map_err(WeakError::wrap)?;
    let recipient = touch_email_address(&mut tx, &self.database_secret, &cmd.recipient.to_raw(), cmd.now)
      .await
      .map_err(WeakError::wrap)?;

    // language=PostgreSQL
    let _ = sqlx::query("savepoint mailer_store_before_outbound_email_insert")
      .execute(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      outbound_email_id: OutboundEmailId,
    }
    // language=PostgreSQL
    let res = sqlx::query_as::<_, Row>(
      r"
          insert
          into outbound_email(
            outbound_email_id, idempotency_key, created_by,
            submitted_at, deadline,
            sender, recipient,
            payload_kind, payload_version, payload,
            text_size, text_sha2_256, text_sha3_256,
            html_size, html_sha2_256, html_sha3_256,
            delivery_status, next_request_at, read_at)
          values (
            $2::outbound_email_id, $3::idempotency_key, $4::user_id,
            $5::instant, $6::instant,
            $7::email_address_hash, $8::email_address_hash,
            $9::email_payload_kind, $10::u8, pgp_sym_encrypt($11::json::text, $1::text)::bytea,
            $12::u16, $13::digest_sha2_256, $14::digest_sha3_256,
            $15::u16, $16::digest_sha2_256, $17::digest_sha3_256,
            'Pending', $5::instant, null
          )
          returning outbound_email_id;
        ",
    )
    .bind(self.database_secret.as_str())
    .bind(outbound_email_id)
    .bind(cmd.idempotency_key)
    .bind(cmd.actor.id)
    .bind(cmd.now)
    .bind(cmd.deadline)
    .bind(sender)
    .bind(recipient)
    .bind(cmd.payload.kind)
    .bind(PgU8::new(cmd.payload.version))
    .bind(&cmd.payload.data)
    .bind(PgU16::new(cmd.payload.text.size))
    .bind(cmd.payload.text.sha2_256)
    .bind(cmd.payload.text.sha3_256)
    .bind(PgU16::new(cmd.payload.html.size))
    .bind(cmd.payload.html.sha2_256)
    .bind(cmd.payload.html.sha3_256)
    .fetch_one(&mut *tx)
    .await;

    let written = match res {
      Ok(written) => written,
      Err(e) => {
        let pg_err: Option<&PgDatabaseError> = e
          .as_database_error()
          .and_then(|db_err| db_err.try_downcast_ref::<PgDatabaseError>());

        if let Some(pg_err) = pg_err {
          if pg_err.code() == "23505"
            && pg_err.table() == Some("outbound_email")
            && pg_err.constraint() == Some("outboud_email__idempotency_key__uk")
          {
            // language=PostgreSQL
            let _ = sqlx::query("rollback to mailer_store_before_outbound_email_insert")
              .execute(&mut *tx)
              .await
              .map_err(WeakError::wrap)?;

            // idempotency key already present
            // language=PostgreSQL
            let old_row = sqlx::query_as::<_, Row>(
              r"
                select
                  outbound_email_id, idempotency_key, created_by,
                  submitted_at, deadline,
                  sender, recipient,
                  payload_kind, payload_version, payload,
                  text_size, text_sha2_256, text_sha3_256,
                  html_size, html_sha2_256, html_sha3_256,
                  delivery_status, next_request_at, read_at
                from outbound_email
                where idempotency_key = $1::idempotency_key;
              ",
            )
            .bind(cmd.idempotency_key)
            .fetch_one(&mut *tx)
            .await
            .map_err(WeakError::wrap)?;
            return Err(CreateOutboundEmailError::Conflict(
              old_row.outbound_email_id,
              cmd.idempotency_key,
            ));
          }
        }
        return Err(CreateOutboundEmailError::Other(WeakError::wrap(e)));
      }
    };

    assert_eq!(written.outbound_email_id, outbound_email_id);

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(OutboundEmail {
      id: outbound_email_id,
      submitted_at: cmd.now,
      created_by: cmd.actor,
      deadline: cmd.deadline,
      sender: cmd.sender,
      recipient: cmd.recipient,
      payload: cmd.payload,
      read_at: None,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<AcquireOutboundEmailRequest> for PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: AcquireOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, AcquireOutboundEmailRequestError> {
    let outbound_email_request_id = OutboundEmailRequestId::from_uuid(self.uuid_generator.uuid_generator().next());
    let period = FinitePeriod::new(cmd.now, cmd.now + Duration::from_hours(1));

    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    {
      let rate_limiter_period = PeriodLower::new(cmd.now - Duration::from_hours(12), Some(cmd.now));

      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        count: PgU32,
      }
      // language=PostgreSQL
      let res = sqlx::query_as::<_, Row>(
        r"
          select count(*)::u32 as count
          from outbound_email_request
          where period && $1::period_lower
        ",
      )
      .bind(rate_limiter_period)
      .fetch_one(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      if res.count.get() >= 40 {
        return Err(AcquireOutboundEmailRequestError::Limit(
          cmd.now + Duration::from_hours(1),
        ));
      }
    }

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      outbound_email_request_id: OutboundEmailRequestId,
      outbound_email_id: OutboundEmailId,
    }
    // language=PostgreSQL
    let res = sqlx::query_as::<_, Row>(
      r"
          insert
          into outbound_email_request(
            outbound_email_request_id,
            outbound_email_id, period,
            status, result_version, result
          )
          values (
            $1::outbound_email_request_id,
            $2::outbound_email_id, period($3::instant, $4::instant),
            'Pending', null, null
          )
          returning outbound_email_request_id, outbound_email_id;
        ",
    )
    .bind(outbound_email_request_id)
    .bind(cmd.outbound_email)
    .bind(period.start)
    .bind(period.end)
    .fetch_one(&mut *tx)
    .await;

    let written = match res {
      Ok(written) => written,
      Err(e) => return Err(AcquireOutboundEmailRequestError::Other(WeakError::wrap(e))),
    };

    assert_eq!(written.outbound_email_request_id, outbound_email_request_id);

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(OutboundEmailRequest {
      id: outbound_email_request_id,
      email: OutboundEmailIdRef::new(written.outbound_email_id),
      period,
      status: EmailRequestStatus::Pending,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<ReleaseOutboundEmailRequest> for PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    cmd: ReleaseOutboundEmailRequest,
  ) -> Result<OutboundEmailRequest, ReleaseOutboundEmailRequestError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    let new_status = match cmd.result {
      None => EmailRequestStatus::Cancelled,
      Some(Ok(())) => EmailRequestStatus::Ok,
      Some(Err(())) => EmailRequestStatus::Error,
    };

    #[derive(Debug, sqlx::FromRow)]
    struct ReqRow {
      outbound_email_id: OutboundEmailId,
      period: FinitePeriod,
    }
    // language=PostgreSQL
    let req_row = sqlx::query_as::<_, ReqRow>(
      r"
          update outbound_email_request
          set period = period(lower(period), $1::instant), status = $2::email_request_status, result_version = null, result = null
          where outbound_email_request_id = $3::outbound_email_request_id and status = 'Pending' and period @> $1::instant
          returning outbound_email_id, period
        ",
    )
    .bind(cmd.now)
    .bind(new_status)
    .bind(cmd.outbound_email_request.id)
    .fetch_one(&mut *tx)
    .await.map_err(WeakError::wrap)?;

    if new_status == EmailRequestStatus::Ok {
      // language=PostgreSQL
      let res = sqlx::query::<_>(
        r"
          update outbound_email
          set delivery_status = $1::email_delivery_status
          where outbound_email_id = $2::outbound_email_id and delivery_status = 'Pending'
        ",
      )
      .bind(EmailDeliveryStatus::Sent)
      .bind(req_row.outbound_email_id)
      .execute(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;

      if res.rows_affected() != 1 {
        return Err(ReleaseOutboundEmailRequestError::Other(WeakError::new(format!(
          "no pending `outbound_email` row found for id {}",
          req_row.outbound_email_id
        ))));
      }
    }

    tx.commit().await.map_err(WeakError::wrap)?;

    Ok(OutboundEmailRequest {
      id: cmd.outbound_email_request.id,
      email: OutboundEmailIdRef::new(req_row.outbound_email_id),
      period: req_row.period,
      status: new_status,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<GetOutboundEmails<JsonValue>> for PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    query: GetOutboundEmails<JsonValue>,
  ) -> Result<Listing<OutboundEmail<JsonValue>>, GetOutboundEmailsError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    let count = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        count: PgU32,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<Postgres, Row>(
        r"
          select count(*)::u32 as count
          from
            outbound_email
          where submitted_at <= $1::instant;
        ",
      )
      .bind(query.time)
      .bind(PgU32::new(query.limit))
      .fetch_one(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      row.count.get()
    };

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      outbound_email_id: OutboundEmailId,
      created_by: UserId,
      submitted_at: Instant,
      deadline: Instant,
      payload_kind: EmailPayloadKind,
      payload_version: PgU8,
      payload: JsonValue,
      text_size: PgU16,
      text_sha2_256: DigestSha2_256,
      text_sha3_256: DigestSha3_256,
      html_size: PgU16,
      html_sha2_256: DigestSha2_256,
      html_sha3_256: DigestSha3_256,
    }
    // language=PostgreSQL
    let rows = sqlx::query_as::<_, Row>(
      r"
          select
            outbound_email_id, idempotency_key, created_by,
            submitted_at, deadline,
--             sender, recipient,
            payload_kind, payload_version, payload,
            text_size, text_sha2_256, text_sha3_256,
            html_size, html_sha2_256, html_sha3_256,
            delivery_status, next_request_at, read_at
          from
            outbound_email
          where submitted_at <= $1::instant
          order by submitted_at desc
          limit $2::u32;
        ",
    )
    .bind(query.time)
    .bind(PgU32::new(query.limit))
    .fetch_all(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    tx.commit().await.map_err(WeakError::wrap)?;

    let items: Vec<OutboundEmail<_>> = rows
      .into_iter()
      .map(|row| OutboundEmail {
        id: row.outbound_email_id,
        submitted_at: row.submitted_at,
        created_by: UserIdRef::new(row.created_by),
        deadline: row.deadline,
        sender: EmailAddress::from_str("todo@eternaltwin.org").unwrap(),
        recipient: EmailAddress::from_str("todo@eternaltwin.org").unwrap(),
        payload: EmailContentPayload {
          kind: row.payload_kind,
          version: row.payload_version.get(),
          data: row.payload,
          text: EmailContentSummary {
            size: row.text_size.get(),
            sha2_256: row.text_sha2_256,
            sha3_256: row.text_sha3_256,
          },
          html: EmailContentSummary {
            size: row.html_size.get(),
            sha2_256: row.html_sha2_256,
            sha3_256: row.html_sha3_256,
          },
        },
        read_at: None,
      })
      .collect();

    Ok(Listing {
      offset: 0,
      limit: query.limit,
      count,
      items,
    })
  }
}

#[async_trait]
impl<TyDatabase, TyUuidGenerator> Handler<GetOutboundEmailRequests> for PgMailerStore<TyDatabase, TyUuidGenerator>
where
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn handle(
    &self,
    query: GetOutboundEmailRequests,
  ) -> Result<Listing<OutboundEmailRequest>, GetOutboundEmailRequestsError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    let count = {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        count: PgU32,
      }
      // language=PostgreSQL
      let row = sqlx::query_as::<Postgres, Row>(
        r"
          select count(*)::u32 as count
          from
            outbound_email_request
          where lower(period) <= $1::instant;
        ",
      )
      .bind(query.time)
      .bind(PgU32::new(query.limit))
      .fetch_one(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      row.count.get()
    };

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      outbound_email_request_id: OutboundEmailRequestId,
      outbound_email_id: OutboundEmailId,
      period: FinitePeriod,
      status: EmailRequestStatus,
    }
    // language=PostgreSQL
    let rows = sqlx::query_as::<_, Row>(
      r"
          select
            outbound_email_request_id, outbound_email_id, period, status
          from
            outbound_email_request
          where lower(period) <= $1::instant
          order by lower(period) desc
          limit $2::u32;
        ",
    )
    .bind(query.time)
    .bind(PgU32::new(query.limit))
    .fetch_all(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    tx.commit().await.map_err(WeakError::wrap)?;

    let items: Vec<OutboundEmailRequest> = rows
      .into_iter()
      .map(|row| OutboundEmailRequest {
        id: row.outbound_email_request_id,
        email: row.outbound_email_id.into(),
        period: row.period,
        status: row.status,
      })
      .collect();

    Ok(Listing {
      offset: 0,
      limit: query.limit,
      count,
      items,
    })
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::store::test::test_mailer_store;
  use crate::store::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::{Instant, SecretString};
  use eternaltwin_core::mailer::store::MailerStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_db_schema::force_create_latest;
  use eternaltwin_user_store::pg::PgUserStore;
  use opentelemetry::trace::noop::NoopTracerProvider;
  use opentelemetry::trace::TracerProvider;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn MailerStore<JsonValue>>, Arc<dyn UserStore>> {
    let config = eternaltwin_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let tracer_provider = NoopTracerProvider::new();
    let tracer = tracer_provider.tracer("mailer_store_test");

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let database_secret = SecretString::new("dev_secret".to_string());
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(
      clock.clone(),
      Arc::clone(&database),
      database_secret.clone(),
      tracer,
      Arc::clone(&uuid_generator),
    ));
    let mailer_store: Arc<dyn MailerStore<JsonValue>> =
      Arc::new(PgMailerStore::new(database, database_secret, uuid_generator));

    TestApi {
      clock,
      mailer_store,
      user_store,
    }
  }

  test_mailer_store!(
    #[serial]
    || make_test_api().await
  );
}
