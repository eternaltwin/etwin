use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::email::{EmailAddress, EmailContent, Mailer, MailerRef};
use eternaltwin_core::types::WeakError;
use opentelemetry::trace::{FutureExt, Status, TraceContextExt, Tracer};
use opentelemetry::Context;
use std::borrow::Cow;

pub struct TraceMailer<TyClock, TyMailer, TyTracer> {
  clock: TyClock,
  inner: TyMailer,
  tracer: TyTracer,
}

impl<TyClock, TyMailer, TyTracer> TraceMailer<TyClock, TyMailer, TyTracer> {
  pub fn new(clock: TyClock, inner: TyMailer, tracer: TyTracer) -> Self {
    Self { clock, inner, tracer }
  }
}

#[async_trait]
impl<TyClock, TyMailer, TyTracer> Mailer for TraceMailer<TyClock, TyMailer, TyTracer>
where
  TyClock: ClockRef,
  TyMailer: MailerRef,
  TyTracer: Tracer + Send + Sync,
  TyTracer::Span: Send + Sync + 'static,
{
  async fn send_email(&self, recipient: &EmailAddress, content: &EmailContent) -> Result<(), WeakError> {
    let clock = self.clock.clock();
    let now = clock.now();
    let span = self.tracer.build(
      self
        .tracer
        .span_builder("Mailer::send_email")
        .with_start_time(now.into_system()),
    );
    let ocx = Context::current_with_span(span);
    let res = self
      .inner
      .mailer()
      .send_email(recipient, content)
      .with_context(ocx.clone())
      .await;
    let span = ocx.span();
    span.set_status(match res.as_ref() {
      Ok(_) => Status::Ok,
      Err(e) => Status::Error {
        description: Cow::Owned(format!("{e:?}")),
      },
    });
    span.end_with_timestamp(clock.now().into_system());
    res
  }
}
