mod url;

use crate::http::url::EtwinUrls;
use crate::{EtwinAuth, EtwinClient, GetSelfError, GetUserError};
use ::url::Url;
use async_trait::async_trait;
use eternaltwin_core::auth::AuthContext;
use eternaltwin_core::clock::ClockRef;
use eternaltwin_core::user::{ShortUser, UserId};
use reqwest::{Client, RequestBuilder, Response};
use std::str::{from_utf8, Utf8Error};
use std::time::Duration;

const USER_AGENT: &str = "eternaltwin_client";
const TIMEOUT: Duration = Duration::from_millis(5000);

pub struct HttpEternaltwinClient<TyClock> {
  client: Client,
  #[allow(unused)]
  clock: TyClock,
  urls: EtwinUrls,
}

#[derive(Debug, thiserror::Error)]
pub enum WithAuthError {
  #[error("basic authentication requires password to be encoded as UTF-8")]
  PasswordUtf8(#[source] Utf8Error),
}

trait RequestBuilderExt {
  fn with_auth(self, auth: &EtwinAuth) -> Result<RequestBuilder, WithAuthError>;
}

impl RequestBuilderExt for RequestBuilder {
  fn with_auth(self, auth: &EtwinAuth) -> Result<RequestBuilder, WithAuthError> {
    Ok(match auth {
      EtwinAuth::Guest => self,
      EtwinAuth::Session(_) => todo!(),
      EtwinAuth::Token(token) => self.bearer_auth(token),
      EtwinAuth::Credentials { username, password } => {
        let password = from_utf8(password.0.as_slice()).map_err(WithAuthError::PasswordUtf8)?;
        self.basic_auth(username, Some(password))
      }
    })
  }
}

impl<TyClock> HttpEternaltwinClient<TyClock>
where
  TyClock: ClockRef,
{
  pub fn new(clock: TyClock, root: Url) -> Self {
    Self {
      client: Client::builder()
        .user_agent(USER_AGENT)
        .timeout(TIMEOUT)
        .redirect(reqwest::redirect::Policy::none())
        .build()
        .expect("building the client always succeeds"),
      clock,
      urls: EtwinUrls::new(root),
    }
  }
}

#[async_trait]
impl<TyClock> EtwinClient for HttpEternaltwinClient<TyClock>
where
  TyClock: ClockRef,
{
  async fn get_self(&self, auth: &EtwinAuth) -> Result<AuthContext, GetSelfError> {
    let mut builder = self.client.get(self.urls.auth_self());
    builder = builder.with_auth(auth)?;
    let res: Response = builder.send().await.map_err(GetSelfError::Send)?;
    let res = res.json::<AuthContext>().await.map_err(GetSelfError::Receive)?;
    Ok(res)
  }

  async fn get_user(&self, auth: &EtwinAuth, user_id: UserId) -> Result<ShortUser, GetUserError> {
    let mut builder = self.client.get(self.urls.user(user_id));
    builder = builder.with_auth(auth)?;
    let res: Response = builder.send().await.map_err(GetUserError::Send)?;
    let res = res.json::<ShortUser>().await.map_err(GetUserError::Send)?;
    Ok(res)
  }

  // pub async fn create_session(&self, username: Username, password: Password) -> Result<ShortUser, AnyError> {
  //   let mut url = self.urls.auth_self();
  //   url.query_pairs_mut().append_pair("method", "Etwin");
  //   let mut builder = self.client.put(url);
  //   builder = builder.json(&UserCredentials {
  //     login: username.to_string(),
  //     password: hex::encode(password.0.as_slice()),
  //   });
  //   let res: Response = builder.send().await?;
  //   let res = res.json::<ShortUser>().await?;
  //   Ok(res)
  // }
}

// #[derive(Debug, Serialize)]
// struct UserCredentials {
//   login: String,
//   password: String,
// }
