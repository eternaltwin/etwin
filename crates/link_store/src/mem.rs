use async_trait::async_trait;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Handler, RawUserDot};
use eternaltwin_core::dinoparc::{DinoparcServer, DinoparcUserId, DinoparcUserIdRef};
use eternaltwin_core::hammerfest::{HammerfestServer, HammerfestUserId, HammerfestUserIdRef};
use eternaltwin_core::link::{
  DeleteLinkError, DeleteLinkOptions, GetLinkOptions, GetLinksFromEtwinOptions, OldRawLink, RawDeleteAllLinks, RawLink,
  RemoteUserIdRef, TouchLinkError, TouchLinkOptions, VersionedRawLink, VersionedRawLinks,
};
use eternaltwin_core::twinoid::{TwinoidUserId, TwinoidUserIdRef};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::UserId;
use std::collections::HashMap;
use std::sync::RwLock;

struct RawLinkHistory<T: RemoteUserIdRef> {
  current: Option<RawLink<T>>,
  old: Vec<OldRawLink<T>>,
}

impl<T: RemoteUserIdRef> Default for RawLinkHistory<T> {
  fn default() -> Self {
    Self {
      current: None,
      old: vec![],
    }
  }
}

struct StoreState {
  from_dinoparc: HashMap<(DinoparcServer, DinoparcUserId), RawLinkHistory<DinoparcUserIdRef>>,
  to_dinoparc: HashMap<(UserId, DinoparcServer), RawLinkHistory<DinoparcUserIdRef>>,
  from_hammerfest: HashMap<(HammerfestServer, HammerfestUserId), RawLinkHistory<HammerfestUserIdRef>>,
  to_hammerfest: HashMap<(UserId, HammerfestServer), RawLinkHistory<HammerfestUserIdRef>>,
  from_twinoid: HashMap<TwinoidUserId, RawLinkHistory<TwinoidUserIdRef>>,
  to_twinoid: HashMap<UserId, RawLinkHistory<TwinoidUserIdRef>>,
}

impl StoreState {
  fn new() -> Self {
    Self {
      from_dinoparc: HashMap::new(),
      to_dinoparc: HashMap::new(),
      from_hammerfest: HashMap::new(),
      to_hammerfest: HashMap::new(),
      from_twinoid: HashMap::new(),
      to_twinoid: HashMap::new(),
    }
  }
}

pub struct MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> Handler<TouchLinkOptions<DinoparcUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    cmd: TouchLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, TouchLinkError<DinoparcUserIdRef>> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    touch_link(
      &mut state.from_dinoparc,
      &mut state.to_dinoparc,
      (cmd.remote.server, cmd.remote.id),
      (cmd.etwin.id, cmd.remote.server),
      || {
        let now = self.clock.clock().now();
        let link: RawLink<DinoparcUserIdRef> = RawLink {
          link: RawUserDot {
            time: now,
            user: cmd.linked_by,
          },
          unlink: (),
          etwin: cmd.etwin,
          remote: cmd.remote,
        };
        link
      },
    )
  }
}

#[async_trait]
impl<TyClock> Handler<TouchLinkOptions<HammerfestUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    cmd: TouchLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, TouchLinkError<HammerfestUserIdRef>> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    touch_link(
      &mut state.from_hammerfest,
      &mut state.to_hammerfest,
      (cmd.remote.server, cmd.remote.id),
      (cmd.etwin.id, cmd.remote.server),
      || {
        let now = self.clock.clock().now();
        let link: RawLink<HammerfestUserIdRef> = RawLink {
          link: RawUserDot {
            time: now,
            user: cmd.linked_by,
          },
          unlink: (),
          etwin: cmd.etwin,
          remote: cmd.remote,
        };
        link
      },
    )
  }
}

#[async_trait]
impl<TyClock> Handler<TouchLinkOptions<TwinoidUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    cmd: TouchLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, TouchLinkError<TwinoidUserIdRef>> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    touch_link(
      &mut state.from_twinoid,
      &mut state.to_twinoid,
      cmd.remote.id,
      cmd.etwin.id,
      || {
        let now = self.clock.clock().now();
        let link: RawLink<TwinoidUserIdRef> = RawLink {
          link: RawUserDot {
            time: now,
            user: cmd.linked_by,
          },
          unlink: (),
          etwin: cmd.etwin,
          remote: cmd.remote,
        };
        link
      },
    )
  }
}

#[async_trait]
impl<TyClock> Handler<DeleteLinkOptions<DinoparcUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    cmd: DeleteLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, DeleteLinkError<DinoparcUserIdRef>> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    delete_link(
      &mut state.from_dinoparc,
      &mut state.to_dinoparc,
      (cmd.remote.server, cmd.remote.id),
      (cmd.etwin.id, cmd.remote.server),
      |start| {
        let now = self.clock.clock().now();
        let link: OldRawLink<DinoparcUserIdRef> = OldRawLink {
          link: start.link,
          unlink: RawUserDot {
            time: now,
            user: cmd.unlinked_by,
          },
          etwin: cmd.etwin,
          remote: cmd.remote,
        };
        link
      },
      || DeleteLinkError::NotFound(cmd.etwin, cmd.remote),
    )
    .map(|_| Default::default())
  }
}

#[async_trait]
impl<TyClock> Handler<DeleteLinkOptions<HammerfestUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    cmd: DeleteLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, DeleteLinkError<HammerfestUserIdRef>> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    delete_link(
      &mut state.from_hammerfest,
      &mut state.to_hammerfest,
      (cmd.remote.server, cmd.remote.id),
      (cmd.etwin.id, cmd.remote.server),
      |start| {
        let now = self.clock.clock().now();
        let link: OldRawLink<HammerfestUserIdRef> = OldRawLink {
          link: start.link,
          unlink: RawUserDot {
            time: now,
            user: cmd.unlinked_by,
          },
          etwin: cmd.etwin,
          remote: cmd.remote,
        };
        link
      },
      || DeleteLinkError::NotFound(cmd.etwin, cmd.remote),
    )
    .map(|_| Default::default())
  }
}

#[async_trait]
impl<TyClock> Handler<DeleteLinkOptions<TwinoidUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    cmd: DeleteLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, DeleteLinkError<TwinoidUserIdRef>> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    delete_link(
      &mut state.from_twinoid,
      &mut state.to_twinoid,
      cmd.remote.id,
      cmd.etwin.id,
      |start| {
        let now = self.clock.clock().now();
        let link: OldRawLink<TwinoidUserIdRef> = OldRawLink {
          link: start.link,
          unlink: RawUserDot {
            time: now,
            user: cmd.unlinked_by,
          },
          etwin: cmd.etwin,
          remote: cmd.remote,
        };
        link
      },
      || DeleteLinkError::NotFound(cmd.etwin, cmd.remote),
    )
    .map(|_| Default::default())
  }
}

#[async_trait]
impl<TyClock> Handler<GetLinkOptions<DinoparcUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    query: GetLinkOptions<DinoparcUserIdRef>,
  ) -> Result<VersionedRawLink<DinoparcUserIdRef>, WeakError> {
    // assert!(options.time.is_none());
    let state = self.state.read().unwrap();
    let link = state.from_dinoparc.get(&(query.remote.server, query.remote.id));

    match link {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(link) => {
        let link: VersionedRawLink<DinoparcUserIdRef> = VersionedRawLink {
          current: link.current.clone(),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock> Handler<GetLinkOptions<HammerfestUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    query: GetLinkOptions<HammerfestUserIdRef>,
  ) -> Result<VersionedRawLink<HammerfestUserIdRef>, WeakError> {
    // assert!(options.time.is_none());
    let state = self.state.read().unwrap();
    let link = state.from_hammerfest.get(&(query.remote.server, query.remote.id));

    match link {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(link) => {
        let link: VersionedRawLink<HammerfestUserIdRef> = VersionedRawLink {
          current: link.current.clone(),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock> Handler<GetLinkOptions<TwinoidUserIdRef>> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(
    &self,
    query: GetLinkOptions<TwinoidUserIdRef>,
  ) -> Result<VersionedRawLink<TwinoidUserIdRef>, WeakError> {
    // assert!(options.time.is_none());
    let state = self.state.read().unwrap();
    let link = state.from_twinoid.get(&query.remote.id);

    match link {
      None => Ok(VersionedRawLink {
        current: None,
        old: vec![],
      }),
      Some(link) => {
        let link: VersionedRawLink<TwinoidUserIdRef> = VersionedRawLink {
          current: link.current.clone(),
          old: vec![],
        };
        Ok(link)
      }
    }
  }
}

#[async_trait]
impl<TyClock> Handler<GetLinksFromEtwinOptions> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, query: GetLinksFromEtwinOptions) -> Result<VersionedRawLinks, WeakError> {
    let state = self.state.read().unwrap();
    let mut links = VersionedRawLinks::default();

    for srv in DinoparcServer::iter() {
      let empty = RawLinkHistory::<DinoparcUserIdRef>::default();
      let link = state.to_dinoparc.get(&(query.etwin.id, srv)).unwrap_or(&empty);
      match srv {
        DinoparcServer::DinoparcCom => links.dinoparc_com.current.clone_from(&link.current),
        DinoparcServer::EnDinoparcCom => links.en_dinoparc_com.current.clone_from(&link.current),
        DinoparcServer::SpDinoparcCom => links.sp_dinoparc_com.current.clone_from(&link.current),
      }
    }

    for srv in HammerfestServer::iter() {
      let empty = RawLinkHistory::<HammerfestUserIdRef>::default();
      let link = state.to_hammerfest.get(&(query.etwin.id, srv)).unwrap_or(&empty);
      match srv {
        HammerfestServer::HammerfestEs => links.hammerfest_es.current.clone_from(&link.current),
        HammerfestServer::HammerfestFr => links.hammerfest_fr.current.clone_from(&link.current),
        HammerfestServer::HfestNet => links.hfest_net.current.clone_from(&link.current),
      }
    }

    {
      let empty = RawLinkHistory::<TwinoidUserIdRef>::default();
      let link = state.to_twinoid.get(&query.etwin.id).unwrap_or(&empty);
      links.twinoid.current.clone_from(&link.current);
    }

    Ok(links)
  }
}

#[async_trait]
impl<TyClock> Handler<RawDeleteAllLinks> for MemLinkStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn handle(&self, cmd: RawDeleteAllLinks) -> Result<(), WeakError> {
    let mut state = self.state.write().unwrap();
    let state: &mut StoreState = &mut state;
    if let Some(link) = state.to_twinoid.get_mut(&cmd.etwin.id) {
      if let Some(cur_link) = link.current.take() {
        let reverse_link = state
          .from_twinoid
          .get_mut(&cur_link.remote.id)
          .expect("remote id exists");
        reverse_link.current = None;

        let old_link = OldRawLink {
          link: cur_link.link,
          unlink: RawUserDot {
            time: cmd.now,
            user: cmd.unlinked_by,
          },
          etwin: cur_link.etwin,
          remote: cur_link.remote,
        };

        link.old.push(old_link.clone());
        reverse_link.old.push(old_link);
      }
    }
    for server in HammerfestServer::iter() {
      if let Some(link) = state.to_hammerfest.get_mut(&(cmd.etwin.id, server)) {
        if let Some(cur_link) = link.current.take() {
          let reverse_link = state
            .from_hammerfest
            .get_mut(&(server, cur_link.remote.id))
            .expect("remote id exists");
          reverse_link.current = None;

          let old_link = OldRawLink {
            link: cur_link.link,
            unlink: RawUserDot {
              time: cmd.now,
              user: cmd.unlinked_by,
            },
            etwin: cur_link.etwin,
            remote: cur_link.remote,
          };

          link.old.push(old_link.clone());
          reverse_link.old.push(old_link);
        }
      }
    }
    for server in DinoparcServer::iter() {
      if let Some(link) = state.to_dinoparc.get_mut(&(cmd.etwin.id, server)) {
        if let Some(cur_link) = link.current.take() {
          let reverse_link = state
            .from_dinoparc
            .get_mut(&(server, cur_link.remote.id))
            .expect("remote id exists");
          reverse_link.current = None;

          let old_link = OldRawLink {
            link: cur_link.link,
            unlink: RawUserDot {
              time: cmd.now,
              user: cmd.unlinked_by,
            },
            etwin: cur_link.etwin,
            remote: cur_link.remote,
          };

          link.old.push(old_link.clone());
          reverse_link.old.push(old_link);
        }
      }
    }

    Ok(())
  }
}

fn touch_link<FK: Eq + core::hash::Hash, TK: Eq + core::hash::Hash, R: RemoteUserIdRef>(
  from: &mut HashMap<FK, RawLinkHistory<R>>,
  to: &mut HashMap<TK, RawLinkHistory<R>>,
  from_key: FK,
  to_key: TK,
  link: impl FnOnce() -> RawLink<R>,
) -> Result<VersionedRawLink<R>, TouchLinkError<R>> {
  let linked_etwin = from.entry(from_key).or_default();
  let linked_remote = to.entry(to_key).or_default();

  match (&mut linked_etwin.current, &mut linked_remote.current) {
    (from @ None, to @ None) => {
      let link: RawLink<R> = link();
      *from = Some(link.clone());
      *to = Some(link.clone());
      let link: VersionedRawLink<R> = VersionedRawLink {
        current: Some(link),
        old: vec![],
      };
      Ok(link)
    }
    (Some(linked_etwin), None) => Err(TouchLinkError::ConflictEtwin(linked_etwin.etwin)),
    (None, Some(linked_remote)) => Err(TouchLinkError::ConflictRemote(linked_remote.remote.clone())),
    (Some(linked_etwin), Some(linked_remote)) => Err(TouchLinkError::ConflictBoth(
      linked_etwin.etwin,
      linked_remote.remote.clone(),
    )),
  }
}

fn delete_link<FK: Eq + core::hash::Hash, TK: Eq + core::hash::Hash, R: RemoteUserIdRef>(
  from: &mut HashMap<FK, RawLinkHistory<R>>,
  to: &mut HashMap<TK, RawLinkHistory<R>>,
  from_key: FK,
  to_key: TK,
  old_link: impl FnOnce(RawLink<R>) -> OldRawLink<R>,
  not_found_err: impl FnOnce() -> DeleteLinkError<R>,
) -> Result<(), DeleteLinkError<R>> {
  let linked_etwin = from.get_mut(&from_key);
  let linked_remote = to.get_mut(&to_key);
  let (linked_etwin, linked_remote) = match (linked_etwin, linked_remote) {
    (Some(linked_etwin), Some(linked_remote)) => (linked_etwin, linked_remote),
    _ => return Err(not_found_err()),
  };

  match (&mut linked_etwin.current, &mut linked_remote.current) {
    (from @ Some(_), to @ Some(_)) if from == to => {
      let link = old_link(from.take().unwrap());
      *to = None;
      linked_etwin.old.push(link.clone());
      linked_remote.old.push(link);
      Ok(())
    }
    _ => Err(not_found_err()),
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemLinkStore;
  use crate::test::TestApi;
  use eternaltwin_core::clock::VirtualClock;
  use eternaltwin_core::core::Instant;
  use eternaltwin_core::dinoparc::DinoparcStore;
  use eternaltwin_core::hammerfest::HammerfestStore;
  use eternaltwin_core::link::store::LinkStore;
  use eternaltwin_core::user::UserStore;
  use eternaltwin_core::uuid::Uuid4Generator;
  use eternaltwin_dinoparc_store::mem::MemDinoparcStore;
  use eternaltwin_hammerfest_store::mem::MemHammerfestStore;
  use eternaltwin_user_store::mem::MemUserStore;
  use std::sync::Arc;

  #[allow(clippy::type_complexity)]
  fn make_test_api() -> TestApi<
    Arc<VirtualClock>,
    Arc<dyn DinoparcStore>,
    Arc<dyn HammerfestStore>,
    Arc<dyn LinkStore>,
    Arc<dyn UserStore>,
  > {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let dinoparc_store: Arc<dyn DinoparcStore> = Arc::new(MemDinoparcStore::new(Arc::clone(&clock)));
    let hammerfest_store: Arc<dyn HammerfestStore> = Arc::new(MemHammerfestStore::new(Arc::clone(&clock)));
    let link_store: Arc<dyn LinkStore> = Arc::new(MemLinkStore::new(Arc::clone(&clock)));
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(Arc::clone(&clock), Uuid4Generator));

    TestApi {
      clock,
      dinoparc_store,
      hammerfest_store,
      link_store,
      user_store,
    }
  }

  test_link_store!(|| make_test_api());
}
