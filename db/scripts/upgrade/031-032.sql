ALTER DOMAIN hammerfest_username DROP CONSTRAINT hammerfest_username_check;
ALTER DOMAIN hammerfest_username ADD CONSTRAINT hammerfest_username_check CHECK (value ~ '^[0-9A-Za-z_]{1,12}$');

-- Time-variant forum ban <profile(moderator)>
CREATE TABLE hammerfest_forum_ban
(
  period PERIOD_LOWER NOT NULL,
  retrieved_at       INSTANT_SET        NOT NULL,
  hammerfest_server  HAMMERFEST_SERVER  NOT NULL,
  hammerfest_user_id HAMMERFEST_USER_ID NOT NULL,
--
  forum_ban          BOOLEAN            NOT NULL,
  PRIMARY KEY (period, hammerfest_server, hammerfest_user_id),
  EXCLUDE USING gist (hammerfest_server WITH =, hammerfest_user_id WITH =, period WITH &&),
  CONSTRAINT hammerfest_forum_ban__user__fk FOREIGN KEY (hammerfest_server, hammerfest_user_id) REFERENCES hammerfest_users (hammerfest_server, hammerfest_user_id) ON DELETE RESTRICT ON UPDATE CASCADE
);
