ALTER TABLE email_addresses
  ADD COLUMN is_well_formed BOOLEAN DEFAULT TRUE;
CREATE DOMAIN raw_email_address AS text;
