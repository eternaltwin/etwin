ALTER DOMAIN hammerfest_username DROP CONSTRAINT hammerfest_username_check;
ALTER DOMAIN hammerfest_username ADD CONSTRAINT hammerfest_username_check CHECK (value ~ '^[0-9A-Za-z_#]{1,12}$');
