import {PathLike} from "node:fs";
import {readFile} from "node:fs/promises";
import {fileURLToPath} from "node:url";

import type {StaticProvider, Type} from "@angular/core";
import {Url} from "@eternaltwin/core/core/url";

export interface EngineOptions {
  CommonEngine: any,
  /**
   * File URI to the directory containing the static files.
   */
  browserDir: Url;
  serverIndex: Url;
  bootstrap: Type<{}>;
  providers: StaticProvider[];
}

export interface RenderOptions {
  url: Url;
  providers: StaticProvider[];
}

export class NgKoaEngine {
  private readonly document: string;
  private readonly publicPath: string;
  private readonly commonEngine: any;
  private readonly bootstrap: Type<{}>;

  public static async create(options: EngineOptions): Promise<NgKoaEngine> {
    const indexHtml = await readTextAsync(options.serverIndex);
    return new NgKoaEngine(options, indexHtml);
  }

  private constructor(options: EngineOptions, document: string) {
    this.publicPath = fileURLToPath(options.browserDir);
    this.document = document;
    this.bootstrap = options.bootstrap;
    this.commonEngine = new options.CommonEngine({bootstrap: options.bootstrap, providers: options.providers});
  }

  public async render(options: RenderOptions): Promise<string> {
    return this.commonEngine.render({
      bootstrap: this.bootstrap,
      document: this.document,
      url: options.url.toString(),
      providers: options.providers,
      publicPath: this.publicPath,
    });
  }
}

async function readTextAsync(filePath: PathLike): Promise<string> {
  return readFile(filePath, {encoding: "utf-8"});
}
