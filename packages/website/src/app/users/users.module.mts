import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {SharedModule} from "../shared/shared.module.mjs";
import {LinkedUserComponent} from "./linked-user.component.mjs";
import {UserViewComponent} from "./user-view.component.mjs";
import {UsersRoutingModule} from "./users-routing.module.mjs";

@NgModule({
  declarations: [LinkedUserComponent, UserViewComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class UsersModule {
}
