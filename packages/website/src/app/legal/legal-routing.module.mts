import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LegalViewComponent } from "./legal-view.component.mjs";

const routes: Routes = [
  {path: "", component: LegalViewComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LegalRoutingModule {
}
