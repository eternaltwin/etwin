import { NgModule } from "@angular/core";

import { SharedModule } from "../shared/shared.module.mjs";
import { LegalRoutingModule } from "./legal-routing.module.mjs";
import { LegalViewComponent } from "./legal-view.component.mjs";

@NgModule({
  declarations: [LegalViewComponent],
  imports: [
    LegalRoutingModule,
    SharedModule,
  ],
})
export class LegalModule {
}
