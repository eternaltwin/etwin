import { CommonModule } from "@angular/common";
import { Component, Input } from "@angular/core";

@Component({
  selector: "aside-style",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./aside-style.component.html",
  styleUrls: ["./aside-style.component.scss"],
})
export class AsideStyleComponent {
}