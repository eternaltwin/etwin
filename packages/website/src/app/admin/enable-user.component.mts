import {Component} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {$EnableUser, EnableUser} from "@eternaltwin/core/user/enable-user";
import {EnableUserOut} from "@eternaltwin/core/user/enable-user-out";
import {UuidHex} from "kryo/uuid-hex";
import {NEVER, Observable} from "rxjs";

import {UserService} from "../../modules/user/user.service.mjs";

interface FormModel {
  userId: FormControl<string>,
}

@Component({
  selector: "et-admin-send-marktwin-email-view",
  template:
`
  <etwin-main-layout>
    <h1>Enable user</h1>
    <form
      method="POST"
      enctype="application/x-www-form-urlencoded"
      action="#"
      [formGroup]="form"
      (ngSubmit)="onSubmit($event)"
    >
      <label>
        User ID<br />
        <input class="field" type="text" [formControl]="userId" value=""/>
      </label>
      <input type="submit" class="btn primary" name="apply"
             value="Enable user"/>
    </form>


    @if (result$ | async; as result) {
      <pre>{{result | json}}</pre>
    }
    @if (error$ | async; as error) {
      <pre>{{error | json}}</pre>
    }

  </etwin-main-layout>
`,
  styleUrls: [],
})
export class EnableUserComponent {
  public readonly userId: FormModel["userId"];
  public readonly form: FormGroup<FormModel>;
  public error$: Observable<string>;
  public result$: Observable<EnableUserOut>;
  #userService: UserService;

  constructor(userService: UserService) {
    this.userId = new FormControl(
      "",
      {
        nonNullable: true,
        validators: [Validators.required]
      },
    );
    this.form = new FormGroup({userId: this.userId});
    this.result$ = NEVER;
    this.error$ = NEVER;
    this.#userService = userService;
  }

  onSubmit(ev: SubmitEvent): void {
    ev.preventDefault();
    this.result$ = NEVER;
    this.error$ = NEVER;
    const model = this.form.getRawValue();
    const rawCmd: EnableUser = {
      userId: model.userId,
      idempotencyKey: genUuidv4(),
    };
    const cmd = $EnableUser.test(null, rawCmd);
    if (cmd.ok) {
      this.result$ = this.#userService.enableUser(cmd.value);
    } else {
      console.error("model read error");
      // todo: set `this.error$`
    }
  }
}

/**
 * UUIDv4 following the spec.
 *
 * See <https://datatracker.ietf.org/doc/html/rfc9562#name-uuid-version-4>
 */
function genUuidv4(): UuidHex {
  const alphabet = "0123456789abcdef";
  let res = "";
  for (let i: number = 0; i < 32; i++) {
    if (i === 8 || i === 12 || i === 16 || i === 20) {
      res += "-";
    }
    let halfByte = Math.floor(16 * Math.random());
    if (i === 12) {
      halfByte = 0b0100;
    } else if (i === 16) {
      halfByte = 0b1000 | (halfByte & 0b0011);
    }
    res += alphabet[halfByte];
  }
  return res;
}
