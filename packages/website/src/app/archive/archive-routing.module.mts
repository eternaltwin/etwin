import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { ArchiveHomeView } from "./archive-home.view.mjs";

const routes: Routes = [
  {path: "", component: ArchiveHomeView},
  {path: "dinoparc", loadChildren: () => import("./dinoparc/dinoparc.module.mjs").then(({DinoparcModule}) => DinoparcModule)},
  {path: "hammerfest", loadChildren: () => import("./hammerfest/hammerfest.module.mjs").then(({HammerfestModule}) => HammerfestModule)},
  {path: "twinoid", loadChildren: () => import("./twinoid/twinoid.module.mjs").then(({TwinoidModule}) => TwinoidModule)},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class ArchiveRoutingModule {
}
