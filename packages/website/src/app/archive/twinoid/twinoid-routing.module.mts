import { Injectable, NgModule } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { TwinoidUser } from "@eternaltwin/core/twinoid/twinoid-user";
import { $TwinoidUserId } from "@eternaltwin/core/twinoid/twinoid-user-id";
import {NOOP_CONTEXT, readOrThrow} from "kryo";
import { $Date } from "kryo/date";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";
import { Observable, of as rxOf } from "rxjs";

import { TwinoidService } from "../../../modules/twinoid/twinoid.service.mjs";
import { TwinoidHomeView } from "./twinoid-home.view.mjs";
import { TwinoidUserView } from "./twinoid-user.view.mjs";

@Injectable()
export class TwinoidUserResolverService implements Resolve<TwinoidUser | null> {
  readonly #router: Router;
  readonly #twinoid: TwinoidService;

  constructor(router: Router, twinoid: TwinoidService) {
    this.#router = router;
    this.#twinoid = twinoid;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TwinoidUser | null> {
    const userId: string | null = route.paramMap.get("user_id");
    if (userId === null || !$TwinoidUserId.test(NOOP_CONTEXT, userId).ok) {
      return rxOf(null);
    }
    const rawTime: string | null = route.queryParamMap.get("time");
    let time: Date | undefined;
    try {
      if (rawTime !== null) {
        time = readOrThrow($Date, SEARCH_PARAMS_VALUE_READER, rawTime);
      }
    } catch {
      // Ignore invalide `time` query param.
    }
    return this.#twinoid.getUser({id: userId, time});
  }
}

const routes: Routes = [
  {
    path: "",
    component: TwinoidHomeView,
    resolve: {
    },
  },
  {
    path: "users/:user_id",
    component: TwinoidUserView,
    resolve: {
      user: TwinoidUserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [TwinoidUserResolverService],
})
export class TwinoidRoutingModule {
}
