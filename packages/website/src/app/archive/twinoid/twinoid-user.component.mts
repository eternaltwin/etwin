import { Component, Input } from "@angular/core";
import { TwinoidUser } from "@eternaltwin/core/twinoid/twinoid-user";

@Component({
  selector: "etwin-twinoid-user",
  templateUrl: "./twinoid-user.component.html",
  styleUrls: [],
})
export class TwinoidUserComponent {
  @Input()
  public user!: TwinoidUser;

  constructor() {
  }

  ngOnInit(): void {
  }

  fullArchiveDate(): null | Date {
    if (!this.user) {
      return null;
    }
    let max: null | Date = null;
    for (const link of this.user.links) {
      const time: Date = link.user.latest.retrieved.latest;
      if (max === null || max.getTime() < time.getTime()) {
        max = time;
      }
    }
    return max;
  }
}
