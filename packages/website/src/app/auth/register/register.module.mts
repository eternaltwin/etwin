import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { SharedModule } from "../../shared/shared.module.mjs";
import { RegisterComponent } from "./register.component.mjs";
import { RegisterEmailComponent } from "./register-email.component.mjs";
import { RegisterRoutingModule } from "./register-routing.module.mjs";
import { RegisterUsernameComponent } from "./register-username.component.mjs";

@NgModule({
  declarations: [RegisterComponent, RegisterEmailComponent, RegisterUsernameComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RegisterRoutingModule,
    SharedModule,
  ],
})
export class RegisterModule {
}
