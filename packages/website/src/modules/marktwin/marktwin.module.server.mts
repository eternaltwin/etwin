import { NgModule } from "@angular/core";

import { MarktwinService } from "./marktwin.service.mjs";
import { ServerMarktwinService } from "./marktwin.service.server.mjs";

@NgModule({
  providers: [
    {provide: MarktwinService, useClass: ServerMarktwinService},
  ],
  imports: [],
})
export class MarktwinModule {
}
