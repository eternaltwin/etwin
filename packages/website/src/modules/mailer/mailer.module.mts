import { NgModule } from "@angular/core";

import { MailerService } from "./mailer.service.mjs";

@NgModule({
  imports: [],
  providers: [
    MailerService,
  ],
})
export class MailerModule {
}
