import { LiteralUnionType } from "kryo/literal-union";
import { $Null } from "kryo/null";
import { TryUnionType } from "kryo/try-union";
import { $Ucs2String } from "kryo/ucs2-string";

export type TwinoidLocale = "en" | "es" | "fr";

export const $TwinoidLocale: LiteralUnionType<TwinoidLocale> = new LiteralUnionType({
  type: $Ucs2String,
  values: ["en", "es", "fr"],
});

export type NullableTwinoidLocale = null | TwinoidLocale;

export const $NullableTwinoidLocale: TryUnionType<NullableTwinoidLocale> = new TryUnionType({variants: [$Null, $TwinoidLocale]});
