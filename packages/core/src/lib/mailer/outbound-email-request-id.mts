import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type OutboundEmailRequestId = UuidHex;

export const $OutboundEmailRequestId: Ucs2StringType = $UuidHex;
