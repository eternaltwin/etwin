import { SetType } from "kryo/set";

import { $HammerfestItemId, HammerfestItemId } from "./hammerfest-item-id.mjs";

/**
 * A map of Hammerfest item unlocks.
 */
export type HammerfestItemUnlocks = Set<HammerfestItemId>;

export const $HammerfestItemUnlocks: SetType<HammerfestItemId> = new SetType({
  itemType: $HammerfestItemId,
  maxSize: 500,
});
